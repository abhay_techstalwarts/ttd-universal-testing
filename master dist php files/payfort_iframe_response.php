<?php
require "../lib/functions.php";
require "../lib/config.php";
#$url="paymentfail";
$url="paymentfail";
$requestParams=$_REQUEST;
if(strtoupper($_SERVER['REQUEST_METHOD'])=="GET")
{
        $redirectURL=DEV_INF."$url?order_id=$order_id";
        redirect_url($redirectURL);
}
$secure_3d=false;
$log_prefix=date("Ymd H:i:s");
$log_file_name="Payfort_repsonse".date('Ymd').".log";
$file_upload_log="/var/tmp/".$log_file_name;
//echo "<pre>";print_r($requestParams);die;
$signature_hash_key=TTD_SECURE_KEY;
ksort($requestParams);
$signature_string="$signature_hash_key";
$payfort_payment_response=$requestParams['status'];
$order_id=$requestParams['merchant_reference'];
$response_message=strtolower($requestParams['response_message']);
$command=strtolower($requestParams['service_command']);
if($command=="")
	$command=strtolower($requestParams['command']);
$payment_status="";
error_log("\n$log_prefix  Token First Request  command=$command ".print_r($requestParams,true)."",3,$file_upload_log);
foreach($requestParams as $key=>$value)
{
	if($key!="signature")
	{	
       		$signature_string.="$key=$value";
	}
}
$signature_string.="$signature_hash_key";
$signature = hash('sha512', $signature_string);
$payment_status=false;
if($signature!=$requestParams['signature'])
{
	$payment_status=false;
}
if($payfort_payment_response==18 && ($response_message=="success" || $response_message=="token has been created" || $response_message=="token has been updated") && $command=="tokenization")
{
	$payment_status=true;
}
if(($payfort_payment_response==14 || $payfort_payment_response==18) && $response_message=="success" && $command=="purchase")
{
	$payment_status=true;
}

if($payment_status==true)
{
	$url="thankyou";
	$api_url = REST_API."Orders/$order_id";
	$nodes=array();
	$nodes[0]['url']  = $api_url;
	$results        = get_curl_repsonse($nodes);
	if ($results[0] == FALSE)
	{
	        $redirectURL=DEV_INF."$url?order_id=$order_id";
	        redirect_url($redirectURL);
	}
	$reponseArr=json_decode($results[0],true);
	if($reponseArr['success']===false)
	{
	        $redirectURL=DEV_INF."$url?order_id=$order_id";
	        redirect_url($redirectURL);
	}
	if($reponseArr['result'][0])
	{
	        $reponseArr=$reponseArr['result'][0];
	}
//PAYFORT_RESPONSE_STATUS
	$order_payment_status="";
	$order_payment_status=$reponseArr['order_payment_status'];
	$currency=$reponseArr['currency'];
	$order_total=$reponseArr['order_total'];
	$shopper_email=$reponseArr['shopper_email'];
	$remember_me=$reponseArr['remember_me'];
	if($order_payment_status!="P")
	{
	        $redirectURL=DEV_INF."$url?order_id=$order_id";
	        redirect_url($redirectURL);
	}	
	/*
	if($command=="purchase")
	{
		$redirectURL=DEV_INF."$url?order_id=$order_id";
		error_log("\n$log_prefix  Token First Request success  command=$command redirectURL=$redirectURL",3,$file_upload_log);
		redirect_url($redirectURL);
	}	*/	
}
else
{
	error_log("\n$log_prefix  Token First Request  Failure command=$command redirectURL=$redirectURL",3,$file_upload_log);
	redirect_url($redirectURL);
}
if($command=="tokenization" && $payment_status==true)
{
	$response_code=$requestParams['response_code'];
	$card_number=$requestParams['card_number'];
	$expiry_date=$requestParams['expiry_date'];
	$response_message=$requestParams['response_message'];
	$token_name=$requestParams['token_name'];
	$status=$requestParams['status'];
	$card_bin=$requestParams['card_bin'];
	$response_message=$requestParams['response_message'];
	$order_id=$requestParams['merchant_reference'];

	$redirectURL=DEV_INF."$url?order_id=$order_id";

	//$update_all_required_data['order_id']=$order_id;
	$update_all_required_data['command']="PURCHASE";
	//$update_all_required_data['command']="AUTHORIZATION";
	$update_all_required_data['access_code']=PAYFORT_ACCESS_CODE;
	$update_all_required_data['merchant_identifier']=PAYFORT_MERCHANT;
	$update_all_required_data['merchant_reference']=$order_id;
	$update_all_required_data['amount']=$order_total*100;
	$update_all_required_data['currency']="$currency";
	$update_all_required_data['language']="en";
	$update_all_required_data['customer_email']="$shopper_email";
	$update_all_required_data['token_name']="$token_name";
	if($remember_me==1)
	{		
		$update_all_required_data['remember_me']="YES";
	}
	else
	{
		$update_all_required_data['remember_me']="NO";		
	}		
	#$update_all_required_data['return_url']="https://dev.ticketstodo.com/payfort_3dsecure_response.php";
//	$update_all_required_data['return_url']="https://dev.ticketstodo.com/thankyou";
	//$requestParams['return_url']="http://stgstatic.ticketstodo.com/success.php";
	$signature_hash_key=TTD_SECURE_KEY;
	ksort($update_all_required_data);
	$signature_string="$signature_hash_key";
	foreach($update_all_required_data as $key=>$value)
	{
        	$signature_string.="$key=$value";
	}
	$signature_string.="$signature_hash_key";
	$signature = hash('sha512', $signature_string);
	$update_all_required_data['signature']=$signature;
/*	$check_length=array();
	foreach($update_all_required_data as $key=>$val)
	{
		$check_length[$key]['params']=$val;
		$check_length[$key]['length']=strlen($val);
	}*/
	$data=json_encode($update_all_required_data);
	$api_url ="https://sbpaymentservices.payfort.com/FortAPI/paymentApi" ;
	$CURLOPT_HTTPHEADERS=array(
	    'Content-Type: application/json',
	    'Content-Length: ' . strlen($data));
	$nodes=array();
	$nodes[0]['url']  = $api_url;
	$nodes[0]['post'] = "$data";
	$results        = get_curl_repsonse($nodes,$CURLOPT_HTTPHEADERS);
	$url="paymentfail";
	if ($results[0] == FALSE)
	{
	        $redirectURL=DEV_INF."$url?order_id=$order_id";
	        redirect_url($redirectURL);
	}
	$response_json_data=json_decode($results[0],true);
	error_log("\n$log_prefix  Token second response command=$command ".print_r($response_json_data,true)."",3,$file_upload_log);
	if($response_json_data['status']=="20" &&  $response_json_data['response_message']=="3-D Secure check requested")
	{
		$r3ds_url=$response_json_data["3ds_url"];
		$redirectURL=$r3ds_url;
		error_log("\n$log_prefix  Token First Request  command=$command redirectURL=$redirectURL",3,$file_upload_log);
		//redirect_url($redirectURL);	
		echo "<html><body onLoad=\"javascript: window.top.location.href='" . $redirectURL. "'\"></body></html>";
	 	exit;
	}
	else if ($response_json_data['status']=="14" &&  strtolower($response_json_data['response_message'])=="success")
	{
		$payment_status=true;
		$command=strtolower($response_json_data['command']);
		$requestParams=$response_json_data;
	}
	else
	{
		$url="paymentfail";
		$redirectURL=DEV_INF."$url?order_id=$order_id";
                redirect_url($redirectURL);
	}

}
if($command=="purchase" && $payment_status==true)
{
	$update_all_required_data="";
	$update_all_required_data['order_id']=$order_id;
	$update_all_required_data['currency']=$currency;
	$update_all_required_data['gid']=1;
	$update_all_required_data['order_details']=$reponseArr;
	$update_all_required_data['payment_details']=$requestParams;
	$data=json_encode($update_all_required_data);
	$log_prefix=date("Ymd H:i:s");
	$log_file_name="Payfort_repsonse".date('Ymd').".log";
	$file_upload_log="/var/tmp/".$log_file_name;
	$HTTP_USER_AGENT=$_SERVER['HTTP_USER_AGENT'];
	$HTTP_USER_AGENT=strtolower($HTTP_USER_AGENT);
	if(strpos($HTTP_USER_AGENT,"httpclient") && strpos($HTTP_USER_AGENT,"java"))
	{
		error_log("\n$log_prefix HTTP_USER_AGENT no neeed to process this $HTTP_USER_AGENT",3,$file_upload_log);
		exit;
	}	
	error_log("\n$log_prefix Payment Resuest for Order $order_id :: $data",3,$file_upload_log);
	$api_url = REST_API."order_payment_details";
	$CURLOPT_HTTPHEADERS=array(
	    'Content-Type: application/json',
	    'Content-Length: ' . strlen($data));
	$nodes=array();
	$nodes[0]['url']  = $api_url;
	$nodes[0]['post'] = "$data";
	$results        = get_curl_repsonse($nodes,$CURLOPT_HTTPHEADERS);
	if ($results[0] == FALSE)
	{
        	$redirectURL=DEV_INF."$url?order_id=$order_id";
	        redirect_url($redirectURL);
	}
	$reponseArr=json_decode($results[0],true);
	error_log("\n$log_prefix  Token First Request  command=$command ".print_r($reponseArr,true)."",3,$file_upload_log);
	if($reponseArr[0])
	{
	        $reponseArr=$reponseArr[0];
	}
	if($reponseArr['success']===true)
	{
		$url='thankyou';
		$redirectURL=DEV_INF."$url?order_id=$order_id";
                redirect_url($redirectURL);
	}
	else
	{
		$redirectURL=DEV_INF."$url?order_id=$order_id";	
        	$error_message=$reponseArr['error']['message'];
	        $error_msg="";
        	if(is_array($error_message))
	        {
        	        foreach($error_message as $key=>$values)
                	{
                        	$error_msg.=$values[0];
	                }
        	        $error_message=$error_msg;
        	}
	}
}
//if($requestParams[""])
redirect_url($redirectURL);
?>
