Sitemap: https://dev.ticketstodo.com/sitemap.xml
User-agent: *
Disallow: /out/


User-agent: Yeti
#Allow:/ko/
Disallow:/zh-TW/
Disallow:/zh-CN/
Disallow:/zh-HK/
Disallow:/th/
Disallow:/vi/
Disallow:/id/
Disallow:/ja/
Disallow:/en-GB/
Disallow:/en-AU/
Disallow:/en-SG/
Disallow:/en-IN/
Disallow:/en-NZ/
Disallow:/en-US/
Disallow:/event/
Disallow:/invite/
Disallow:/preview/
Disallow:/my_klook/
Disallow:/voucher/

User-Agent: *
#block JP site & Wifi vertical
Disallow: /ja/
Disallow: /invite/
Disallow: /preview/
Disallow: /my_klook/
Disallow: /voucher/
Disallow: /zh-CN/voucher/
Disallow: /zh-TW/voucher/
Disallow: /zh-HK/voucher/
Disallow: /ko/voucher/
Disallow: /th/voucher/
Disallow: /vi/voucher/
Disallow: /activity/9370-art-zoo-inflatable-park-singapore/

User-agent: dotbot Disallow: /
User-agent: MJ12bot Disallow: /
User-agent: 008 Disallow: /
User-agent: Yahoo Pipes 1.0 Disallow: /
User-agent: Yahoo Pipes 2.0 Disallow: /
User-agent: Fasterfox Disallow: /
User-agent: SemrushBot Disallow: /
User-agent: Mediapartners-Google Disallow: /landing
