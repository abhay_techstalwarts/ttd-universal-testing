// DEV Configuration
// export const environment = {
//   isLive:0,
//   production: true,
//   apiBaseUrl: 'https://stgapi.ticketstodo.com',
//   serverBaseUrl: 'https://dev.ticketstodo.com',
//   cloudinaryServer: 'nearlive',
//   imageUploadUrl: 'https://stgimages.ticketstodo.com/imageupload.php',
//   paymentBaseUrl: 'https://devpayment.ticketstodo.com',
//   payfortUrl: 'https://sbcheckout.payfort.com/FortAPI/paymentPage'
// };

// STG Configuration
export const environment = {
 isLive:0,
  production: true,
  apiBaseUrl: 'https://api.ticketstodo.com',
  serverBaseUrl: 'https://stg.ticketstodo.com',
  cloudinaryServer: 'prod',
  imageUploadUrl: 'https://images.ticketstodo.com/imageupload.php',
  paymentBaseUrl: 'https://stgpayment.ticketstodo.com',
  payfortUrl: 'https://checkout.payfort.com/FortAPI/paymentPage'
};

// LIVE Configuration
// export const environment = {
//   isLive:1,
//   production: true,
//   apiBaseUrl: 'https://api.ticketstodo.com',
//   serverBaseUrl: 'https://www.ticketstodo.com',
//   cloudinaryServer: 'prod',
//   imageUploadUrl: 'https://images.ticketstodo.com/imageupload.php',
//   paymentBaseUrl: 'https://payment.ticketstodo.com',
//   payfortUrl: 'https://checkout.payfort.com/FortAPI/paymentPage'
// };
