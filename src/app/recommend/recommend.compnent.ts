import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { ShoppingCartService, BrowserStorageService } from '../shared/_services';
@Component({
  selector: 'app-recommend',
  templateUrl: './recommend.component.html',
  styleUrls: ['./recommend.component.css']
})
export class RecommendComponent implements OnInit {
  loading: boolean;
  cartData;
  currency: string;

  constructor(
    private cartService: ShoppingCartService,
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject(PLATFORM_ID) private platformID: any,
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private shoppingCartService: ShoppingCartService,
    private browserStorageService: BrowserStorageService,
    private _location: Location,

  ) {
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformID)) {
      window.scrollTo(0, 0);
    }
    this.getItem();
  }

  getItem() {
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
    if (this.browserStorageService.getLocalStorage('shopperId') === '0') {
      this.loading = false;
    } else {
      this.loading = true;
      this.shoppingCartService
        .getAddedItem(this.browserStorageService.getLocalStorage('shopperId'))
        .subscribe(
          success => {
            this.cartData = success.result;
            this.loading = false;
          },
          _fail => {
            this.loading = false;
          }
        );
    }
  }

  goBack() {
    this._location.back();
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.currency = $event;
    this.getItem();
  }

}
