import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './signUp.component';

@NgModule({
  declarations: [
    SignUpComponent,
  ],
  imports: [
    SignUpRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    SignUpComponent
  ],
  providers: []
})
export class SignUpModule {
}
