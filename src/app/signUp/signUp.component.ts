﻿import { Component, OnInit, ElementRef, PLATFORM_ID, ViewChild, Inject } from '@angular/core';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, AlertService } from '../shared/_services/index';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from './../shared/_helper/form-helper';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { isPlatformBrowser } from '@angular/common';

@Component({
  moduleId: module.id.toString(),
  selector: 'app-signup',
  templateUrl: 'signUp.component.html'
})

export class SignUpComponent implements OnInit {

  model: any = {};
  text: any;
  returnUrl: string;
  loginForm;
  failMessage: string;
  @ViewChild('signUpButton') signUpButton: ElementRef;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: any,
    private browserStorageService: BrowserStorageService,
    private authenticationService: AuthenticationService,
    public _auth: AuthService,
    public alertService: AlertService
  ) {

  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    this.initiateForms();
    // reset login status
    this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  directLogin() {
    if (this.loginForm.isValid()) {
      FormHelper.disableButton(this.signUpButton, 'Signing In...');
      this.authenticationService.signUp(this.loginForm.getData())
        .subscribe(
          success => {
            FormHelper.enableButton(this.signUpButton, 'Sign Up');
            if (success.success) {
              this.browserStorageService.setLocalStorage('user', JSON.stringify(success.result[0]));
              this.browserStorageService.setLocalStorage('shopperId', JSON.stringify(success.result[0].shopper_id));
              this.setCart(success.result[0]);
            }
          },
          error => {
            FormHelper.enableButton(this.signUpButton, 'Sign Up');
            if (error.error.message) {
              this.failMessage = error.error.message;
              this.alertService.error(this.failMessage);
            }
          });
    }
  }
  setCart(response: any) {
    this.browserStorageService.setLocalStorage(
      'cartCount',
      response.item_counts
    );

    this.browserStorageService.setLocalStorage(
      'shopperId',
      response.shopper_id
    );

    if (this.returnUrl) {
      this.router.navigateByUrl(this.returnUrl);
    } else {
      this.router.navigateByUrl('');
    }
  }

  signIn(socialPlatform: string) {
    let provider;
    if (socialPlatform === 'facebook') {
      provider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      provider = GoogleLoginProvider.PROVIDER_ID;
    }
    this._auth.signIn(provider).then(
      (data) => {
        this.ttdSocialSignUp(data);
      }
    );
  }
  ttdSocialSignUp(user) {
    this.authenticationService.socialLogin(user)
      .subscribe(
        success => {
          this.browserStorageService.setLocalStorage('user', JSON.stringify(success.result));
          this.browserStorageService.setLocalStorage('shopperId', JSON.stringify(success.result.shopper_id));
        },
        error => {
          if (error.error.message) {
            this.failMessage = error.error.message;
          }
        });
  }

  initiateForms() {
    this.loginForm = new ValidationManager({
      'email': { 'rules': 'required|email' },
      'password': { 'rules': 'required|minLength:8' },
      'title': { 'rules': 'required' },
      'firstname': { 'rules': 'required' },
      'lastname': { 'rules': 'required' },
      'create_shopper': { 'rules': 'required', 'value': 1 },
      'userby': { 'rules': 'required', 'value': 'login' }
    });
  }

}
