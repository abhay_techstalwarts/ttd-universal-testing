import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShoppingCartComponent } from './shopping-cart.component';
import { SharedModule } from '../shared/shared.module';
import { ShoppingCartRouting } from './shopping-cart.routing';
@NgModule({
  declarations: [
    ShoppingCartComponent,
  ],
  imports: [
    ShoppingCartRouting,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    ShoppingCartComponent,
  ]
})
export class ShoppingCartModule {
}
