import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { ValidationManager } from 'ng2-validation-manager';
import swal from 'sweetalert2';
import * as _filter from 'lodash/filter';
import { Router } from '@angular/router';
import { ShoppingCartService, CityService } from '../shared/_services/index';
import { BrowserStorageService } from '../shared/_services/browserStorage.service';
import { CommonHelper } from '../shared/_helper/common.helper';
import { isPlatformBrowser } from '@angular/common';

declare let $: any;
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']  
})
export class ShoppingCartComponent implements OnInit {
  cartData: any;
  text: any;
  noOfItem: number;
  loading = true;
  editItemForm;
  currency: string;
  expiredItems: any[];
  popularDestinations: any[];

  selectedAll: true;
  activityId: number;
  timeout:any;
  allowBindQty=true;

  constructor(@Inject(WINDOW) private window: Window,
    private browserStorageService: BrowserStorageService,
    private shoppingCartService: ShoppingCartService,
    @Inject(PLATFORM_ID) private platformId: any,
    private router: Router,
    private cityService: CityService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.window.scrollTo(0, 0);
    }
    this.getCart();
    this.getExpiredItems();
    this.getLocalCurrency();
    this.selectedAll = true;
    this.loadPopularDestinations();
  }

  getLocalCurrency() {
    this.currency = this.browserStorageService.getLocalStorage('currencyType');

  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
    this.getCart();
    this.getExpiredItems();
  }

  getCart() {
    if (this.browserStorageService.getLocalStorage('shopperId') === '0') {
      this.loading = false;
    } else {
      this.loading = true;
      this.shoppingCartService
        .getCartData(this.browserStorageService.getLocalStorage('shopperId'))
        .subscribe(
          success => {
            
              this.cartData = success.result;
              this.loading = false;
              this.noOfItem = this.cartData.item_counts;
              this.browserStorageService.setLocalStorage('cartCount', this.cartData.item_counts);
              this.selectAll();
          },
          _fail => {
            this.loading = false;
          }
        );
    }
  }

  getExpiredItems() {
    this.shoppingCartService
      .getExpiredItems(this.browserStorageService.getLocalStorage('shopperId'))
      .subscribe(success => {
        this.expiredItems = success.result.data;
      });
  }

  deleteItem(item: any, fromExpired?: boolean, expiredItemIndex?: number) {
    const shopperId = this.browserStorageService.getLocalStorage('shopperId');
    this.shoppingCartService.deletePackage(shopperId, item).subscribe(
      success => {
        this.cartData = success.result;
        if (fromExpired) {
          if (expiredItemIndex < this.expiredItems.length) {
            this.expiredItems.splice(expiredItemIndex, 1);
          }
        }
        this.noOfItem = this.cartData.item_counts;
        this.browserStorageService.setLocalStorage('cartCount', this.cartData.item_counts);
        this.text = 'reload header';
        this.loading = false;
      },
      _fail => {
        swal({
          type: 'error',
          title: 'Failed',
          text: 'Not able to delete item!'
        });
        this.loading = false;
      }
    );
  }

  deleteSelectItems() {
    const selectedItems = _filter(this.cartData.data, 'selected');
    selectedItems.forEach(item => {
      this.deleteItem(item);
    });
  }

  clearExpiredItems() {
    while (this.expiredItems.length > 0) {
      this.deleteItem(this.expiredItems[0], true, 0);
    }
  }

  editItem(packageId) {
    this.allowBindQty = true;
    this.loading = true;
    const shopperId = this.browserStorageService.getLocalStorage('shopperId');
    this.shoppingCartService
      .editPackage(shopperId, packageId, this.editItemForm.getData())
      .subscribe(
        success => {
          if(this.allowBindQty){
          this.loading = false;
          this.cartData = success.result;
          this.noOfItem = this.cartData.item_counts;
          this.browserStorageService.setLocalStorage('cartCount', this.cartData.item_counts);
        }

        },
        _fail => {
          this.loading = false;
          swal({
            type: 'error',
            title: 'Failed',
            text: 'Failed to edit cart!'
          });
        }
      );
  }

  removeQty(groupDetail, item) {
    this.allowBindQty = false;
    clearTimeout(this.timeout);
    let val= parseInt($(`#${item.package_id}_${groupDetail.group_price_id}`).val())
    if (val > groupDetail.min_quantity  && item.total_participants > 1) {
      if (val > 0) {
        val--;
        $(`#${item.package_id}_${groupDetail.group_price_id}`).val(val)
        this.initializeForm(groupDetail, item,val);
      }
      let that =this;
		  this.timeout = setTimeout(function(){
        that.editItem(item.package_id)
      }, 800);
    }
  }

  addQty(groupDetail, item) {
    this.allowBindQty = false;
    clearTimeout(this.timeout);
    let val= parseInt($(`#${item.package_id}_${groupDetail.group_price_id}`).val())
    if (val < groupDetail.max_quantity) {
      
      if (val >= 0) {
        val++;
        $(`#${item.package_id}_${groupDetail.group_price_id}`).val(val)
        this.initializeForm(groupDetail, item,val);
      }
      let that =this;
      
		  this.timeout = setTimeout(function(){
        that.editItem(item.package_id)
      }, 800);
      
    }
  }


  initializeForm(groupDetail, item ,quantity) {
    this.editItemForm = new ValidationManager({
      booking_date: {
        rules: 'required',
        value: CommonHelper.stringToDateFormater(item.booking_activity_date)
      },
      total_participants: {
        rules: 'required|number',
        value: quantity
      },
      group_price_details: [
        new ValidationManager({
          package_group_price_id: {
            rules: 'required|number',
            value: groupDetail.package_group_price_id
          },
          group_price_id: {
            rules: 'required|number',
            value: groupDetail.group_price_id
          },
          age_group_name: {
            rules: 'required|number',
            value: groupDetail.age_group_name
          },
          quantity: {
            rules: 'required|number',
            value: quantity
          },
          package_id: { rules: 'required', value: item.package_id },
          partner_id: { rules: 'required', value: item.partner_id }
        })
      ]
    });
  }

  selectAll() {
    if (this.cartData && this.cartData.data) {
      this.cartData.data.forEach(element => {
        element.selected = this.selectedAll;
      });
    }
  }

  checkIfAllSelected() {
    if (this.cartData && this.cartData.data) {
      this.selectedAll = this.cartData.data.every(item => {
        return item.selected === true;
      });
    }
    let totalCost = 0;
    this.cartData.data.forEach(item => {
      if (item.selected === true) {
        totalCost += item.cost_price;
      }
    });
    this.cartData.total_cost = totalCost.toFixed(2);
  }

  payNow() {
    if (this.cartData && this.cartData.data && this.cartData.data.length > 0) {
      const selectedItems = new Array();
      this.cartData.data.forEach(item => {
        if (item.selected) {
          const obj = {
            package_id: item.package_id,
            booking_date: CommonHelper.stringToDateFormater(
              item.booking_activity_date
            )
          };
          selectedItems.push(obj);
        }
      });
      if (selectedItems && selectedItems.length > 0) {
        this.browserStorageService.setLocalStorage('package_details', JSON.stringify(selectedItems));
        this.router.navigate(['/pay']);
      } else {
        swal({
          type: 'info',
          text: 'Please select atleast one package to proceed'
        });
      }
    } else {
      swal({
        type: 'info',
        text: 'Your cart is empty'
      });
    }
  }

  loadPopularDestinations() {
    const curPage = 1;
    const records = 8;
    this.cityService.getPopularDestinations(curPage, records).subscribe(
      success => {
        this.popularDestinations = success.result.data;
      },
      _fail => { }
    );
  }

  onEditClick(data) {
    const titleRoute = data.package_name.replace(/\, /gi, '-').replace(/\ /gi, '-').replace(/\//gi, '-');
    this.router.navigate(['/activity', this.activityId + '-' + titleRoute], { queryParams: { edit: true}});
  }
}
