﻿
import { Component, OnInit, ElementRef, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from './../shared/_helper/form-helper';
import { AuthService } from 'angularx-social-login';
import { AuthenticationService, BrowserStorageService } from '../shared/_services';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
@Component({
  moduleId: module.id.toString(),
  selector: 'app-login',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  model: any = {};
  text: any;
  returnUrl: string;
  loginForm;
  failMessage: string;
  isServer = false;
  @ViewChild('loginButton') loginButton: ElementRef;


  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private route: ActivatedRoute,
    private router: Router,
    private browserStorageService: BrowserStorageService,
    @Inject(PLATFORM_ID) private platformID: any,
    private authenticationService: AuthenticationService,
    public _auth: AuthService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformID)) {
      window.scrollTo(0, 0);
    } else {
      this.isServer = true;
    }

    this.initiateForms();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
  }

  directLogin() {
    if (this.loginForm.isValid()) {
      FormHelper.disableButton(this.loginButton, 'Logging In...');
      this.authenticationService.login(this.loginForm.getData()).subscribe(
        success => {
          FormHelper.enableButton(this.loginButton, 'Log In');
          if (success.success) {
            this.browserStorageService.setLocalStorage(
              'user',
              JSON.stringify(success.result[0])
            );

            this.browserStorageService.setLocalStorage(
              'shopperId',
              JSON.stringify(success.result[0].shopper_id)
            );

            this.setCart(success.result[0]);
          }
        },
        error => {
          FormHelper.enableButton(this.loginButton, 'Log In');
          if (error.error.message) {
            this.failMessage = error.error.message;
          }
        }
      );
    }
  }

  signIn(socialPlatform: string) {
    let provider;
    if (socialPlatform === 'facebook') {
      provider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      provider = GoogleLoginProvider.PROVIDER_ID;
    }
    this._auth.signIn(provider).then(data => {
      this.browserStorageService.setLocalStorage('user', JSON.stringify(data));

      this.getCart();
      this.serverLogin(data);
    });
  }

  serverLogin(user) {
    this.authenticationService.socialLogin(user).subscribe(
      success => {
        this.browserStorageService.setLocalStorage(
          'user',
          JSON.stringify(success.result)
        );

        this.browserStorageService.setLocalStorage(
          'shopperId',
          JSON.stringify(success.result.shopper_id)
        );
        this.setCart(success.result);
      },
      error => {
        this.browserStorageService.setLocalStorage(
          'user',
          JSON.stringify(error.result)
        );

        this.browserStorageService.setLocalStorage(
          'shopperId',
          JSON.stringify(error.result.shopper_id)
        );

        this.setCart(error.result);
      }
    );
  }

  initiateForms() {
    this.loginForm = new ValidationManager({
      email: 'required|email',
      password: 'required|minLength:8',
      status: { rules: 'required', value: 1 }
    });
  }

  getCart() {
    const data = {
      email: JSON.parse(this.browserStorageService.getLocalStorage('user'))
        .email,
      status: 1,
      check_count_status: 1
    };
    this.authenticationService.getShopperIdonLogin(data).subscribe(
      success => {
        this.browserStorageService.setLocalStorage(
          'cartCount',
          success.result.item_counts
        );

        this.browserStorageService.setLocalStorage(
          'shopperId',
          success.result.shopper_id
        );

        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        } else {
          this.router.navigateByUrl('');
        }
      },
      _fail => {
        this.browserStorageService.setLocalStorage('cartCount', '0');
        this.browserStorageService.setLocalStorage('shopperId', '0');
        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        } else {
          this.router.navigateByUrl('');
        }
      }
    );
  }

  setCart(response: any) {
    this.browserStorageService.setLocalStorage(
      'cartCount',
      response.item_counts
    );

    this.browserStorageService.setLocalStorage(
      'shopperId',
      response.shopper_id
    );

    if (this.returnUrl) {
      this.router.navigateByUrl(this.returnUrl);
    } else {
      this.router.navigateByUrl('');
    }
  }

  signUpClick() {
    this.router.navigate(['/signup'], { queryParamsHandling: 'merge' });
  }


}
