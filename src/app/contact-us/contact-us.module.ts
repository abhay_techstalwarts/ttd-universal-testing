import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContactUsComponent } from './contact-us.component';
import { SharedModule } from '../shared/shared.module';
import { ContactUsRouting } from './contact-us.routing';
@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    ContactUsRouting,
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [ContactUsComponent],
  providers: []
})
export class ContactUsModule {}
