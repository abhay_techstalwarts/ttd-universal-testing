import { isPlatformBrowser } from '@angular/common';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { StaticPagesService, CityService, BrowserStorageService } from '../shared/_services/index';
import { ValidationManager } from 'ng2-validation-manager';
import swal from 'sweetalert2';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactUsData: any;
  text: any;
  device: string;
  cities = [];
  contactUsForm;
  defaultCityValue = '';

  constructor(
    @Inject(WINDOW) private window: Window, @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platFormId: any,
    private staticPagesService: StaticPagesService,
    private cityService: CityService,
    private browserStorageService: BrowserStorageService
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
  }

  ngOnInit() {

    if (isPlatformBrowser(this.platFormId)) {
      this.window.scrollTo(0, 0);
    }
    this.initiateForms();
  }

  setCity($event) {
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('custom_fields', 3)
      .setValue('value', $event);
  }

  onSubmit() {
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('comment')
      .isValid();
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('custom_fields', 0)
      .isValid();
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('custom_fields', 1)
      .isValid();
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('custom_fields', 2)
      .isValid();
    this.contactUsForm
      .getChildGroup('ticket')
      .getChildGroup('custom_fields', 3)
      .isValid();

    if (
      this.contactUsForm.isValid() &&
      this.contactUsForm
        .getChildGroup('ticket')
        .getChildGroup('comment')
        .isValid() &&
      this.contactUsForm
        .getChildGroup('ticket')
        .getChildGroup('custom_fields', 0)
        .isValid() &&
      this.contactUsForm
        .getChildGroup('ticket')
        .getChildGroup('custom_fields', 1)
        .isValid() &&
      this.contactUsForm
        .getChildGroup('ticket')
        .getChildGroup('custom_fields', 2)
        .isValid() &&
      this.contactUsForm
        .getChildGroup('ticket')
        .getChildGroup('custom_fields', 3)
        .isValid()
    ) {
      this.staticPagesService
        .getContactUs(this.contactUsForm.getData())
        .subscribe(
          _success => {
            this.initiateForms();
            this.defaultCityValue = 'clear';
            swal(
              'Thanks! We have received your question and will get back to you soon',
              '',
              'success'
            );
          },
          _fail => {
            this.initiateForms();
            this.defaultCityValue = 'clear';
            swal(
              'Thanks! We have received your question and will get back to you soon',
              '',
              'success'
            );
          }
        );
    }
  }

  initiateForms() {
    this.contactUsForm = new ValidationManager({
      ticket: new ValidationManager({
        subject: { rules: 'required' },
        comment: new ValidationManager({
          body: { rules: 'required' }
        }),
        custom_fields: [
          new ValidationManager({
            id: { rules: 'required', value: '360003574413' },
            value: { rules: 'required' }
          }),
          new ValidationManager({
            id: { rules: 'required', value: '360003657934' },
            value: { rules: 'required|email' }
          }),
          new ValidationManager({
            id: { rules: 'required', value: '360003590593' },
            value: { rules: '' }
          }),
          new ValidationManager({
            id: { rules: 'required', value: '360003590613' },
            value: { rules: 'required' }
          })
        ]
      })
    });

  }
}
