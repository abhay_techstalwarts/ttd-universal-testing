import { Component, OnInit,OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { Subscription, timer } from 'rxjs';
import {
  HomeService,
  BrowserStorageService,
  SEOProfileService
} from '../shared/_services/index';
import {
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/animations';
import { DefaultConstant } from '../shared/_constants/default_constants';
import { environment } from '../../environments/environment';

declare let jQuery: any;
@Component({
  moduleId: module.id.toString(),
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        'out',
        style({
          transform: 'translate3d(-100%, 0, 0)'
        })
      ),
      transition('in => out', animate('500ms ease-in-out')),
      transition('out => in', animate('500ms ease-in-out'))
    ]),
    trigger('slideInOutCity', [
      state(
        'inCity',
        style({
          transform: 'translate3d(0, 0, 0)'
        })
      ),
      state(
        'outCity',
        style({
          transform: 'translate3d(-100%, 0, 0)'
        })
      ),
      transition('inCity => outCity', animate('500ms ease-in-out')),
      transition('outCity => inCity', animate('500ms ease-in-out'))
    ])
  ]
})
export class HomeComponent implements OnInit, OnDestroy {
  homeData: any;
  text: any;
  state = 'in';
  stateCity = 'inCity';
  device: string;
  private sub: Subscription;
  private timer;
  loadAllDestinations = false;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  metaOgTitle = `Book Tickets Online. Tours, Attractions, Activities & Things To Do | TicketsToDo`;
  metaDescription = `At TicketsToDo we offer a simple way to discover activities, attractions & things
   to do with just a few clicks. We aim at offering online booking experience for all sorts of
   entertainment with a superior customer service.`;


  constructor(@Inject(WINDOW) private window: Window, @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platFormId: any,
    private browserStorageService: BrowserStorageService,
    private homeService: HomeService,
    private seoProfileService: SEOProfileService,
    private meta: Meta,
    private titleService: Title
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
  }


  ngOnInit() {
    if (isPlatformBrowser(this.platFormId)) {
      this.timer = timer(4000, 4000);
      this.window.scrollTo(0, 0);
      this.loadHomeData();
    }
    this.seoProfileService.createLinkForCanonicalURL();
    this.seoProfileService.setMetaTags(DefaultConstant.metaData, DefaultConstant.metaData.meta_image);
    this.titleService.setTitle(this.metaOgTitle);
    this.meta.updateTag({ name: 'og:title', content: this.metaOgTitle });
    this.meta.updateTag({ name: 'description', content: this.metaDescription });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  loadHomeData() {
    this.homeService.getHomeData().subscribe(
      success => {
        this.homeData = success.result;
        this.browserStorageService.setLocalStorage('currencyType', this.homeData.user_currency);
        jQuery('.bannerImage:gt()').hide();
        this.sub = this.timer.subscribe(t => this.slideShow(t));
      },
      _fail => { }
    );
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.loadHomeData();
  }

  animateMe() {
    this.state = this.state === 'in' ? 'out' : 'in';
  }

  slideCity() {
    this.stateCity = this.stateCity === 'inCity' ? 'outCity' : 'inCity';
  }

  slideShow(_tick) {
    if (isPlatformBrowser(this.platFormId)) {
      jQuery('.bannerImage:first-child').fadeOut(1000).next('.bannerImage').fadeIn(1000).end().appendTo('#slideshow');
    }
  }

}
