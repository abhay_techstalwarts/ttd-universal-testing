import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { StaticPagesService, SEOProfileService } from '../shared/_services/index';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  privacyPolicyData: any;
  text: any;
  device: string;

  constructor(
    @Inject(WINDOW) private window: Window, 
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platformId: any,
    private browserStorageService: BrowserStorageService,
    private staticPagesService: StaticPagesService,
    private seoProfileService: SEOProfileService
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) { this.window.scrollTo(0, 0); }
    this.staticPagesService.getPrivacyPolicy().subscribe(
      success => {
        this.privacyPolicyData = success.result[0].page_content;
        this.seoProfileService
          .setSEOProfileDetails(success.result[0].page_id, 'pages', '')
          .subscribe(_success => { }, _fail => { });
      },
      _fail => { }
    );
  }
}
