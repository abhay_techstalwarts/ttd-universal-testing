import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit, ElementRef, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BrowserStorageService } from '../shared/_services';
import { ForgetpwdService } from '../shared/_services/index';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from './../shared/_helper/form-helper';
import { environment } from './../../environments/environment';


@Component({
  moduleId: module.id.toString(),
  selector: 'app-forget-password',
  templateUrl: 'forget-password.component.html'
})

export class ForgotPasswordComponent implements OnInit {
  text: any;
  returnUrl: string;
  changepwdUrl = `${environment.serverBaseUrl}/changepwd`;
  forgetpwdForm;
  forgetpwd = {
    'to': undefined,
    'name': 'user',
    'action_url': this.changepwdUrl,
    'support_url': 'https://support.ticketstodo.com/hc/en-us',
    'operating_system': '',
    'browser_name': ''
  };
  errors: any;
  success: boolean;
  @ViewChild('submitButton') submitButton: ElementRef;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private browserStorageService: BrowserStorageService,
    private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformID: any,
    private forgetpwdService: ForgetpwdService
  ) {
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformID)) {
      window.scrollTo(0, 0);
    }
    this.initiateForms();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
  }

  submitForetpwdEmail() {
    this.forgetpwd.to = this.forgetpwdForm.getValue('email');
    this.forgetpwd.action_url = this.changepwdUrl + '/' + this.forgetpwdForm.getValue('email');

    if (this.forgetpwdForm.isValid()) {
      FormHelper.disableButton(this.submitButton, 'SUBMITTING...');
      this.forgetpwdService.requestPwd(this.forgetpwd)
        .subscribe(
          _success => {
            FormHelper.enableButton(this.submitButton, 'SUBMIT');
            this.success = true;
          },
          error => {
            FormHelper.enableButton(this.submitButton, 'SUBMIT');
            if (error.message) {
              this.errors = error.message;
            }
          });
    }
  }

  initiateForms() {
    this.forgetpwdForm = new ValidationManager({
      'email': { rules: 'required|email' }
    });
  }

}
