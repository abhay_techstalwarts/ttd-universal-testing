import { Component, OnInit, Inject, PLATFORM_ID, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { ShoppingCartService, BrowserStorageService } from '../shared/_services';
import { AlertService, UserdataService, CartService, CountryService } from '../shared/_services/index';
import { Cart } from '../shared/_models/cart';
import { ValidationManager } from 'ng2-validation-manager';
import { environment } from './../../environments/environment';
import { FormHelper } from '../shared/_helper/form-helper';
import { DefaultConstant } from '../shared/_constants/default_constants';

declare let $: any;

@Component({
  selector: 'app-checkout',
  templateUrl: 'checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

export class CheckoutComponent implements OnInit {
  text: any;
  cart: Cart[];
  shopperDataForm: any;
  couponDetails: any;
  shopperdata: any;
  addtocartdatas: any;
  total_cost: any;
  paymentform;
  shopperId = '';
  currency = '';
  package_details = '';
  strCouponMsg = 'Enter your coupon to redeem your discount';
  hideCouponTextbox = false;
  coupon_code = '';
  phone_no = '';
  isLive= environment.isLive;
  payfortForm: any;
  rememberMe = false;
  @ViewChild('payButton') payButton: ElementRef;
  paypalFormUrl = environment.paymentBaseUrl + '/dopayment.php';
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  transactionCurrency: string;
  transactionAmount: number;
  payfortDataSubmitUrl = environment.payfortUrl;
  payfortLoading = false;
  ticketsCouponForm: any;
  ticketsCreditPoints: number;
  ticketsCoupon = { coupon: 'asdfg', couponAmount: 300 };
  numberVerify = false;
  countries: any[];
  grand_total: number;
  travelerDataForm: any;
  travelerEmail: any;
  travelerMobileNo: number;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platformID: any,
    private cartService: ShoppingCartService,
    private browserStorageService: BrowserStorageService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private userdataService: UserdataService,
    private cartdataService: CartService,
    private countryService: CountryService
  ) {
    this.shopperId = this.browserStorageService.getLocalStorage('shopperId');
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
    this.package_details = this.browserStorageService.getLocalStorage('package_details');
  }

  ngOnInit() {
    this.cartService.updateCartCount().subscribe(_success => { });
    if (isPlatformBrowser(this.platformID)) { window.scrollTo(0, 0); }
    this.initaiteForm();
    if (this.browserStorageService.getLocalStorage('package_details')) {
      this.loadCart();
      this.getShopperdata();
    } else {
      this.router.navigate(['/shoppingcart']);
    }
    this.coutryData();
    $('#loader').hide();

  }

  numberVerification() {
    this.numberVerify = !this.numberVerify;
  }

  ngOnDestroy(): void {
    if (this.browserStorageService.getLocalStorage('package_details')) {
      this.clearPackageDetails();
    }
  }

  getShopperdata() {
    this.userdataService.shopperData(this.shopperId).subscribe(
      success => {
        this.shopperdata = success.result[0];
        this.phone_no = this.shopperdata.phone;
        this.initializeForm(this.shopperdata);
      },
      _fail => { }
    );
  }

  loadPayfortForm(
    user_currency: string,
    transactionCurrency: string,
    payment_mode?: string,
    coupon_code?: string
  ) {
    this.userdataService
      .getPayfortQuickPayData(user_currency, transactionCurrency, payment_mode, coupon_code)
      .subscribe(
        success => {
          this.payfortForm = success.result;
          setTimeout(this.payfortDataForm, 1000);
          this.startLoading();
          $('#payfortModal').modal('show');
        },
        _fail => { }
      );
  }

  payfortDataForm() {
    const myForm = <HTMLFormElement>(
      document.getElementById('payfortDataSubmitForm')
    );
    myForm.submit();
  }

  startLoading() {
    this.payfortLoading = true;
  }

  endLoading() {
    this.payfortLoading = false;
  }

  submitPaymentForm(payment_mode: string) {
    if (this.phone_no === '') {
      this.alertService.error('Please enter phone no. and click update button.');
      return;
    }
    if (this.shopperDataForm.isValid() && this.travelerDataForm.isValid()) {
      if (this.paymentform.getValue('gid') === '1') {
        this.loadPayfortForm(
          this.currency,
          this.transactionCurrency,
          payment_mode,
          this.coupon_code
        );
      } else if (this.paymentform.getValue('gid') === '2') {
        FormHelper.enableButton(this.payButton, 'processing...');
        const paypalForm = <HTMLFormElement>(
          document.getElementById('paypalForm')
        );
        paypalForm.submit();
      }
    } else {
      let target;
      target = $('input.ng-invalid').first();
      if (target) {
        $('html,body').animate({ scrollTop: ($(target).offset().top - 150) }, 'slow', () => {
          target.focus();
        });
      }
    }
  }

  clearPackageDetails() {
    this.browserStorageService.emptyKeyLocalStorage('package_details');

  }

  editShopperdata() {
    if (this.shopperDataForm.isValid()) {
      this.userdataService
        .editshopperdata(this.shopperDataForm.getData())
        .subscribe(
          success => {
            this.alertService.success(success.error.message);
            this.phone_no = success.result.phone;
          },
          _fail => {
            this.alertService.error('Failed Updating User Details');
          }
        );
    }
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
    this.loadCart();
  }


  loadCart() {
    this.userdataService
      .addtocartData(this.shopperId, this.package_details, this.currency, this.coupon_code)
      .subscribe(
        success => {
          this.addtocartdatas = success.result;
          this.addtocartdatas.data.forEach(element => {
            element.group_price_details = element.group_price_details.filter(x => x.quantity > 0);
          });
          this.transactionCurrency = this.currency;
          this.transactionAmount = this.addtocartdatas.total_cost;
          this.grand_total = this.addtocartdatas.total_cost;
          this.paymentTypeChange(this.paymentform.getValue('gid'));
        },
        _fail => { }
      );
  }

  loadTransactionAmount(transactionCurrency: string) {
    this.userdataService
      .addtocartData(this.shopperId, this.package_details, transactionCurrency, this.coupon_code)
      .subscribe(
        success => {
          if (success.result && success.result.total_cost) {
            this.transactionAmount = success.result.total_cost.toFixed(2);
          }
        },
        _fail => { }
      );
  }

  paymentTypeChange(type) {
    if (type === '1') {
      // Payfort supports only AED
      if (this.currency === 'AED') {
        this.transactionCurrency = this.currency;
        this.transactionAmount = this.addtocartdatas.total_cost;
      } else {
        this.transactionCurrency = 'AED';
        this.loadTransactionAmount(this.transactionCurrency);
      }
    } else {
      // Paypal support only USD
      if (this.currency === 'USD') {
        this.transactionCurrency = this.currency;
        this.transactionAmount = this.addtocartdatas.total_cost;
      } else {
        this.transactionCurrency = 'USD';
        this.loadTransactionAmount(this.transactionCurrency);
      }
    }
  }

  remember(_value) {
    this.rememberMe = !this.rememberMe;
    this.userdataService
      .rememberMe(this.rememberMe, this.shopperId)
      .subscribe(_success => { }, _fail => { });
  }

  initaiteForm() {
    const pattern = new RegExp('^[0-9\\(\\)\\-\\+]{8,15}$');
    // tslint:disable-next-line: max-line-length
    const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const mobile_regex = /^(\+\d{1,3}[- ]?)?\d{8,15}$/;
    this.shopperDataForm = new ValidationManager({
      shopper_id: { rules: 'required|number' },
      title: { rules: '', value: '' },
      firstname: { rules: 'required' },
      middlename: { rules: '', value: '' },
      lastname: { rules: 'required' },
      email: { rules: 'required|email', value: '' },
      phone: { rules: 'required|pattern:' + pattern, value: '' },
      faxno: { rules: '', value: '' },
      address: { rules: '', value: '' },
      city: { rules: '', value: '' },
      state: { rules: '', value: '' },
      country: { rules: '', value: '' },
      zipcode: { rules: '', value: '' },
      remember_me: { rules: '' }
    });
    this.shopperDataForm.setErrorMessage('phone', 'pattern', 'Phone number can contain: [0-9()-+] with 8-15 digits');

    this.travelerDataForm = new ValidationManager({
      first_name: { rules: '', value: '' },
      family_name: { rules: '', value: '' },
      country: { rules: '', value: '' },
      email: { rules: 'required|pattern:' + email_regex, value: '' },
      country_code: { rules: '', value: '' },
      mobile_no: { rules: 'required|pattern:' + mobile_regex, value: '' },
      address: { rules: '', value: '' }
    });
    this.travelerDataForm.setErrorMessage('mobile_no', 'pattern', 'Phone number can contain: 0-9 with 8-15 digits');
    this.travelerDataForm.setErrorMessage('email', 'pattern', 'Please enter valid email');

    this.couponDetails = new ValidationManager({
      shopper_id: { rules: 'required', value: this.shopperId },
      currency: { rules: 'required', value: this.currency },
      coupon_code: { rules: 'required' }
    });

    this.paymentform = new ValidationManager({
      gid: { rules: 'required|number', value: '1' },
      shopper_id: { rules: 'required', value: this.shopperId },
      currency: { rules: 'required', value: this.currency },
      is_live: { rules: 'required', value: this.isLive }
    });
  }

  validateCoupon() {
    if (this.couponDetails.isValid()) {
      $('#loader').show();
      $('#redeemBtn').hide();
      this.cartdataService
        .validateCoupon(this.couponDetails.getData())
        .subscribe(
          success => {
            this.coupon_code = this.couponDetails.getValue('coupon_code');
            this.strCouponMsg = 'Coupon code entered is :';
            this.hideCouponTextbox = true;
            this.addtocartdatas = success.result;
            this.addtocartdatas.data.forEach(element => {
              element.group_price_details = element.group_price_details.filter(x => x.quantity > 0);
            });
            this.transactionCurrency = this.currency;
            this.transactionAmount = this.addtocartdatas.total_cost;
            this.grand_total = this.addtocartdatas.grand_total;
            this.paymentTypeChange(this.paymentform.getValue('gid'));
            this.alertService.success(success.error.message);
            $('#loader').hide();
          },
          _fail => {
            this.alertService.error(_fail.error.message);
            $('#loader').hide();
            $('#redeemBtn').show();
          }
        );
    }
  }

  removeCoupon() {
    this.coupon_code = '';
    this.couponDetails.setValue({
      shopper_id: this.shopperId,
      currency: this.currency,
      coupon_code: ''
    });
    this.loadCart();
    this.hideCouponTextbox = false;
    this.strCouponMsg = 'Enter your coupon to redeem your discount';
  }

  initializeForm(shopper: any) {
    this.rememberMe =
      Number(shopper.remember_me) === 1 && shopper.token_name ? true : false;
    this.shopperDataForm.setValue({
      shopper_id: shopper.shopper_id == null ? '' : shopper.shopper_id,
      title: shopper.title == null ? '' : shopper.title,
      firstname: shopper.firstname == null ? '' : shopper.firstname,
      middlename: shopper.middlename == null ? '' : shopper.middlename,
      lastname: shopper.lastname === 'null' ? '' : shopper.lastname,
      email: shopper.email == null ? '' : shopper.email,
      phone: shopper.phone == null ? '' : shopper.phone,
      faxno: shopper.faxno == null ? '' : shopper.faxno,
      address: shopper.address == null ? '' : shopper.address,
      city: shopper.shoppcityer_id == null ? '' : shopper.shoppcityer_id,
      state: shopper.state == null ? '' : shopper.state,
      country: shopper.country == null ? '' : shopper.country,
      zipcode: shopper.zipcode == null ? '' : shopper.zipcode,
      remember_me: this.rememberMe
    });
  }

  coutryData() {
    this.countryService.getCountries().subscribe(
      success => {
        this.countries = success.result;
      },
      _fail => { }
    );
  }

}
