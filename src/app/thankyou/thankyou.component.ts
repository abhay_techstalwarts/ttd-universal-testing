import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { ShoppingCartService } from '../shared/_services';
import { DomSanitizer } from '@angular/platform-browser';
import { DefaultConstant } from '../shared/_constants/default_constants';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  text: any;
  orderId: string;
  orderDetails: any;
  device;
  loading: boolean;
  trackingSrcUrl: string;
  userEmail;
  errorCode: any;
  user;
  shopperId: number;
  orderShopperId = 0;
  orderCreatedAt: string;
  tmpString: string;
  fontColor = '#89b383';
  bookingNumber: number;

  public pageContent: Array<Object> = [
    { id: 1, fontColor: '#d3cc00', imgURL: 'http://res.cloudinary.com/dcm/image/upload/v1561378624/prod/u/up-chevron.png', title: 'Confirmation in-progress', msg: 'Thank you for booking with TicketsToDo! Your booking is being confirmed with our partner. We will get back to you with your confirmed booking details in an email sent to <userEmail>' },
    { id: 2, fontColor: '#d84848', imgURL: 'http://res.cloudinary.com/dcm/image/upload/v1561378506/prod/x/x-mark.png', title: 'Booking Failed', msg: 'Thank you for booking with TicketsToDo! Unfortunately, your booking failed. Details of the issue have been outlined and sent to <userEmail>' },
    { id: 3, fontColor: '#89b383', imgURL: 'http://res.cloudinary.com/dcm/image/upload/v1561378458/prod/c/check-mark.png', title: 'Payment successful!', msg: 'Thank you for booking with TicketsToDo! Your order confirmation and e-voucher have been sent to <userEmail>' }
  ];
  pageContentIndex = 1;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private browserStorageService: BrowserStorageService,
    private route: ActivatedRoute,
    private cartService: ShoppingCartService,
    public domSanitizer: DomSanitizer
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
  }

  ngOnInit() {
    this.user = JSON.parse(this.browserStorageService.getLocalStorage('user'));
    this.shopperId = this.user.shopper_id;
    this.userEmail = this.user.email;

    this.getOrderId();
    this.getOrderDeatails();
    this.cartService.updateCartCount().subscribe(_success => {}, _error => {});
  }

  getOrderId() {
    this.orderId = this.route.snapshot.queryParamMap.get('order_id');
  }

  getOrderDeatails() {
    this.loading = true;
    this.cartService.getOrderDetails(this.orderId).subscribe(
      success => {
        this.loading = false;
        this.orderDetails = success.result.order_details;
        
        this.errorCode = success.error.code;
        let dcmTrackingUrl = DefaultConstant.dcmTrackingUrl;
        if (this.orderDetails && this.orderDetails[0] && dcmTrackingUrl && this.orderId) {
          this.bookingNumber = this.orderDetails[0].supplierOrders_id;
          this.orderShopperId = this.orderDetails[0].shopper_id;
          this.orderCreatedAt = this.orderDetails[0].created_at;
          if (this.errorCode == 200 || this.errorCode == "200") {
            if (this.orderDetails[0].transaction_state === 0) {
              this.pageContentIndex = 0;
            } else {
              this.pageContentIndex = 2;
            }

          } else {
            this.pageContentIndex = 1;
          }
          this.tmpString = this.pageContent[this.pageContentIndex]['msg'];
          this.tmpString = this.tmpString.replace('<userEmail>', this.userEmail);
          this.pageContent[this.pageContentIndex]['msg'] = this.tmpString;
          // dcmTrackingUrl = dcmTrackingUrl + 'adv_sub=' + this.orderId;
          // if (this.orderDetails[0].coupon_code) {
          //   dcmTrackingUrl = dcmTrackingUrl + '&adv_sub5=' + this.orderDetails[0].coupon_code + '||' + this.orderDetails[0].coupon_discount_amount;
          // } else {
          //   dcmTrackingUrl = dcmTrackingUrl + '&adv_sub5=NULL';
          // }
          // if (success.result.tracking_order_total) {
          //   dcmTrackingUrl = dcmTrackingUrl + '&amount=' + success.result.tracking_order_total;
          // }
          // this.trackingSrcUrl = dcmTrackingUrl;
        }
      },
      _fail => {
        this.loading = false;
        this.errorCode = _fail.error.code;
      }
    );
  }
}
