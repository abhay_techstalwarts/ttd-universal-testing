import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  ArticleComponent,
} from './index';
import { SharedModule } from '../shared/shared.module';
import { ArticleRouting } from './article-routing.module';


@NgModule({
  declarations: [
    ArticleComponent,
  ],
  imports: [
    ArticleRouting,
    CommonModule,
    SharedModule,
  ],
  exports: [
    ArticleComponent,
  ],
  providers: []
})
export class ArticleModule {

}
