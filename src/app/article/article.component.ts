import { Component, OnInit, Input, Inject, AfterViewInit } from '@angular/core';
import { PLATFORM_ID } from '@angular/core';
import {isPlatformBrowser } from '@angular/common';
import { CityService } from '../shared/_services';
import { ActivatedRoute, Router } from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';
declare let $: any;
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, AfterViewInit {
  @Input() aData;
  counter: number;
  selectedIndex: number;
  articleData: any;
  article_id: any;
  selectedArticle: any;
  allArticles: any;
  urlString: String;
  shareLink: any;

  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(PLATFORM_ID) private platformId: any,
    private route: ActivatedRoute,
    private cityService: CityService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadArticleData();
  }

  loadArticleData() {
   
    this.route.data.subscribe(success => {
      let data = success.articleData
      if (data.result) {
            this.selectedArticle =data.result[0];
          }
    });
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.window.scrollTo(0, 0);
      this.loadArticleList();
    }
  }
  

  loadArticleList(){
    this.cityService
              .getArticles(this.selectedArticle.city_id)
              .subscribe(article_list => {
                console.log({article_list});
                this.articleData = article_list.result;
    
                for (let index = 0; index < this.articleData.length; index++) {
                  if (this.article_id == this.articleData[index].article_id) {
                    this.articleData.splice(index, 1);
                  }
                }
              });
  }

  moreArticle(articleId, articleTitle: string, cityId) {
    // tslint:disable-next-line: prefer-const
    let titleRoute = articleTitle
      .replace(/\, /gi, '-')
      .replace(/\ /gi, '-')
      .replace(/\//gi, '-');
    this.router.navigate([]).then(result => {  window.open(`/article/${articleId}-${titleRoute}?city_id=${cityId}`, '_blank'); });;
    this.loadArticleData();
  }
}
