import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './chang-password.component';

@NgModule({
  declarations: [
    ChangePasswordComponent,
  ],
  imports: [
    ChangePasswordRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    ChangePasswordComponent,
  ],
  providers: []
})
export class ChangePasswordModule {

}
