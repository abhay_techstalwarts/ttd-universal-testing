import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ForgetpwdService,
  AuthenticationService,
  BrowserStorageService
} from '../shared/_services/index';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from './../shared/_helper/form-helper';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  text: any;
  returnUrl: string;
  changepwdForm;
  errors: any;
  email: string;
  notEqualToMessage: string;
  changepwdUrl = `${environment.apiBaseUrl}/changepwd`;
  changePwdObj = {
    email: this.route.snapshot.paramMap.get('email'),
    new_password: undefined,
    type: 'forgot'
  };
  @ViewChild('submitButton') submitButton: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private forgetpwdService: ForgetpwdService,
    private authenticationService: AuthenticationService,
    private browserStorageService: BrowserStorageService
  ) {}

  ngOnInit() {
    this.getEmail();
    this.initiateForms();
  }

  getEmail() {
    this.email = this.route.snapshot.paramMap.get('email');
  }

  submitChangepwd() {
    if (this.changepwdForm.isValid()) {
      this.changePwdObj.new_password = this.changepwdForm.getValue(
        'confirm_new_password'
      );
      FormHelper.disableButton(this.submitButton, 'SUBMITTING...');
      this.forgetpwdService.changePwd(this.changePwdObj).subscribe(
        _success => {
          FormHelper.enableButton(this.submitButton, 'SUBMIT');
          this.directLogin(
            this.changePwdObj.email,
            this.changePwdObj.new_password
          );
        },
        error => {
          FormHelper.enableButton(this.submitButton, 'SUBMIT');
          if (error.message) {
            this.errors = error.message;
          }
        }
      );
    }
  }

  directLogin(email: string, newpw: string) {
    const dataObj = {
      email: email,
      password: newpw,
      status: 1
    };
    this.authenticationService.login(dataObj).subscribe(
      success => {
        if (success.success) {
          this.browserStorageService.setLocalStorage(
            'user',
            JSON.stringify(success.result[0])
          );
          this.browserStorageService.setLocalStorage(
            'shopperId',
            JSON.stringify(success.result[0].shopper_id)
          );
          this.setCart(success.result[0]);
        }
      },
      error => {
        if (error.error.message) {
          this.errors = error.error.message;
        }
      }
    );
  }

  setCart(response: any) {
    this.browserStorageService.setLocalStorage(
      'cartCount',
      response.item_counts
    );
    this.browserStorageService.setLocalStorage(
      'shopperId',
      response.shopper_id
    );
    this.router.navigateByUrl('');
  }

  initiateForms() {
    this.changepwdForm = new ValidationManager({
      new_password: { rules: 'required|minLength:8' },
      confirm_new_password: { rules: 'required|equalTo:new_password' }
    });
  }
}
