﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/_guards/index';
import { ActivityResolve } from './shared/_resolver/activity-resolve.service';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { CityResolve } from './shared/_resolver/city-resolve';
import { ArticleResolve } from "./shared/_resolver/article-resolve.service";
import { ForgotPasswordComponent } from './forget-password/forget-password.component';
import { CheckoutComponent } from './checkout-page/checkout.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { PaymentfailComponent } from './paymentfail/paymentfail.component';
import { HomeComponent } from './home/index';
import { RecommendComponent } from './recommend/recommend.compnent';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'city/:cityUrl',
    loadChildren: 'app/city/city.module#CityModule',
    resolve: { cityData: CityResolve }
  },
  {
    path: 'article/:article_id',
    loadChildren: 'app/article/article.module#ArticleModule',
    resolve: { articleData: ArticleResolve }
  },
  {
    path: 'activity/:activityUrl',
    loadChildren: 'app/activity/activity.module#ActivityModule',
    resolve: { activityData: ActivityResolve }
  },
  {
    path: 'search',
    loadChildren: 'app/search-page/search-page.module#SearchPageModule'
  },
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: 'signup',
    loadChildren: 'app/signUp/sign-up.module#SignUpModule'
  },
  {
    path: 'forgetpwd',
    component: ForgotPasswordComponent
  },
  {
    path: 'contact',
    loadChildren: 'app/contact-us/contact-us.module#ContactUsModule'
  },
  {
    path: 'privacy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'terms',
    component: TermsConditionsComponent
  },
  {
    path: 'about-us',
    loadChildren: 'app/about-us/about-us.module#AboutUsModule'
  },
  {
    path: 'addtocart',
    component: RecommendComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'thankyou',
    component: ThankyouComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'paymentfail',
    component: PaymentfailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pay',
    component: CheckoutComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'shoppingcart',
    loadChildren: 'app/shopping-cart/shopping-cart.module#ShoppingCartModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'changepwd/:email',
    loadChildren:
      'app/change-password/change-password.module#ChangePasswordModule'
  },
  {
    path: 'voucher',
    loadChildren: 'app/voucher/voucher.module#VoucherModule'
  },
  {
    path: 'home',
    loadChildren: 'app/user-account/user-account.module#UserAccountModule'
  },
  // otherwise redirect to layout
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      initialNavigation: 'enabled',
      useHash: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
