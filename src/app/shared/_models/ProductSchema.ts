export const ProductSchema = {
    '@context': 'http://schema.org/',
    '@type': 'Product',
    'name': '',
    'mpn': '',
    'sku': '',
    'description': '',
    'image': [],
    'brand': {
        '@type': 'Thing',
        'name': 'TicketsToDo'
    },
    'aggregateRating': {
        '@type': 'AggregateRating',
        'ratingValue': '4.5',
        'ratingCount': '1000',
        'bestRating': '5',
        'worstRating': '1'
    },
    'review': {
        '@type': 'Review',
        'reviewRating': {
            '@type': 'Rating',
            'ratingValue': '5'
        },
        'name': '',
        'author': {
            '@type': 'Person',
            'name': 'User'
        },
        'datePublished': '',
        'reviewBody': ''
    },
    'offers': {
        '@type': 'Offer',
        'url': '',
        'priceCurrency': '',
        'price': '',
        'priceValidUntil': '',
        'itemCondition': 'http://schema.org/UsedCondition',
        'availability': 'http://schema.org/InStock'
    }
};
