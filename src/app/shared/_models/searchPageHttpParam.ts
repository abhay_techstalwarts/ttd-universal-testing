export class HttpParam {
  search: string;
  category_id: string;
  city_id: string;
  sort_by: string;
  price_from: number;
  price_to: number;
}
