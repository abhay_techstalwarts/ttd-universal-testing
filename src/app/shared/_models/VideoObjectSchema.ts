export const VideoObjectSchema = {
    '@context': 'http://schema.org',
    '@type': 'VideoObject',
    'name': '',
    'description': '',
    'thumbnailUrl': '',
    'uploadDate': '',
    'publisher': {
        '@type': 'Organization',
        'name': 'TicketsToDo',
        'logo': {
            '@type': 'ImageObject',
            'url': 'https://res.cloudinary.com/dcm/image/upload/h_30,w_140/v1/prod/t/ttdlogo.png',
            'width': 140,
            'height': 30
        }
    },
    'contentUrl': ''
};
