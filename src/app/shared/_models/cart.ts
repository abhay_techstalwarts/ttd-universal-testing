import {environment} from './../../../environments/environment';


export class Cart {
  package_id: number;
  currency: string;
  quantity: number;
  adult = 0;
  children: number;
  total_participants: number;
  discount = 0;
  booking_activity_date: Date;
  booking_activity_time: any;
  adult_activity_count: number;
  children_activity_count: number;
  tracking_url = environment.apiBaseUrl + '/addtocart';
}
