export class Pages {
  constructor(public page_id: number,
              public page_title: string,
              public page_url: string,
              public page_content: string,
              public redirect_url: string,
              public meta_content: string,
              public meta_keyword: string,
              public meta_title: string,
              public meta_description: string,
              public status: number) {
  }

}
