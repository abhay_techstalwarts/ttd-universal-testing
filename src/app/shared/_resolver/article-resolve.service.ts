import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import {
  Response,
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { ApiHelperService } from '../_services/api-helper.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ArticleResolve implements Resolve<any> {
  constructor(private http: HttpClient,private apiHelperService:ApiHelperService,@Inject(PLATFORM_ID) private platFormId: any) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot
    ): Observable<any>|Promise<any>|any {
   
    const article_id = route.params.article_id.split('-')[0];
    let url:any;
    
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = {
      headers: headers
    };

    url =`${environment.apiBaseUrl}/articles/` + article_id;;
    console.log(url);
    
    return this.http.get(url, options).pipe(
      map(response =>(response)),
      catchError(this.handleError)
    );

  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}