import { throwError as observableThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ApiHelperService } from '../_services/api-helper.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class CityResolve implements Resolve<any> {
  constructor(private http: Http,private apiHelperService:ApiHelperService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.cityUrl.split('-')[0];
    const url = this.apiHelperService.getApiUrl(`citypage/${id}`);

    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
