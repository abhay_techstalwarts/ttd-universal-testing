import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import {
  Response,
} from '@angular/http';
import { isPlatformServer } from '@angular/common';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { ApiHelperService } from '../_services/api-helper.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ActivityResolve implements Resolve<any> {
  activityState:any;
  constructor(private http: HttpClient,private apiHelperService:ApiHelperService,@Inject(PLATFORM_ID) private platFormId: any) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot
    ): Observable<any>|Promise<any>|any {
   
    const id = route.params.activityUrl.split('-')[0];
    let url:any;
    
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = {
      headers: headers
    };

    if (isPlatformServer(this.platFormId)) {
      url = this.apiHelperService.getApiUrl(`activities_new/${id}`);
    } else {
      url = this.apiHelperService.getApiUrl(`activitypackages/${id}`);
    }
    
    return this.http.get(url, options).pipe(
      map(response =>(response)),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}