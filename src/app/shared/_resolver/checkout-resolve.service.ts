import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { throwError as observableThrowError } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Resolve } from '@angular/router';
import { environment } from './../../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { AffiliateService } from "../_services/affiliate.service";

@Injectable()
export class CheckoutResolve implements Resolve<any> {
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private http: Http,private affiliateService : AffiliateService) {}

  resolve() {
    const shopperId = this.localStorage.getItem('shopperId');
    const package_details = this.localStorage.getItem('package_details');
    const currencyType = this.localStorage.getItem('currencyType');
    let data = {
      shopper_id: shopperId,
      package_details: JSON.parse(package_details),
      currency: currencyType,
      user_currency: currencyType,
      is_live: environment.isLive,
      gid: 1
    };
    data = this.affiliateService.bindAffiliateData(data);
    const url = `${environment.apiBaseUrl}/payfort_check_out?payment_mode=`;
    if (package_details) {
      return this.http.post(url, data).pipe(
        map(response => response.json()),
        catchError(this.handleError)
      );
    }
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
