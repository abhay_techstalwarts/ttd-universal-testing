import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ApiHelperService } from '../_services/api-helper.service';

@Injectable()
export class AddToCartResolve implements Resolve<any> {

  constructor(private httpClient: HttpClient,private apiHelperService:ApiHelperService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.activityUrl.split('-')[0];
    const url = this.apiHelperService.getApiUrl(`activitypackages/${id}`);
    return this.httpClient.get(url);
  }
}
