import { ActivatedRoute } from '@angular/router';

export class CommonHelper {
  static getBannerUrl(bannerUrl: string) {
    return bannerUrl.substr(bannerUrl.lastIndexOf('/') - 1);
  }

  static getActiveUrl(route: ActivatedRoute) {
    let returnUrl = '';
    for (let i = 0; i < route.snapshot.url.length; i++) {
      returnUrl = returnUrl + '/' + route.snapshot.url[i];
    }
    return returnUrl;
  }

  static dateFormater(date) {
    let dd = date.getDate();
    let mm = date.getMonth() + 1; // January is 0!
    const yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    date = dd + '/' + mm + '/' + yyyy;
    return date;
  }

  static dateFormaterNew(date) {
    let dd = date.getDate();
    let mm = date.getMonth() + 1; // January is 0!
    const yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    date = String(yyyy).replace(/\D/g, '') + '-' + String(mm).replace(/\D/g, '') + '-' + String(dd).replace(/\D/g, '');
    return date;
  }

  static stringToDateFormater(date) {
    return this.dateFormaterNew(new Date(date));
  }

  static extractStartTime(time) {
    return time ? time.substr(0, time.indexOf('-')) : null;
  }
}
