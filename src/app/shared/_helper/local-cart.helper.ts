import { Inject } from '@angular/core';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
export class LocalCartHelper {
  static cart = [];

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, ) {
  }

  static addItem(item) {
    localStorage.setItem('cart_details', JSON.stringify(item));
  }

  static getCart(): any {
    this.cart = JSON.parse(localStorage.getItem('cart_details'));
    return this.cart;
  }

  static deleteCart() {
    localStorage.removeItem('cart_details');
  }

  static deleteItem(index) {
    if (JSON.parse(localStorage.getItem('cart_details'))) {
      this.cart = JSON.parse(localStorage.getItem('cart_details'));
    }
    this.cart.splice(index, 1);
    localStorage.setItem('cart_details', JSON.stringify(this.cart));
    return this.cart;
  }
}
