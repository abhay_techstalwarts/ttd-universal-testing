import { BrowserStorageService } from './../_services/browserStorage.service';
import { Inject,PLATFORM_ID } from '@angular/core';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { isPlatformBrowser } from '@angular/common';

export class CurrencyHelper {
 constructor(@Inject(LOCAL_STORAGE) private localStorage: any,private browserStorageService:BrowserStorageService,@Inject(isPlatformBrowser)
 private platformId:any
 ) {}


  static getCurrency() {
    return localStorage.getItem('currencyType');
  }
}
