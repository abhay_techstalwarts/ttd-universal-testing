import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  OnDestroy,
  PLATFORM_ID,
  HostListener
} from '@angular/core';
import { Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Subscription } from 'rxjs';

import { BrowserStorageService } from './../../_services/browserStorage.service';
import { AuthenticationService } from './../../_services/authentication.service';
import { UserService } from '../../../shared/_subjects/user.subject';
import {
  CurrencyService,
  ShoppingCartService,
  ActivityService,
} from '../../../shared/_services/index';
import { DefaultConstant } from '../../_constants/default_constants';
import { environment } from '../../../../environments/environment';

declare let jQuery: any;
@Component({
  moduleId: module.id.toString(),
  selector: 'app-header',
  templateUrl: 'header-desktop.component.html',
  styleUrls: ['./header-desktop.component.css']
})
export class HeaderDesktopComponent implements OnInit, OnChanges, OnDestroy {
  @Input() text;
  @Input() noOfItem;
  @Input() requestedCurrency = '';
  @Input() navFor = '';
  @Input() cityName;
  @Input() cityId;
  @Output() currency: EventEmitter<string> = new EventEmitter();

  whiteNav = true;
  cartCount: string;
  blogUrl = DefaultConstant.blogUrl;

  user;
  currencies;
  subscription: Subscription;
  updateUsersubscription: Subscription;
  hideCurrencies: boolean;
  cartItems: any;
  recentActivity = [];

  loadingCart = true;
  currencyType = '';
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  showKidzappLogo = false;
  whatsapp = DefaultConstant.whatsapp;


  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private userService: UserService,
    private browserStorageService: BrowserStorageService,
    private currencyService: CurrencyService,
    private authenticationService: AuthenticationService,
    private shoppingCartService: ShoppingCartService,
    private activityService: ActivityService,
    @Inject(PLATFORM_ID) private platFormId: any
  ) {

    const user = this.browserStorageService.getLocalStorage('user');
    this.currencyType = this.browserStorageService.getLocalStorage('currencyType');
    
    if (isPlatformBrowser(this.platFormId)) {
      this.subscription = this.userService.clearUser().subscribe(() => {
        if (user) { this.user = JSON.parse(user); }
      });
      this.updateUsersubscription = this.userService
        .modifyUser()
        .subscribe(() => {
          if (user) { this.user = JSON.parse(user); }
        });
    }
  }

  ngOnInit() {
    if(this.navFor == 'home'){
      this.whiteNav = false;
    }

    this.showKidzappLogo = false;

    if (isPlatformBrowser(this.platFormId)) {
      const user = this.browserStorageService.getLocalStorage('user');
      if (user) { this.user = JSON.parse(user); }
    }

    this.getCartCount();
    this.loadRecentActivityCard();
    
    if (this.browserStorageService.getLocalStorage('from_kidzapp')) {
      const from_kidzapp = this.browserStorageService.getLocalStorage('from_kidzapp');
      if (from_kidzapp && from_kidzapp === 'true') {
        this.showKidzappLogo = true;
      } else {
        this.showKidzappLogo = false;
      }
    } else {
      this.showKidzappLogo = false;
    }
  }

  ngOnDestroy() {
    if (this.updateUsersubscription) {
      this.updateUsersubscription.unsubscribe();
    }
  }

  ngOnChanges() {
    if (this.text === 'reload header') {
      this.getCartCount(1);
    } else {
      this.getCartCount();
    }
  }

  @HostListener('window:scroll', ['$event'])
  onScrollEvent(_$event) {
    if (isPlatformBrowser(this.platFormId)) {
      const main = jQuery('.main');
      if(!main || !(main && main.length)){
        return
      }

      const y = jQuery(document).scrollTop();
      const sreenWidth = jQuery(window).width();
      const t = main.offset().top !== undefined ? main.offset().top : 0;

      if (y > t && sreenWidth > 768) {
        this.whiteNav = true;
      } else {
        this.whiteNav = false;
      }
    }
  }

  loadCurrencies() {
    if (!this.currencies) {
      this.getCurrency();
    }
  }

  getCartCount(count = 0) {
    if (count > 0){
      let newCount: any;
      newCount = parseInt(this.cartCount) - count;

      this.browserStorageService.emptyKeyLocalStorage('cartCount');
      this.browserStorageService.setLocalStorage(
        'cartCount',
        newCount
      );
    }
    this.cartCount = this.browserStorageService.getLocalStorage('cartCount');
  }

  logout() {
    this.authenticationService.logout();
    this.userService.clearUser();
    this.user = null;
  }

  getCurrency() {
    this.currencyService.getCurrencies().subscribe(success => {
      this.currencies = success.result;
    });
  }

  updateCurrency(currency: string) {
    this.currency.emit(currency);
  }

  loadCart() {
    this.loadingCart = true;
    if (this.user) {
      if (this.browserStorageService.getLocalStorage('shopperId') === '0') {
      } else {
        this.shoppingCartService
          .getCartData(this.browserStorageService.getLocalStorage('shopperId'))
          .subscribe(
            success => {
              this.loadingCart = false;
              this.cartItems = success.result;
            },
            _fail => {
              this.loadingCart = false;
            }
          );
      }
    } else {
      jQuery('.popover__content').css('display', 'none');
    }
  }

  loadRecentActivityCard() {
    this.activityService.getRecentActivity().subscribe(data=>{
      this.recentActivity = data;
    });

  }
}
