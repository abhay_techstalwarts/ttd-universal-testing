import { Component, Input } from '@angular/core';
import { DefaultConstant } from '../../_constants/default_constants';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() cData;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;

  constructor() {}
}
