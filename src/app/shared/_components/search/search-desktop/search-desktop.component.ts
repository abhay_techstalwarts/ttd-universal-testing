import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Input , Inject,PLATFORM_ID} from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../../../shared/_services/index';
import * as algoliasearch from 'algoliasearch';
import * as _find from 'lodash/find';
import { isPlatformBrowser } from '@angular/common';

declare let $: any;

@Component({
  selector: 'app-search-desktop',
  templateUrl: './search-desktop.component.html',
  styleUrls: ['./search-desktop.component.css']
})
export class SearchDesktopComponent implements OnInit {
  @Input() dropDown: boolean;
  @Input() loadRegions: boolean;
  showInitialDropdown = false;
  searchData;
  showCities: boolean;
  regions = new Array();
  showSearchList: boolean;
  searchKeyword: string;
  client = algoliasearch('V9ZTOAFXXQ', '89ae2b73c29b1eb98f5124afa688314d');
  index = this.client.initIndex('prod_TTD');
  loadCities = false;

  constructor(@Inject(WINDOW) private window: Window, private searchService: SearchService, 
  @Inject(PLATFORM_ID) private platformID:any,
  private router: Router) {}

  ngOnInit() {}

  loadSearchData(searchKeyword) {
    if (searchKeyword && searchKeyword.length > 1) {
      this.searchService.search(searchKeyword).subscribe(
        success => {
          if (success.result) {
            this.searchData = success.result;
          } else {
            this.searchData = null;
            this.algoliaSer(searchKeyword);
          }
        },
        _fail => {
          this.searchData = null;
        }
      );
    } else if (!searchKeyword) {
      this.searchData = null;
    }
  }

  algoliaSer(searchKeyword) {
    this.searchData = this.index
      .search({
        query: searchKeyword,
        filters: 'status = 1'
      })
      .then(res => {
        this.searchData = res.hits;
      });
  }

  goToSearchPage() {
    if (this.searchKeyword && this.searchKeyword.length > 1) {
      const searchkey = this.searchKeyword.toLowerCase();
      const obj = _find(this.searchData, function(data) {
        return data.name.toLowerCase() === searchkey;
      });
      if (obj && obj.search_type && (obj.search_type === 'city' || obj.search_type === 'activity')) {
        const url = obj.search_type + '/' + obj.url;
        this.goTo(url);
      } else {
        this.searchKeyword = this.searchKeyword.replace(/ /g, '+');
        $('#exploreDestination').modal('hide');
        this.router.navigate(['search', this.searchKeyword], {
          queryParams: { search: this.searchKeyword }
        });
      }
    }
  }

  goTo(url: any) {
    $('#exploreDestination').modal('hide');
    this.router.navigateByUrl(url);
  }

  goTop() {
    if(isPlatformBrowser(this.platformID))
      this.window.scrollTo(0, 250);
  }
}
