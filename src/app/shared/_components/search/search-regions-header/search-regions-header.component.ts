import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Inject,
  PLATFORM_ID,
  SimpleChanges
} from '@angular/core';
import { CityService } from '../../../../shared/_services/index';
import { environment } from './../../../../../environments/environment';
import { DefaultConstant } from '../../../../shared/_constants/default_constants';
import * as _filter from 'lodash/filter';
import * as _sortBy from 'lodash/sortBy';
import * as _slice from 'lodash/slice';

import { isPlatformBrowser } from '@angular/common';

declare let $: any;

@Component({
  selector: 'app-search-desktop-regions-header',
  templateUrl: './search-regions-header.component.html',
  styleUrls: ['./search-regions-header.component.css']
})
export class SearchDesktopRegionsHeaderComponent implements OnInit, OnChanges {
  @Input()
  loadCities;
  popularCities: object;
  regions = new Array();
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;

  constructor(
    private cityService: CityService,
    @Inject(PLATFORM_ID) private platformId: any
  ) {}

  ngOnInit() {}

  ngOnChanges(_changes: SimpleChanges) {
    if (this.loadCities === true && !this.popularCities) {
      this.loadRegions();
      this.loadPopularCities();
    }
  }

  loadPopularCities() {
    this.cityService.getPopularCities().subscribe(
      success => {
        this.popularCities = success.result;
      },
      _fail => {
        alert({
          type: 'error',
          text: 'Unable to load popular ceties'
        });
      }
    );
  }

  loadRegions() {
    this.cityService.getRegions().subscribe(
      success => {
        const allRegions = success.result.data;
        this.regions = _filter(allRegions, ['status', 1]);
        this.regions = _sortBy(this.regions, ['sequence']);
        this.regions.forEach(region => {
          region.countries = _filter(region.countries, ['status', 1]);
          region.countries.forEach(country => {
            country.cities = _filter(country.cities, ['status', 1]);
            country.cities = _sortBy(country.cities, ['rank']);
          });
        });
      },
      _fail => {
        alert({
          type: 'error',
          text: 'Unable to load regions'
        });
      }
    );
  }

  filterRegions(apiRegions) {
    for (let i = 0; i < apiRegions.length; i++) {
      if (apiRegions[i].countries && apiRegions[i].countries.length >= 1) {
        this.regions.push(apiRegions[i]);
      }
    }
  }

  openCity(cityName) {
    let i = 0;
    let tabcontent = [];
    let tablinks = [];
    tabcontent = $('.tabcontentH');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }
    tablinks = $('.tablinksH');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace('active', '');
    }
    if (isPlatformBrowser(this.platformId)) {
      document.getElementById(cityName).style.display = 'block';
    }
  }

  filterPopularCities(cities) {
    const popularCities = _filter(cities, ['ispopular', 1]);
    return _slice(popularCities, 0, 2);
  }
}
