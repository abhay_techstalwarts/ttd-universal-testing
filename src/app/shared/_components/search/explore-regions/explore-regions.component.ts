import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';
import { CityService } from '../../../../shared/_services/index';
import * as _filter from 'lodash/filter';
import * as _sortBy from 'lodash/sortBy';
import * as _slice from 'lodash/slice';

declare let $: any;

@Component({
  selector: 'app-explore-desktop-regions',
  templateUrl: './explore-regions.component.html',
  styleUrls: ['./explore-regions.component.css']
})
export class ExploreDesktopRegionsComponent implements OnInit, OnChanges {
  @Input()
  loadCities;
  popularCities: object;
  regions = new Array();

  constructor(private cityService: CityService, private router: Router) {}

  ngOnInit() {}

  ngOnChanges(_changes: SimpleChanges) {
    if (this.loadCities === true && !this.popularCities) {
      this.loadRegions();
    }
  }

  loadRegions() {
    this.cityService.getRegions().subscribe(
      success => {
        const allRegions = success.result.data;
        this.regions = _filter(allRegions, ['status', 1]);
        this.regions = _sortBy(this.regions, ['sequence']);
        this.regions.forEach(region => {
          region.countries = _filter(region.countries, ['status', 1]);
          region.countries.forEach(country => {
            country.cities = _filter(country.cities, ['status', 1]);
            country.cities = _sortBy(country.cities, ['rank']);
          });
        });
        setTimeout(() => {
          this.openCity(this.regions[0].region_id);
        }, 300);
      },
      _fail => {
        alert({
          type: 'error',
          text: 'Unable to load regions'
        });
      }
    );
  }

  filterRegions(apiRegions) {
    for (let i = 0; i < apiRegions.length; i++) {
      if (apiRegions[i].countries && apiRegions[i].countries.length >= 1) {
        this.regions.push(apiRegions[i]);
      }
    }
    setTimeout(() => {
      this.openCity(this.regions[0].region_id);
    }, 1000);
  }

  openCity(regionId) {
    let i, tabcontent2, tablinks2;
    tabcontent2 = $('.tabcontent2');
    for (i = 0; i < tabcontent2.length; i++) {
      tabcontent2[i].style.display = 'none';
    }
    tablinks2 = $('.tablinks2');
    for (i = 0; i < tablinks2.length; i++) {
      tablinks2[i].className = tablinks2[i].className.replace('active', '');
    }
    const region = $('#' + regionId);
    region[0].style.display = 'block';
  }

  goTo(url: any) {
    this.router.navigateByUrl(url);
    $('#exploreDestination').modal('hide');
  }

  filterPopularCities(cities) {
    const popularCities = _filter(cities, ['ispopular', 1]);
    return _slice(popularCities, 0, 2);
  }
}
