import { Component, OnInit } from '@angular/core';
import { SearchService, CityService } from '../../../../shared/_services/index';
import { Router } from '@angular/router';
import { AlertService } from '../../../../shared/_services/alert.service';
import * as algoliasearch from 'algoliasearch';

@Component({
  selector: 'app-search-mobile',
  templateUrl: './search-mobile.component.html',
  styleUrls: ['./search-mobile.component.css']
})
export class SearchMobileComponent implements OnInit {
  popularCities: object;
  showInitialDropdown = false;
  searchData;
  showCities: boolean;
  searchKeyword: string;
  client = algoliasearch('V9ZTOAFXXQ', '89ae2b73c29b1eb98f5124afa688314d');
  index = this.client.initIndex('prod_TTD');

  constructor(private cityService: CityService,
              private searchService: SearchService,
              private alertService: AlertService,
              private router: Router) {
  }

  ngOnInit() {
    this.loadPopularCities();
  }

  loadPopularCities() {
    this.cityService.getPopularCities()
      .subscribe(
        success => {
          this.popularCities = success.result;
        },
        _fail => {
          this.alertService.warning('404 Data not found');
        }
      );
  }

  loadSearchData(searchKeyword) {
    if (searchKeyword && searchKeyword.length > 1) {
      this.searchService.search(searchKeyword)
      .subscribe(
        success => {
          if (success.result) {
            this.searchData = success.result;
          } else {
            this.searchData = [];
            this.algoliaSer(searchKeyword);
          }
        },
        _fail => {
          this.searchData = [];
        }
      );
    } else {
      this.searchData = [];
    }
  }

  algoliaSer(searchKeyword) {
    if (searchKeyword.length %  3 === 0) {
      this.searchData = this.index.search({
        query: searchKeyword,
        filters: 'status = 1'
      }).then(res => {
        this.searchData = res.hits;
      });
    }
  }

  goToSearchPage() {
    if (this.searchKeyword) {
      this.router.navigate(['/search', this.searchKeyword], {queryParams: {search: this.searchKeyword}});
    }
  }

}
