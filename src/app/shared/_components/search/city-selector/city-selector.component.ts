import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges
} from '@angular/core';
import { CityService } from '../../../../shared/_services/index';
import { alert } from 'notie';
import * as _sortBy from 'lodash/sortBy';
import * as _filter from 'lodash/filter';

declare let $: any;

@Component({
  selector: 'app-city-selector',
  templateUrl: './city-selector.component.html',
  styleUrls: ['./city-selector.component.css']
})
export class CitySelectorComponent implements OnInit, OnChanges {
  showInitialDropdown = false;
  searchData;
  showCities: boolean;
  regions = new Array();
  showSearchList: boolean;
  searchKeyword: string;
  popularCities: object;
  @Output() city: EventEmitter<string> = new EventEmitter();
  @Input() defaultCityValue = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.loadRegions();
  }

  ngOnChanges() {
    if (this.defaultCityValue === 'clear') {
      this.searchKeyword = '';
    }
  }

  loadRegions() {
    this.cityService.getRegions().subscribe(
      success => {
        const allRegions = success.result.data;
        this.regions = _filter(allRegions, ['status', 1]);
        this.regions = _sortBy(this.regions, ['sequence']);
        this.regions.forEach(region => {
          region.countries = _filter(region.countries, ['status', 1]);
          region.countries.forEach(country => {
            country.cities = _filter(country.cities, ['status', 1]);
            country.cities = _sortBy(country.cities, ['rank']);
          });
        });
      },
      _fail => {
        alert({
          type: 'error',
          text: 'Unable to load regions'
        });
      }
    );
  }

  select(city) {
    this.searchKeyword = city;
    this.city.emit(this.searchKeyword);
  }

  filterRegions(apiRegions) {
    for (let i = 0; i < apiRegions.length; i++) {
      if (apiRegions[i].countries && apiRegions[i].countries.length >= 1) {
        this.regions.push(apiRegions[i]);
      }
    }
    setTimeout(() => {
      this.openCity(this.regions[0].region_id);
    }, 1000);
  }

  openCity(regionId) {
    let i, tabcontent2, tablinks2;
    tabcontent2 = $('.tabcontent2');
    for (i = 0; i < tabcontent2.length; i++) {
      tabcontent2[i].style.display = 'none';
    }
    tablinks2 = $('.tablinks2');
    for (i = 0; i < tablinks2.length; i++) {
      tablinks2[i].className = tablinks2[i].className.replace('active', '');
    }
    const region = $('#' + regionId);
    region[0].style.display = 'block';
  }
}
