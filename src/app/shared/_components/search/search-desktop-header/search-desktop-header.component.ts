import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../../../shared/_services/index';
import * as algoliasearch from 'algoliasearch';

declare let $: any;

@Component({
  selector: 'app-search-desktop-header',
  templateUrl: './search-desktop-header.component.html',
  styleUrls: ['./search-desktop-header.component.css']
})
export class SearchDesktopHeaderComponent implements OnInit {
  @Input() dropDown: boolean;
  @Input() cityName: string;
  @Input() cityId: number;
  showInitialDropdown = false;
  searchData;
  showCities: boolean;
  regions = new Array();
  showSearchList: boolean;
  searchKeyword: string;
  client = algoliasearch('V9ZTOAFXXQ', '89ae2b73c29b1eb98f5124afa688314d');
  index = this.client.initIndex('prod_TTD');
  loadCities = false;

  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit() {}

  loadSearchData(searchKeyword) {
    if (searchKeyword && searchKeyword.length > 1) {
      this.searchService.search(searchKeyword, this.cityId).subscribe(
        success => {
          if (success.result) {
            this.searchData = success.result;
          } else {
            this.searchData = null;
            this.algoliaSer(searchKeyword, this.cityId);
          }
        },
        _fail => {
          this.searchData = null;
        }
      );
    } else if (!this.searchKeyword) {
      this.searchData = null;
    }
  }

  algoliaSer(searchKeyword, cityId?: number) {
    this.searchData = this.index
      .search({
        query: searchKeyword,
        filters: (cityId && cityId > 0) ? 'status = 1 AND city_id = ' + cityId : 'status = 1'
      })
      .then(res => {
        this.searchData = res.hits;
      });
  }

  goToSearchPage() {
    if (this.searchKeyword) {
      this.searchKeyword = this.searchKeyword.replace(/ /g, '+');
      $('#exploreDestination').modal('hide');
      this.router.navigate(['search', this.searchKeyword], {
        queryParams: { search: this.searchKeyword }
      });
    }
  }

  goTo(url: any) {
    $('#exploreDestination').modal('hide');
    this.router.navigate([url]);
  }
}
