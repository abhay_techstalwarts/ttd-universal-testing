﻿import { Component } from '@angular/core';
@Component({
  moduleId: module.id.toString(),
  selector: 'app-loading',
  templateUrl: 'loading.component.html',
  styleUrls: ['./loading.component.css']
})

export class LoadingComponent {
  message: any;
  loadingImg: any;

  constructor() {
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.loadingImg ='assets/images/TTDfavicon.gif';
    }, 1000);
  }

}
