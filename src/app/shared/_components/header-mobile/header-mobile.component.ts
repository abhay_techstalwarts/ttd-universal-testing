﻿import { BrowserStorageService } from './../../_services/browserStorage.service';
import { Component, OnInit, PLATFORM_ID, Inject, OnChanges, OnDestroy } from '@angular/core';
import { DefaultConstant } from '../../_constants/default_constants';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';
import { UserService } from '../../_subjects/user.subject';

@Component({
  selector: 'app-header-mobile',
  templateUrl: 'header-mobile.component.html',
  styleUrls: ['./header-mobile.component.css']
})

export class HeaderMobileComponent implements OnInit, OnChanges, OnDestroy {
  user;
  cartCount: string;
  subscription: Subscription;
  updateUsersubscription: Subscription;
  environment = environment;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  showKidzappLogo = false;

  constructor(private browserStorageService: BrowserStorageService,
    @Inject(PLATFORM_ID) private platFormId: any,
    private userService: UserService
  ) {
    const user = this.browserStorageService.getLocalStorage('user');
    if (isPlatformBrowser(this.platFormId)) {
      this.subscription = this.userService.clearUser().subscribe(() => {
        if (user) { this.user = JSON.parse(user); }
      });
      this.updateUsersubscription = this.userService
        .modifyUser()
        .subscribe(() => {
          if (user) { this.user = JSON.parse(user); }
        });
    }
  }

  ngOnInit() {
    this.showKidzappLogo = false;
    if (isPlatformBrowser(this.platFormId)) {
      const user = this.browserStorageService.getLocalStorage('user');
      if (user) { this.user = JSON.parse(user); }
    }
    this.getCartCount();
    if (this.browserStorageService.getLocalStorage('from_kidzapp')) {
      const from_kidzapp = this.browserStorageService.getLocalStorage('from_kidzapp');
      if (from_kidzapp && from_kidzapp === 'true') {
        this.showKidzappLogo = true;
      } else {
        this.showKidzappLogo = false;
      }
    } else {
      this.showKidzappLogo = false;
    }
  }

  ngOnDestroy() {
    if (this.updateUsersubscription) {
      this.updateUsersubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnChanges() {
    this.getCartCount();
  }

  getCartCount() {
    this.cartCount = this.browserStorageService.getLocalStorage('cartCount');
  }
}
