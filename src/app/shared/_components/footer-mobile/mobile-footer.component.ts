﻿﻿import { isPlatformBrowser } from '@angular/common';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Inject,
  PLATFORM_ID
} from '@angular/core';
import { DefaultConstant } from '../../_constants/default_constants';

import {
  CurrencyService,
  BrowserStorageService
} from '../../../shared/_services/index';
@Component({
  moduleId: module.id.toString(),
  selector: 'app-footer-mobile',
  templateUrl: 'mobile-footer.component.html',
  styleUrls: ['./mobile-footer.component.css']
})
export class MobileFooterComponent implements OnInit {
  @Input() requestedCurrency;
  user;
  currencies: any[];
  @Output() currency: EventEmitter<string> = new EventEmitter();
  currencyType = '';
  askTTDUrl = DefaultConstant.askTTDUrl;


  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platFormId: any,
    private currencyService: CurrencyService,
    private browserStorageService: BrowserStorageService,
  ) {
  }

  ngOnInit() {
    this.currencyType = this.browserStorageService.getLocalStorage('currencyType');
    this.requestedCurrency = this.currencyType;
    const user = this.browserStorageService.getLocalStorage('user');
    if (user) { this.user = JSON.parse(user); }
    this.getCurrency();
  }

  loadCurrencies() {
    if (!this.currencies) {
      this.getCurrency();
    }
  }

  getCurrency() {
    this.currencyService.getCurrencies().subscribe(success => {
      this.currencies = success.result;
    });
  }

  updateCurrency(currency: string) {
    this.currency.emit(currency);
    this.browserStorageService.setLocalStorage('currencyType', currency, true);
    this.currencyType = currency;
  }

  swithToDesktop() {
    this.browserStorageService.setLocalStorage('manual_device_change', 'desktop', false);
    location.reload();
  }
}
