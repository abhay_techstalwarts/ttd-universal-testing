﻿import { CityService } from './../../_services/city.service';
import { ActivityService } from './../../_services/activity.service';
import { WINDOW } from '@ng-toolkit/universal';
import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  Inject,
  PLATFORM_ID
} from '@angular/core';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from '../../../shared/_helper/form-helper';
import {
  SubscribeService,
  BrowserStorageService
} from '../../../shared/_services';
import swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';
import { DefaultConstant } from '../../_constants/default_constants';
import { isPlatformBrowser } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as _ from 'lodash';

declare global {
  interface Window { zE: any; zEmbed: any }
}
@Component({
  moduleId: module.id.toString(),
  selector: 'app-footer-desktop',
  templateUrl: 'footer-desktop.component.html',
  styleUrls: ['./footer-desktop.component.css']
})

export class FooterDesktopComponent implements OnInit {
  @Input() text;
  subscribeForm;
  @ViewChild('subscribeButton') subscribeButton: ElementRef;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;

  blogUrl = DefaultConstant.blogUrl;
  refundUrl = DefaultConstant.refundUrl;
  faqUrl = DefaultConstant.faqUrl;
  paymentMethodUrl = DefaultConstant.paymentMethodUrl;
  askTTDUrl = DefaultConstant.askTTDUrl;
  showSubscribedMsg: boolean = false;
  isDesktop: boolean;
  alreadySubscribedMsg = false;

  topActivities: any;
  topCities: any;
  telegram = DefaultConstant.telegram;
  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(PLATFORM_ID) private platFormId: any,
    private subscribeService: SubscribeService,
    private browserStorageService: BrowserStorageService,
    private deviceService: DeviceDetectorService,
    private activityService: ActivityService,
    private cityService: CityService
  ) {
    this.isDesktop = this.deviceService.isDesktop();
  }

  ngOnInit() {
    this.initiateForms();
    this.getTopActivities();
    this.getTopCities();
 
  }

  onSubscribe() {
    this.subscribeForm.setValue({
      password: this.subscribeForm.getValue('email'),
      status: 1
    });
    if (this.subscribeForm.isValid()) {
      FormHelper.disableButton(this.subscribeButton, 'Subscribing...');
      this.subscribeService.subscribe(this.subscribeForm.getData()).subscribe(
        () => {
          FormHelper.enableButton(this.subscribeButton, 'Subscribe');
          this.showSubscribedMsg = true;
          this.alreadySubscribedMsg = false;
        },
        fail => {
          FormHelper.enableButton(this.subscribeButton, 'Subscribe');
          if (fail[0].error.message.includes('Duplicate')) {
            this.alreadySubscribedMsg = true;
            this.showSubscribedMsg = false;
          }
        }
      );
    }
  }



  initiateForms() {
    this.subscribeForm = new ValidationManager({
      email: 'required|email',
      password: 'required',
      status: 'required'
    });
  }

  swithToMobile() {
    this.browserStorageService.setLocalStorage('manual_device_change', 'mobile', false);
    location.reload();
  }


  getTopActivities() {
    this.activityService.getActivityByRank().subscribe(success => {
      this.topActivities = success.result;
      this.topActivities = _.sortBy(this.topActivities, ['rank']);
      const activityBaseUrl = '/activity/';

      this.topActivities.forEach((element, i) => {
        if (i === 4) {
          return false;
        }
        this.topActivities[i].url =
        activityBaseUrl + this.topActivities[i].activity_id + '-' + this.topActivities[i].name.replace(/ /g, '-');
      });
      // console.log(this.topActivities);
    },
    fail => {
      this.topActivities = null;
    });
  }

  getTopCities() {
    this.cityService.getCityByRank().subscribe(success => {
      this.topCities = success.result;
      this.topCities = _.sortBy(this.topCities, ['rank']);
      const cityBaseUrl = '/city/';

      this.topCities.forEach((element, i) => {
        if (i === 4) {
          return false;
        }
        this.topCities[i].url = cityBaseUrl + this.topCities[i].city_id + '-' + this.topCities[i].displayname.replace(/ /g, '-');
      });
      // console.log(this.topCities);
    },
    fail => {
      this.topCities = null;
    });
  }
}
