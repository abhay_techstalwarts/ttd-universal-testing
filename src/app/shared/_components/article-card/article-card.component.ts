import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.css']
})
export class ArticleCardComponent implements OnInit {
  @Input() aData;
  titleRoute: any;

  constructor(private router: Router) {}

  ngOnInit() {
    this.titleRoute = this.aData.title
      .replace(/\, /gi, '-')
      .replace(/\ /gi, '-')
      .replace(/\//gi, '-');
  }

  RouteLink(articleId, cityId) {
    this.router.navigate(['/article', articleId + '-' + this.titleRoute], {
      queryParams: { city_id: cityId }
    });
  }
}
