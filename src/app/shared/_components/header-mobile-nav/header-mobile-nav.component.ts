import { Component, OnInit, Input, Output,
  EventEmitter,PLATFORM_ID,Inject} from '@angular/core';
import {
  HomeService,
  BrowserStorageService
} from '../../../shared/_services/index';
import { DefaultConstant } from '../../_constants/default_constants';
import { AuthenticationService } from './../../_services/authentication.service';
import { environment } from '../../../../environments/environment';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { UserService } from './../../../shared/_subjects/user.subject';
import { Subscription } from 'rxjs';


declare let $: any;
@Component({
  selector: 'app-header-mobile-nav',
  templateUrl: './header-mobile-nav.component.html',
  styleUrls: ['./header-mobile-nav.component.css']
})
export class HeaderMobileNavComponent implements OnInit {
  currencies: any[];
  @Output() currency: EventEmitter<string> = new EventEmitter();
  showKidzappLogo = false;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  cartCount: string;
  user: any;
  subscription: Subscription;
  updateUsersubscription: Subscription;
  blogUrl = DefaultConstant.blogUrl;
  isOpenMenu = false;
  isRightMenuOpen = false;
  isShowDestination =false;
  firstCall=true;
  homeData:any;
  whatsapp =DefaultConstant.whatsapp;
  telegram =DefaultConstant.telegram;

  constructor(
    private browserStorageService: BrowserStorageService,
    private userService: UserService,
    
    private homeService: HomeService,
    private authenticationService: AuthenticationService,

    @Inject(PLATFORM_ID) private platFormId: any

  ) { 
    if (isPlatformBrowser(this.platFormId)) {
    const user = this.browserStorageService.getLocalStorage('user');
    this.subscription = this.userService.clearUser().subscribe(() => {
      if (user) {
        this.user = JSON.parse(user);
      }
    });
    this.updateUsersubscription = this.userService
      .modifyUser()
      .subscribe(() => {
        if (user) {
          this.user = JSON.parse(user);
        }
      });
  }
}

  ngOnInit() {
    if (isPlatformBrowser(this.platFormId)) {
      const user = this.browserStorageService.getLocalStorage('user');
      if (user) {
        
        this.user = JSON.parse(user);
    console.log(this.user);

      }
      this.getCartCount();
    }
   
    if (this.browserStorageService.getLocalStorage('from_kidzapp')) {
      const from_kidzapp = this.browserStorageService.getLocalStorage('from_kidzapp');
      if (from_kidzapp && from_kidzapp === 'true') {
        this.showKidzappLogo = true;
      } else {
        this.showKidzappLogo = false;
      }
    } else {
      this.showKidzappLogo = false;
    }
  }

  ngOnChanges() {
    this.getCartCount();
  }

  getCartCount() {
    this.cartCount = this.browserStorageService.getLocalStorage('cartCount');
  }

  destinationToggle(){
    if(this.isShowDestination){
      this.isShowDestination =false;
      $('#destinationToggleBtn').html("View less")
      $('#destination_list').css({'height': '100%'});
    }else{
      this.isShowDestination =true;
      $('#destinationToggleBtn').html("View more")
      $('#destination_list').css({'height': '240px'});
    }
   
  }
  

  ngOnDestroy() {
    if (this.updateUsersubscription) {
      this.updateUsersubscription.unsubscribe();
    }
  }

  openRightMenu(){
    this.isRightMenuOpen = true;
    $('.side_menu_shadow').css({'display': 'block','background-color':' rgba(0, 0, 0, 0.5)'});
    $('.right_side_menu').css({'right': '0'});
  }
  closeRightMenu(){
    $('.side_menu_shadow').css({'display': 'none','background-color':' rgba(0, 0, 0, 0)'});
    $('.right_side_menu').css({'right': '-80%'});
    this.isRightMenuOpen =false;
  }

  logout() {
    this.authenticationService.logout();
    this.userService.clearUser();
    this.user = null;
    this.closeRightMenu()
  }
  toggleMenu(){
    if(this.isOpenMenu){
      this.isOpenMenu=false;
      this.closeSideMenu();
    }else{
      if(this.firstCall){
          this.homeService.getHomeData().subscribe(
            success => {
              this.homeData = success.result;
              this.browserStorageService.setLocalStorage('currencyType', this.homeData.user_currency);
            },
            _fail => { }
          );
          this.firstCall = false;
        }
      
      this.isOpenMenu=true;
      this.showMenu();
    }

  }

  closeSideMenu(){
    $('.side_menu').css({'transform': 'translateY(120%)'});
  }
  showMenu(){
    $('.side_menu').css({'transform': 'translateY(0px)'});
  }

}
