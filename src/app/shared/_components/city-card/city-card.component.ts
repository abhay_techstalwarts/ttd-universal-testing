import { Component, OnInit, Input } from '@angular/core';
import { DefaultConstant } from '../../_constants/default_constants';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.css']
})
export class CityCardComponent implements OnInit {
  @Input() city;
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;

  constructor() {}

  ngOnInit() {}
}
