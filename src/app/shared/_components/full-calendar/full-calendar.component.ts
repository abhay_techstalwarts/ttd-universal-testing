import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FullCalendarService } from './../../_services/fullCalendar.service';
import { CommonHelper } from './../../_helper/common.helper';

declare let FullCalendar: any;
declare let $: any;

@Component({
  selector: 'app-full-calendar',
  templateUrl: './full-calendar.component.html',
  styleUrls: ['./full-calendar.component.css']
})
export class FullCalendarComponent implements OnInit, AfterViewInit {
  @Input('calendarId') calendarId: any;
  @Input('inputId') inputId: any;
  @Input('packageId') packageId: any;
  @Output() selectedPacksageId: EventEmitter<string> = new EventEmitter();
  @Input('events') calendarEvents: any;
  @Input() dateRangeStart: any;
  @Input() dateRangeEnd: any;
  fullCalendarEl: any;
  selectedDate: any = '';
  disabledDates: any[] = [];
  disabledDays: number[] = [];
  cellIndex: any = 0;
  isShowCalendarVisible = false;
  fullCalendarDefault: any = {
    plugins: ['interaction', 'dayGrid'],
    defaultView: 'dayGridMonth',
    selectable: true,
    showNonCurrentDates: false,
    fixedWeekCount: false,
    height: 'parent',
    events: [],
    selectAllow: (info: any) => {
      return this.isAllowedDate(info.start);
    },
    dateClick: (info: any) => {
      if (this.isAllowedDate(info.date)) {
        $('td.fc-widget-content[data-date]', `#${this.calendarId}`).removeClass(
          'full-calendar-selected-date'
        );
        $('td.fc-day-top[data-date]', `#${this.calendarId}`).removeClass(
          'full-calendar-selected-date'
        );
        this.cellIndex = info.dayEl.cellIndex;
        this.selectedDate = info.dateStr;
        if (this.packageId) {
        this.selectedPacksageId.emit(this.packageId);
        }
        $(`#${this.inputId}`).val(this.selectedDate);

        if (this.calendarId === 'participationCalendar') {
          this.fullCalendarService.sendCalendarData('activityDate', info.date);
        } else {
          this.fullCalendarService.sendCalendarData('packageDate', info.date);
        }

        $(`#${this.calendarId}`).addClass('full-calendar-hidden');
        this.isShowCalendarVisible = false;


        $('body').css({ overflow: 'auto' });
        $('.block_screen').css({ display: 'none' });
      }
    
    },
    datesRender: () => {
      this.disableRang();
    },
    dayRender: dayRenderInfo => {
      if (this.disabledDays && this.disabledDays.length > 0) {
        this.disabledDays.forEach(disabledDay => {
          if (dayRenderInfo.date.getDay() === disabledDay) {
            $(dayRenderInfo.el).addClass('disabled-week-day');
            const disableDate = $(dayRenderInfo.el)[0].dataset.date;
            $(`td.fc-day-top[data-date=${disableDate}]`).addClass(
              'disabled-week-day'
            );
          }
        });
      }
    }
  };

  // instances
  fullCalendarInstace: any;

  constructor(private fullCalendarService: FullCalendarService) { }

  ngOnInit() {
    this.subscribedData();
  }

  ngAfterViewInit() { }

  // init full calendar
  initFullCalendar() {
    this.fullCalendarEl = document.getElementById(this.calendarId);
    if (!this.fullCalendarEl.hasChildNodes()) {
      this.fullCalendarInstace = new FullCalendar.Calendar(
        this.fullCalendarEl,
        this.fullCalendarDefault
      );
      this.fullCalendarInstace.render();
    }
  }

  disableRang() {
    const $classThis = this;
    const parent = $('.fc-view-container', `#${this.calendarId}`);
    const $dates = $('td.fc-widget-content[data-date]', parent);
    const $datesNumber = $('td.fc-day-top[data-date]', parent);
    $dates.addClass('disabled-date');
    $datesNumber.addClass('disabled-date');
    $('.fc-content-skeleton tbody tr td', parent).removeClass(
      'selected-date-comment'
    );
    $dates.each(function (index) {
      const currentDate = $(this).attr('data-date');
      if (
        $classThis.isAllowedDate(currentDate) &&
        !$(this).hasClass('fc-other-month')
      ) {
        $(`td.fc-day-top[data-date=${currentDate}]`).removeClass(
          'disabled-date'
        );
        $(this).removeClass('disabled-date');
      }
    });
    if (this.selectedDate) {
      const selectedDateEl = $(
        `td.fc-day-top[data-date=${this.selectedDate}]`,
        `#${this.calendarId}`
      );
      if (
        !selectedDateEl.hasClass('disabled-date') &&
        !selectedDateEl.hasClass('fc-other-month')
      ) {
        selectedDateEl.addClass('full-calendar-selected-date');
        const topEL = $(
          `td.fc-widget-content[data-date="${this.selectedDate}"]`,
          `#${this.calendarId}`
        );
        topEL.addClass('full-calendar-selected-date');
        const commentEl = $(`td.fc-day-top[data-date=${this.selectedDate}]`)
          .parent()
          .parent()
          .siblings()[0];
        $(`tr:eq(0) td:eq(${this.cellIndex})`, commentEl).addClass(
          'selected-date-comment'
        );
      }
    }
  }

  // refreshCalendar
  refreshCalendar() {
    this.calendarEvents = Array.isArray(this.calendarEvents)
      ? this.calendarEvents
      : [];
    if (this.calendarEvents.length) {
      // get all events and remove events
      const calEvents = this.fullCalendarInstace.getEvents();
      calEvents.forEach(evt => {
        evt.remove();
      });
      // now add all events
      this.calendarEvents.forEach(data => {
        this.fullCalendarInstace.addEvent({
          title: data.comment,
          date: data.date
        });
        if (data.isDisabled && !this.disabledDates.includes(data.date)) {
          this.disabledDates.push(data.date);
        }
      });
    }
  }

  subscribedData() {
    this.fullCalendarService.getCalendarData().subscribe(event => {
      const action = event.action;
      switch (action) {
        case 'activityDate': {
          this.selectedDate = CommonHelper.dateFormaterNew(event.data);
          $(`#${this.inputId}`).val(this.selectedDate);
          break;
        }
        case 'packageDate': {
          this.selectedDate = CommonHelper.dateFormaterNew(event.data);
          $(`#${this.inputId}`).val(this.selectedDate);
          break;
        }
        case 'disabledDays': {
          this.disabledDays = event.data;
          break;
        }
      }
    });
  }

  // check if date should be allowed or not
  isAllowedDate(selectedDate) {
    let returnData = true;
    if (
      this.disabledDates.includes(selectedDate) ||
      this.disabledDays.includes(new Date(selectedDate).getDay())
    ) {
      return false;
    }

    const selected = new Date(selectedDate);
    // check if it is past date
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    if (selected < today) {
      returnData = false;
    }
    if (this.dateRangeStart && this.dateRangeEnd) {
      const sDate: any = new Date(this.dateRangeStart);
      const eDate: any = new Date(this.dateRangeEnd);
      if (selected < sDate || selected > eDate) {
        return false;
      }
    }
    // other conditions to disallow selection of date
    this.disabledDates.forEach(d => {
      const disDate = new Date(d);
      disDate.setHours(0, 0, 0, 0);
      if (disDate.toString() === selected.toString()) {
        returnData = false;
      }
    });

    return returnData;
  }

  hideFullCalendar(cid) {
    $(`.full-calendar-container`).addClass('full-calendar-hidden');
    this.fullCalendarInstace.destroy();
    this.isShowCalendarVisible = false;
    $('.block_screen').css({ display: 'none' });
    $('body').css({ overflow: 'auto' });
  }

  // show full calendar
  showFullCalendar(cId) {

    console.log(cId);
    if (this.isShowCalendarVisible) {
      $(`#${cId}`).addClass('full-calendar-hidden');
      this.fullCalendarInstace.destroy();
      this.isShowCalendarVisible = false;
      $('.block_screen').css({ display: 'none' });
      $('body').css({ overflow: 'auto' });
      // $('body').css({ overflow: 'auto' });
    } else {
      $(`${cId}`).removeClass('full-calendar-hidden');
      if ($('.block_screen').width() < 769) {
        $('body').css({ overflow: 'hidden' });
      }
      $('.block_screen').css({ display: 'block' });
      // $('body').css({ overflow: 'hidden' });
      this.initFullCalendar();

      // this.fullCalendarInstace.render();
      this.refreshCalendar();
      this.disableRang();
      $('td.fc-widget-content[data-date]', `#${cId}`).removeClass(
        'full-calendar-selected-date disabled-date'
      );

      if (this.disabledDates.length > 0) {
        this.disabledDates.forEach(d => {
          $(`td.fc-widget-content[data-date="${d}"]`, `#${cId}`).addClass(
            'disabled-date'
          );
          $(`td.fc-day-top[data-date=${d}]`, `#${cId}`).addClass(
            'disabled-date'
          );
        });
      }

      if (this.selectedDate) {
        const selectedDateEl = $(
          `td.fc-day-top[data-date=${this.selectedDate}]`,
          `#${cId}`
        );
        if (
          !selectedDateEl.hasClass('disabled-date') &&
          !selectedDateEl.hasClass('fc-other-month')
        ) {
          selectedDateEl.addClass('full-calendar-selected-date');
          const topEL = $(
            `td.fc-widget-content[data-date="${this.selectedDate}"]`,
            `#${cId}`
          );
          topEL.addClass('full-calendar-selected-date');
          const commentEl = $(`td.fc-day-top[data-date=${this.selectedDate}]`)
            .parent()
            .parent()
            .siblings()[0];
          $(`tr:eq(0) td:eq(${this.cellIndex})`, commentEl).addClass(
            'selected-date-comment'
          );
        }
      }

      $(`#${cId}`).removeClass('full-calendar-hidden');
      this.isShowCalendarVisible = true;
    }
  }
}
