import {
  AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnInit, Output,Inject
} from '@angular/core';

@Directive({
  selector: '[appTableFilterable]'
})
export class TableFilterableDirective implements OnInit, AfterViewInit {
  @Input('appTableFilterable') params: object;
  @Output() onFilterChanged = new EventEmitter<object>();

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const filterButtons = this.el.nativeElement.querySelectorAll('button[data-filter]');
    Array.from(filterButtons).forEach((button: any) => {
      button.addEventListener('click', this.onClick.bind(this));
    });
  }

  onClick(event) {
    const action = event.target.dataset.filter;
    if (action === 'apply-filters') {
      this.applyFilters();
    } else if (action === 'clear-filters') {
      this.clearFilters();
    } else {
      return;
    }
    this.onFilterChanged.emit(this.params);
  }

  private applyFilters() {
    const filters = this.getFilters();

    Array.from(filters).forEach((element: any) => {
      if (element.value !== null
        && element.value !== 'null'
        && element.value !== ''
        && element.value !== undefined) {
        this.params[element.dataset.filter] = element.value;
      } else {
        delete this.params[element.dataset.filter];
      }
    });
  }

  private clearFilters() {
    const filters = this.getFilters();

    Array.from(filters).forEach((element: any) => {
      delete this.params[element.dataset.filter];
    });

    this.el.nativeElement.reset();
  }

  private getFilters() {
    return this.el.nativeElement.querySelectorAll('[data-filter]:not(button)');
  }
}
