import { WINDOW } from '@ng-toolkit/universal';
import { Directive, Input, HostListener , Inject} from '@angular/core';

@Directive({
  selector: '[appScrollPointCart]'
})
export class ScrollPointCartDirective {
  @Input() appScrollPointCart: number;

  constructor(@Inject(WINDOW) private window: Window, ) {
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
  }

}
