﻿import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable,Inject } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BrowserStorageService } from '../_services';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private router: Router,private browserStorageService:BrowserStorageService) {
  }

  canActivate(_route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.browserStorageService.getLocalStorage('user')) {
    // if (this.localStorage.getItem('user')) {
      // logged in so return true
      return true;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

    return false;
    // not logged in so redirect to login page with the return url
  }
}
