import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { ScrollPointCartDirective } from '../shared/_directives/affix2.directive';
import { SlugifyPipe } from "./_pipes/slugify.pipe";

import {
  CardComponent,
  SearchDesktopComponent,
  SearchDesktopHeaderComponent,
  SearchDesktopRegionsComponent,
  ExploreDesktopRegionsComponent,
  SearchDesktopRegionsHeaderComponent,
  MobileFooterComponent,
  FooterDesktopComponent,
  HeaderMobileComponent,
  HeaderMobileNavComponent,
  HeaderDesktopComponent,
  CitySelectorComponent,
  CityCardComponent,
  LoadingComponent,
  ArticleCardComponent,
  FullCalendarComponent,
} from './_components/index';
@NgModule({
  declarations: [
    // Components
    CardComponent,
    LoadingComponent,
    ScrollPointCartDirective,
    SlugifyPipe,
    // Desktop component
    FooterDesktopComponent,
    HeaderDesktopComponent,
    SearchDesktopRegionsComponent,
    SearchDesktopRegionsHeaderComponent,
    ExploreDesktopRegionsComponent,
    SearchDesktopComponent,
    SearchDesktopHeaderComponent,
    CitySelectorComponent,
    CityCardComponent,
    FullCalendarComponent,

    // Mobile Component
    MobileFooterComponent,
    HeaderMobileComponent,
    ArticleCardComponent,
    HeaderMobileNavComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    LazyLoadImageModule,
  ],
  exports: [
    // Components
    CardComponent,
    LoadingComponent,
    ScrollPointCartDirective,

    // Desktop component
    FooterDesktopComponent,
    HeaderDesktopComponent,
    SearchDesktopRegionsComponent,
    ExploreDesktopRegionsComponent,
    SearchDesktopComponent,
    SearchDesktopHeaderComponent,
    CitySelectorComponent,
    CityCardComponent,
    ArticleCardComponent,
    FullCalendarComponent,
    HeaderMobileNavComponent,
    LazyLoadImageModule,
    // Mobile Component
    MobileFooterComponent,
    HeaderMobileComponent,
  ]
})
export class SharedModule {
}
