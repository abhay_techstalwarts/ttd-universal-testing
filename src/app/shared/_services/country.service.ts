import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CountryService {
  constructor(private http: HttpClient) { }

  getCountries(): Observable<any> {
    const url = `${environment.apiBaseUrl}/countries`;
    return this.http.get(url);
  }

}
