import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { ApiHelperService } from './api-helper.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class CityService {
  constructor(private http: Http, private httpClient: HttpClient,private apiHelperService:ApiHelperService) {}

  private url = `${environment.apiBaseUrl}/citypage/2`;

  getCityData(cityId): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`citypage/${cityId}`);
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getCity(id: number): Observable<any> {
    const url = `${this.url}/${id}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getArticles(city_id): Observable<any> {
    // const url = `assets/json/article.json`;
    // const url = `https://stgapi.ticketstodo.com/articles`;
    const url = `${environment.apiBaseUrl}/articles?records_per_page=0&status=published&city_id=` + city_id;
    return this.httpClient.get(url);
  }

  getPopularCities(): Observable<any> {
    const url = `${environment.apiBaseUrl}/cities?is_popular=1`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getRegions(): Observable<any> {
    const url = `${environment.apiBaseUrl}/regions_details`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getCities(): Observable<any> {
    const url = `${environment.apiBaseUrl}/cities`;
    return this.httpClient.get(url);
  }

  getCategories(cityId: number): Observable<any> {
    const url = `${environment.apiBaseUrl}/citycategories/${cityId}`;
    return this.httpClient.get(url);
  }

  getCategoryWiseCityActivities(cityId, categoryId?): Observable<any> {
    let url = this.apiHelperService.getApiUrl(`citypage/${cityId}`);
    url = categoryId ? `${url}&category_id=${categoryId}` : `${url}`;
    return this.httpClient.get(url);
  }

  getPopularDestinations(pageno = 0, records_per_page = 0): Observable<any> {
    const url = `${environment.apiBaseUrl}/cities?page=${pageno}&records_per_page=${records_per_page}&is_popular=1`;
    return this.httpClient.get(url);
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }


  getCityByRank(option = 1): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`ranked_city/${option}`);
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

}
