import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class StaticPagesService {
  activityData: any;
  activityId: any;

  constructor(private http: Http) {}

  getAboutUs(): Observable<any> {
    const Url = 'https://api.ticketstodo.com/pages/7';
    return this.http.get(Url).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getContactUs(data): Observable<any> {
    const Url = 'https://ticketstodo.zendesk.com/api/v2/tickets.json';
    const username = 'support.team@ticketstodo.com';
    const password = '1HyFtLLH1oy2';
    const headers: Headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
    headers.append('Content-Type', 'application/json');
    return this.http.post(Url, data, { headers: headers }).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getPrivacyPolicy(): Observable<any> {
    const Url = 'https://api.ticketstodo.com/pages/10';
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(Url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getTermsAndConditions(): Observable<any> {
    const Url = 'https://api.ticketstodo.com/pages/12';
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(Url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
