import { BrowserStorageService } from './browserStorage.service';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';

import { ApiHelperService } from './api-helper.service';
import { environment } from '../../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ActivityService {
  activityData: any;
  activityId: any;

  constructor(private http: Http,private browserStorageService:BrowserStorageService,private apiHelperService:ApiHelperService) {}

  getActivityData(activityId): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`activitypackages/${activityId}`);
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  checkAvailability(activityId, date): Observable<any> {
    // const url =
    //   environment.apiBaseUrl +
    //   `/search_activity_packages?activity_id=${activityId}&search_date=${date}&currency=${localStorage.getItem(
    //     'currencyType'
    //   )}`;
    const url =
    environment.apiBaseUrl +
    `/search_activity_packages?activity_id=${activityId}&search_date=${date}&currency=${this.browserStorageService.getLocalStorage(
      'currencyType'
    )}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getPackageTimeSlots(packageId, date): Observable<any> {
    // const url =
    //   environment.apiBaseUrl +
    //   `/get_package_slots/${packageId}?search_date=${date}&currency=${localStorage.getItem(
    //     'currencyType'
    //   )}`;
    const url =
    environment.apiBaseUrl +
    `/get_package_slots/${packageId}?search_date=${date}&currency=${this.browserStorageService.getLocalStorage(
      'currencyType'
    )}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getPackageGroups(packageId, date, time): Observable<any> {
    // const url =
    //   environment.apiBaseUrl +
    //   `/get_package_groups/${packageId}?search_date=${date}&booking_time=${time}&currency=${localStorage.getItem(
    //     'currencyType'
    //   )}`;
    const url =
    environment.apiBaseUrl +
    `/get_package_groups/${packageId}?search_date=${date}&booking_time=${time}&currency=${this.browserStorageService.getLocalStorage(
      'currencyType'
    )}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  setRecentActivity(activity){
    let recentActivity = this.browserStorageService.getLocalStorage('recentActivity');
    recentActivity = recentActivity ? JSON.parse(recentActivity) : [];
    if(!(recentActivity.find(item => activity.activity_id == item.activity_id))){
      recentActivity.push(activity);
      this.browserStorageService.setLocalStorage('recentActivity',JSON.stringify(recentActivity),true)
    }
  }
  setUpdatedRecentActivity(activitys){
    this.browserStorageService.setLocalStorage('recentActivity',JSON.stringify(activitys),true)
  }

  getRecentActivity(): Observable<any>{
    let recentActivity = this.browserStorageService.getLocalStorage('recentActivity');
    recentActivity = recentActivity ? JSON.parse(recentActivity) : [];
    if (!recentActivity) {
      return recentActivity;
    }
    let acticityIds= '';
    recentActivity.forEach((element,index) => {
      if (index == 0) {
        acticityIds += `activity_ids[]=${element.activity_id}`;
      }else{
        acticityIds += `&activity_ids[]=${element.activity_id}`
      }
      
    });
    // return recentActivity.reverse();
    const url =
    environment.apiBaseUrl +
    `/recentactivies?${acticityIds}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => {
        let result = response.json().result;
        let newRecentActivity = recentActivity.filter(old => result.some(({activity_id}) => old.activity_id === activity_id));
        this.setUpdatedRecentActivity(newRecentActivity);
        return newRecentActivity;
      }),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }

  getActivityByRank(option = 1): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`ranked_activity/${option}`);
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }
}
