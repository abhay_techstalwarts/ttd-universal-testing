import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ForgetpwdService {
  constructor(private http: Http) {}

  private requestPwdUrl = `${environment.apiBaseUrl}/forgotpassword`;
  private changePwdUrl = `${environment.apiBaseUrl}/loginuser/changepassword`;

  requestPwd(data): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    const body = this.serializeObj(data);
    return this.http.post(this.requestPwdUrl, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  changePwd(data): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({ headers: headers });
    const body = this.serializeObj(data);
    return this.http.put(this.changePwdUrl, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }

  private serializeObj(obj: any) {
    const result = [];
    for (const property in obj)
      result.push(
        encodeURIComponent(property) + '=' + encodeURIComponent(obj[property])
      );

    return result.join('&');
  }
}
