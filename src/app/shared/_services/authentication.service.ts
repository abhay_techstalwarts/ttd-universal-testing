import { BrowserStorageService } from './browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../../environments/environment';
import { ApiHelperService } from './api-helper.service';
import { DefaultConstant } from './../_constants/default_constants';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private http: Http,
    private httpClient: HttpClient,
    private router: Router,
    private browserStorageService: BrowserStorageService,
    private apiHelperService: ApiHelperService
  ) { }

  private url = `${environment.apiBaseUrl}/loginuser`;

  login(data) {
    const apiUrl = `${this.url}/validate`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    const body = this.serializeObj(data);

    return this.http.post(apiUrl, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  socialLogin(user: any) {
    const apiUrl = `${environment.apiBaseUrl}/check_ttd_user`;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    const body = {
      email: user.email,
      firstname: user.firstName,
      lastname: user.lastName,
      image_url: user.photoUrl,
      userby: 'login'
    };

    return this.http.post(apiUrl, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  signUp(data) {
    const apiUrl = `${this.url}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Post,
      headers: headers
    });
    const body = this.serializeObj(data);

    return this.http.post(apiUrl, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  logout() {
    // this.localStorage.removeItem('user');
    this.browserStorageService.emptyKeyLocalStorage('user');

    // this.localStorage.removeItem('cartCount');
    this.browserStorageService.emptyKeyLocalStorage('cartCount');

    // this.localStorage.removeItem('currencyType');
    this.browserStorageService.emptyKeyLocalStorage('currencyType');

    // this.localStorage.removeItem('shopperId');
    this.browserStorageService.emptyKeyLocalStorage('shopperId');

    // this.localStorage.removeItem('currentUser');
    this.browserStorageService.emptyKeyLocalStorage('currentUser');

    for (let i = 0; i < DefaultConstant.securedUrl.length; i++) {
      if (this.router.url === DefaultConstant.securedUrl[i]) {
        this.router.navigate(['/']);
      }
    }
  }

  goToHomePage() {
    this.router.navigate(['/']);
  }

  getUser() {
    // return JSON.parse(this.localStorage.getItem('user'));
    let user = this.browserStorageService.getLocalStorage('user');
    if (user) {
      user = JSON.parse(user);
    }
    return user;
  }

  getShopperIdonLogin(data: object): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`shopper`);
    return this.httpClient.post(url, data);
  }

  public handleError(error: Response) {
    return observableThrowError(error.json());
  }

  private serializeObj(obj: any) {
    const result = [];
    for (const property in obj) {
      result.push(
        encodeURIComponent(property) + '=' + encodeURIComponent(obj[property])
      );
    }
    return result.join('&');
  }
}
