import { Injectable, Inject, RendererFactory2, ViewEncapsulation } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as _cloneDeep from 'lodash/cloneDeep';
import { VideoObjectSchema } from '../_models/VideoObjectSchema';
import { ProductSchema } from '../_models/ProductSchema';

@Injectable({
    providedIn: 'root',
})
export class JsonLdService {

    constructor(
        @Inject(DOCUMENT) private doc,
        private rendererFactory: RendererFactory2
    ) { }

    private jsonLd: any = {};

    setSchemaObjects(rawData: any) {
        this.removeTag('type="application/ld+json"');

        // Add video schema
        const videoObject = this.getVideoSchema(rawData);
        this.addScriptToDom(videoObject);

        // Add product schema
        const productObject = this.getProductSchema(rawData);
        this.addScriptToDom(productObject);
    }

    addScriptToDom(object: any) {
        const script: HTMLScriptElement = this.doc.createElement('script');
        script.setAttribute('type', 'application/ld+json');
        script.textContent = this.toJson(object);
        this.doc.head.appendChild(script);
    }

    getVideoSchema(rawData: any) {
        const object = _cloneDeep(VideoObjectSchema);
        if (rawData) {
            object.name = rawData.name;
            object.description = rawData.activity_description;
            object.thumbnailUrl = rawData.video_thumbnail_image_url;
            object.uploadDate = rawData.updated_at;
            if (rawData.videos && rawData.videos[0]) {
                object.contentUrl = rawData.videos[0].video_url;
            }
        }
        return object;
    }

    getProductSchema(rawData: any) {
        const object = _cloneDeep(ProductSchema);
        if (rawData) {
            const today = new Date().toLocaleDateString();
            object.name = rawData.name;
            object.description = rawData.activity_description;
            object.mpn = String(rawData.activity_id);
            object.sku = String(rawData.activity_id);

            // set reviews
            object.review.name = rawData.name;
            object.review.datePublished = today;

            // set offers
            object.offers.url = this.doc.URL;
            object.offers.priceCurrency = rawData.request_currency;
            object.offers.price = String(rawData.min_default_discounted_price);
            object.offers.priceValidUntil = today;

            // set images
            if (rawData.images && rawData.images.length > 0) {
                rawData.images.forEach(element => {
                    object.image.push(element.image_url);
                });
            }
        }
        return object;
    }

    toJson(obj: any) {
        return JSON.stringify(obj);
    }

    removeTag(attrSelector: string) {
        if (attrSelector) {
            try {
                const renderer = this.rendererFactory.createRenderer(this.doc, {
                    id: '-1',
                    encapsulation: ViewEncapsulation.None,
                    styles: [],
                    data: {}
                });
                const head = this.doc.head;
                if (head === null) {
                    throw new Error('<head> not found within DOCUMENT.');
                }
                const scriptTags = this.doc.querySelectorAll(
                    'script[' + attrSelector + ']'
                );
                for (const script of scriptTags) {
                    renderer.removeChild(head, script);
                }
            } catch (e) {
                console.log('Error while removing tag ' + e.message);
            }
        }
    }
}
