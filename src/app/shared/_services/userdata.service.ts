import { BrowserStorageService } from './browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { ApiHelperService } from './api-helper.service';
import { map, catchError } from 'rxjs/operators';
import { AffiliateService } from "./affiliate.service";

@Injectable()
export class UserdataService {
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private http: Http, 
  private apiHelperService: ApiHelperService,
   private browserStorageService: BrowserStorageService,
   private affiliateService : AffiliateService) { }

  private Url = `${environment.apiBaseUrl}/shopper`;
  private AddToCartUrl = `${environment.apiBaseUrl}/addtocart`;
  private cancelOrderUrl = `${environment.apiBaseUrl}/CancelOrder`;
  private PaymentUrl = `${environment.paymentBaseUrl}/dopayment.php`;

  shopperData(id: string | number): Observable<any> {
    const url = `${this.Url}/${id}?currency=USD`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getUserBookings(shopperId) {
    const url = this.apiHelperService.getApiUrl('ShopperOrdersDetail/' + shopperId);
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  cancelUserBookings(data) {
    data = this.affiliateService.bindAffiliateData(data);
    const url = this.apiHelperService.getApiUrl('/OrderTransactions');
    return this.http.post(url, data).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  cancelOrder(data) {
    data = this.affiliateService.bindAffiliateData(data);
    const url = this.apiHelperService.getApiUrl('/CancelOrder');
    return this.http.post(url, data).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  cancelPackage(data) {
    data = this.affiliateService.bindAffiliateData(data);
    const url = `${this.cancelOrderUrl}?booking_id=${data.booking_id}`;
    return this.http.post(url, data).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getOrderTransactions(orderId) {
    const url = this.apiHelperService.getApiUrl('/OrderTransactions/' + orderId);
    return this.http.get(url).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  addtocartData(
    id: string | number,
    package_details?: string,
    currency?: string,
    coupon_code?: string
  ): Observable<any> {
    const url = `${this.AddToCartUrl}/${id}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    let data = {
      package_details: package_details,
      currency: currency,
      coupon_code: coupon_code
    };
    const inputParams = this.affiliateService.bindAffiliateData(data);
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers,
      params: inputParams
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  insertPaymentData(paymentdata: any): Observable<any> {
    paymentdata = this.affiliateService.bindAffiliateData(paymentdata);
    const paymenturl = `${this.PaymentUrl}`;
    return this.http.post(paymenturl, paymentdata).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  editshopperdata(model: any) {
    const url = `${this.Url}/${model.shopper_id}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Put,
      headers: headers
    });
    model = this.affiliateService.bindAffiliateData(model);
    const body = this.serializeObj(model);

    return this.http.put(url, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getPayfortQuickPayData(user_currency: string, transactionCurrency: string, payment_mode?: string, coupon_code?:string): Observable<any> {
    // const shopperId = this.localStorage.getItem('shopperId');
    const shopperId = this.browserStorageService.getLocalStorage('shopperId');

    // const package_details = this.localStorage.getItem('package_details');
    const package_details = this.browserStorageService.getLocalStorage('package_details');
    const currencyType = this.localStorage.getItem('currencyType');
    let data = {
      shopper_id: shopperId,
      package_details: JSON.parse(package_details),
      currency: currencyType,
      user_currency: user_currency,
      gid: 1,
      is_live: environment.isLive,
      coupon_code:coupon_code
    };
    data = this.affiliateService.bindAffiliateData(data);
    const url = `${environment.apiBaseUrl}/payfort_check_out?payment_mode=${payment_mode}`;
    return this.http.post(url, data).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  rememberMe(remember: any, shopperId) {
    let data = {
      remember_me: remember
    };
    data = this.affiliateService.bindAffiliateData(data);
    const url = `${this.Url}/${shopperId}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Put,
      headers: headers
    });
    data = this.affiliateService.bindAffiliateData(data);
    const body = this.serializeObj(data);

    return this.http.put(url, body, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }

  private serializeObj(obj: any) {
    const result = [];
    for (const property in obj)
      result.push(
        encodeURIComponent(property) + '=' + encodeURIComponent(obj[property])
      );

    return result.join('&');
  }
}
