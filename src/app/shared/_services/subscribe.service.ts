import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Response
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class SubscribeService {
  private url = `${environment.apiBaseUrl}/loginuser?userby=subscribe`;

  constructor(private http: Http) {}

  subscribe(data): Observable<any> {
    return this.http.post(this.url, data).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json());
  }
}
