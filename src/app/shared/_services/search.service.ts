import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class SearchService {
  activityData: any;
  activityId: any;

  constructor(private http: Http) {}

  private url = `${environment.apiBaseUrl}/homesearch?search_str=`;
  private searchUrl = `${environment.apiBaseUrl}/homesearch`;

  search(searchKeyword: string, cityId?: number): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const inputParams = {
      search_str: searchKeyword,
      status: 1,
      city_id: cityId
    };
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers,
      params: inputParams
    });

    return this.http.get(this.searchUrl, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  searchPageSearch(searchKeyword): Observable<any> {
    const url = `${this.url}${searchKeyword}`;
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
