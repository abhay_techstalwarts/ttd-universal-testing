import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FullCalendarService {
    private calendarData = new Subject<any>();

    // send and get calendar date
    sendCalendarData(action, data) {
        this.calendarData.next({ action, data });
    }
    getCalendarData(): Observable<any> {
        return this.calendarData.asObservable();
    }
}
