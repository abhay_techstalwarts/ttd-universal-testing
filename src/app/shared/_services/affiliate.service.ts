import { Injectable } from '@angular/core';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { BrowserStorageService } from "./browserStorage.service";



@Injectable({
  providedIn: 'root'
})
export class AffiliateService {

  constructor(
    private http: Http,
    private browserStorageService: BrowserStorageService
  ) { }

  private url = `${environment.apiBaseUrl}/affiliate/domain`;

  getAffiliateId(domain: string): Observable<any> {
    const url = `${this.url}?domain=${domain}`;
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(url, options).pipe(
      map(response =>( response.json())),
      catchError(this.handleError)
    );
  }

  bindAffiliateData(data:any){
    let affilateData = this.browserStorageService.getLocalStorage('Affiliate');
    let txn_id = this.browserStorageService.getLocalStorage('txn_id');
    
    if (affilateData) {
      let affilate = JSON.parse(affilateData)
      if(affilate && affilate.affiliate_id){
        data['affiliate_id'] = affilate.affiliate_id;
      }
    }
    if (txn_id) {
      data['txn_id'] = txn_id;
    }
    return data;
  }

  

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
