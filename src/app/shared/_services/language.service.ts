import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class LanguageService {
  constructor(private http: Http) {}

  getHomeEnglishText(): Observable<any> {
    const url = 'assets/json/english.json';
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
