import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { Http, Response } from '@angular/http';
import { ApiHelperService } from './api-helper.service';
import { map, catchError } from 'rxjs/operators';
import { AffiliateService } from './affiliate.service';

@Injectable()
export class CartService {
  constructor(private http: Http, private affiliateService: AffiliateService) {}

  private url = `${environment.apiBaseUrl}/addtocart`;

  getCart(id: any): Observable<any> {
    const AddCartUrl = `${this.url}/${id}`;
    return this.http.get(AddCartUrl).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  addToCart(cart: object): Observable<any> {
    cart = this.affiliateService.bindAffiliateData(cart);
    return this.http.post(this.url, cart).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  validateCoupon(couponDetails: object): Observable<any> {
    const sURL = `${environment.apiBaseUrl}/Coupon_validate`
    couponDetails = this.affiliateService.bindAffiliateData(couponDetails);
    return this.http.post(sURL, couponDetails).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error.json());
  }
}
