import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiHelperService } from './api-helper.service';
import {  throwError as observableThrowError, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';



@Injectable()
export class ArticleService {

  articleData: any;

  constructor(private http: Http, private httpClient: HttpClient, private apiHelperService: ApiHelperService) { }

  getArticles(): Observable<any> {
    const url = `${environment.apiBaseUrl}/articles`;
    return this.httpClient.get(url);
  }

  setArticleData(articleList) {
    this.articleData = articleList;
  }

  getArticleData(): Observable<any> {
    return this.articleData;
  }

  getArticlesById(article_id): Observable<any> {
    const url = `${environment.apiBaseUrl}/articles/` + article_id;
    return this.httpClient.get(url).pipe(
      map(response =>response),
      catchError(this.handleError)
    );
  }

  public handleError(error: Response) {
    return observableThrowError(error || 'Server error');
  }
}
