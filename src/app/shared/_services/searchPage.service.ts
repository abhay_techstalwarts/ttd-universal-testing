import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



import { ActivatedRoute } from '@angular/router';
import { ApiHelperService } from './api-helper.service';
import { ApiService } from './api.service';
import { environment } from './../../../environments/environment';
import { HttpParam } from '../_models/searchPageHttpParam';
import { Router } from '@angular/router';

@Injectable()
export class SearchPageService {

  activityData: any;
  activityId: any;

  constructor(private httpClient: HttpClient,
              private activatedRoute: ActivatedRoute,
              private apiService: ApiService,
              private apiHelperService:ApiHelperService,
              private router: Router) {
  }

  private url = `${environment.apiBaseUrl}/searchactivities`;


  search(params?: any): Observable<any> {
    const url = this.apiHelperService.getApiUrl('searchactivities');
    params = this.apiHelperService.deleteEmptyParams(params);
    this.router.navigate([], {queryParams: params});
    return this.httpClient.get(url, {params});
  }

  getCategories(): Observable<any> {
    const url = `${environment.apiBaseUrl}/categories`;
    return this.httpClient.get(url);
  }

}
