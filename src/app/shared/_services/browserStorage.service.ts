import { PLATFORM_ID, Injectable, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable()
export class BrowserStorageService {
  // private localStorage:LocalStorageType;
  private browserStorage = {};

  constructor(@Inject(PLATFORM_ID) private platFormId: any) {
    if (isPlatformBrowser(this.platFormId)) {
      this.browserStorage['cartCount'] = localStorage.getItem('cartCount');
      this.browserStorage['currencyType'] = localStorage.getItem(
        'currencyType'
      );
      this.browserStorage['shopperId'] = localStorage.getItem('shopperId');
      this.browserStorage['package_details'] = localStorage.getItem(
        'package_details'
      );
      this.browserStorage['device'] = localStorage.getItem('device');
      this.browserStorage['recentActivity'] = localStorage.getItem('recentActivity');
      this.browserStorage['user'] = localStorage.getItem('user');
      this.browserStorage['cart_details'] = localStorage.getItem(
        'cart_details'
      );
      this.browserStorage['manual_device_change'] = sessionStorage.getItem(
        'manual_device_change'
      );
    } else if (isPlatformServer(this.platFormId)) {
      this.browserStorage['device'] = 'desktop';
    }
  }

  getLocalStorage(key: string): any {
    let value: string;
    if (key && this.browserStorage) {
      value = this.browserStorage[key];
    }
    return value;
  }

  setLocalStorage(key: string, value: string, isL = true): void {
    if (key && value && isPlatformBrowser(this.platFormId)) {
      if (isL) {
        localStorage.setItem(key, value);
      } else {
        sessionStorage.setItem(key, value);
      }
      this.browserStorage[key] = value;
    }else if(key && value){
      this.browserStorage[key] = value;
    }
  }

  emptyKeyLocalStorage(key: string, isL = true) {
    if (key && isPlatformBrowser(this.platFormId)) {
      if (isL) {
        localStorage.removeItem(key);
      } else {
        sessionStorage.removeItem(key);
      }
      this.browserStorage[key] = '';
    }else if(key){
      this.browserStorage[key] = "";
    }
  }
}
