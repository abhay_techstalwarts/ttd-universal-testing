import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class CurrencyService {
  constructor(private http: Http) {}

  private url = `${environment.apiBaseUrl}/currencies?status=1`;
  private geopluginUrl = `${environment.apiBaseUrl}/Get_ISP_DETAILS_BY_IP`;


  getCurrencies(): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(this.url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getCurrencyByLocation(): Observable<any> {
    const options = new RequestOptions({
      method: RequestMethod.Get,
    });
    return this.http.get(this.geopluginUrl, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }


  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }

  private serializeObj(obj: any) {
    const result = [];
    for (var property in obj)
      result.push(
        encodeURIComponent(property) + '=' + encodeURIComponent(obj[property])
      );
    return result.join('&');
  }
}
