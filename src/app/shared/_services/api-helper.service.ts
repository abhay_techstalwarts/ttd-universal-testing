import { BrowserStorageService } from './browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpParam } from '../_models/searchPageHttpParam';
import { CurrencyService } from './currency.service';
import * as _trimEnd from 'lodash/trimEnd';
import * as _trim from 'lodash/trim';

import { DefaultConstant } from '../_constants/default_constants';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiHelperService {
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private browserStorageService: BrowserStorageService,
    private currencyService: CurrencyService,
    @Inject(PLATFORM_ID) private platFormId: any
  ) {}

  getApiUrl(path: string) {
    return this.generateUrlWithPath(environment.apiBaseUrl, path);
  }

  generateUrlWithPath(url: string, path: string) {
    let currencyType = this.browserStorageService.getLocalStorage('currencyType');
    if (currencyType == null || currencyType === '' || currencyType === 'NULL') {
      currencyType = DefaultConstant.defaultCurrency;
      this.browserStorageService.setLocalStorage(
        'currencyType',
        currencyType,
        true
      );
    }
    return `${_trimEnd(url, '/')}/${_trim(path, '/')}?currency=${currencyType}`;
  }

  deleteEmptyParams(param: HttpParam) {
    for (const property in param) {
      if (param.hasOwnProperty(property)) {
        if (param[property] === '') {
          delete param[property];
        }
      }
    }
    return param;
  }
}
