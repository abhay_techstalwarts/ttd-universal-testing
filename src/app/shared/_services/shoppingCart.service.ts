import { BrowserStorageService } from './browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { ApiHelperService } from './api-helper.service';
import { map, catchError } from 'rxjs/operators';
import { CommonHelper } from '../_helper/common.helper';
import { empty, of } from 'rxjs';
import { AffiliateService } from "./affiliate.service";

@Injectable()
export class ShoppingCartService {
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private http: Http,
    private httpClient: HttpClient,
    private apiHelperService: ApiHelperService,
    private browserStorageService: BrowserStorageService,
    private affiliateService: AffiliateService
  ) {}

  private url = `${environment.apiBaseUrl}/addtocart`;

  getCityData(): Observable<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });
    return this.http.get(this.url, options).pipe(
      map(response => response.json()),
      catchError(this.handleError)
    );
  }

  getCartData(shopperId): Observable<any> {
    const url = this.apiHelperService.getApiUrl(`addtocart/${shopperId}`);
    return this.httpClient.get(url);
  }

  updateCartCount(): Observable<any> {
    if (
      this.browserStorageService.getLocalStorage('user') &&
      this.browserStorageService.getLocalStorage('shopperId') &&
      Number(this.browserStorageService.getLocalStorage('shopperId')) > 0
    ) {
      const shopperId = this.browserStorageService.getLocalStorage('shopperId');

      const url = this.apiHelperService.getApiUrl(`addtocart/${shopperId}`);
      return this.httpClient.get(url).pipe(
        map((response: any) => {
          if (response && response.result && response.result.item_counts) {
            this.browserStorageService.setLocalStorage(
              'cartCount',
              response.result.item_counts
            );
          } else {
            this.browserStorageService.setLocalStorage('cartCount', '0');
          }
        }),
        catchError((error: any) => {
          this.browserStorageService.setLocalStorage('cartCount', '0');
          return observableThrowError(error);
        })
      );
    } else {
      return empty();
    }
  }

  getAddedItem(shopperId): Observable<any> {
    const url = this.apiHelperService.getApiUrl(
      `recommended_activity/${shopperId}`
    );
    return this.httpClient.get(url);
  }

  getExpiredItems(shopperId): Observable<any> {
    // const currencyType = this.localStorage.getItem('currencyType');
    const currencyType = this.browserStorageService.getLocalStorage(
      'currencyType'
    );

    const url = `${
      environment.apiBaseUrl
    }/addtocart/${shopperId}?filter_by=expired&currency=${currencyType}`;
    return this.httpClient.get(url);
  }

  deletePackage(shopperId: string, item: any): Observable<any> {
    const url = this.apiHelperService.getApiUrl(
      `addtocart/${shopperId}/${item.package_id}`
    );
    const inputParams = {
      booking_date: CommonHelper.stringToDateFormater(
        item.booking_activity_date
      )
    };
    return this.httpClient.delete(url, { params: inputParams });
  }

  editPackage(
    shopperId: string,
    packageId: number,
    data: any
  ): Observable<any> {
    const url = this.apiHelperService.getApiUrl(
      `addtocart/${shopperId}/${packageId}`
    );
    data = this.affiliateService.bindAffiliateData(data);
    return this.httpClient.put(url, data);
  }

  getOrderDetails(orderId: number | string): Observable<any> {
    const url = `${environment.apiBaseUrl}/GetOrderDetails/${orderId}`;
    return this.httpClient.get(url);
  }

  getVoucherDetails(orderId, transactionId) {
    const url = `${
      environment.apiBaseUrl
    }/GetOrderDetails/${orderId}?transaction_id=${transactionId}`;
    return this.httpClient.get(url);
  }

  getQrcode(api): Observable<any> {
    const url = api;
    return this.httpClient.get(url);
  }

  getErrorMessage(orderId): Observable<any> {
    const url = `${environment.apiBaseUrl}/OrderPaymentDetails/${orderId}`;
    return this.httpClient.get(url);
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
