import { throwError as observableThrowError, Observable } from 'rxjs';
import {
  Injectable,
  Inject,
  RendererFactory2,
  ViewEncapsulation
} from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import {
  Http,
  Headers,
  Response,
  RequestOptions,
  RequestMethod
} from '@angular/http';
import { environment } from './../../../environments/environment';
import { DefaultConstant } from '../_constants/default_constants';
import { DOCUMENT } from '@angular/common';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class SEOProfileService {
  constructor(
    private http: Http,
    private metaService: Meta,
    @Inject(DOCUMENT) private doc,
    private rendererFactory: RendererFactory2,
    private titleService: Title
  ) { }

  private url = `${environment.apiBaseUrl}/seo_profile`;

  setSEOProfileDetails(id: any, type: string, metaImageUrl: string): Observable<any> {
    const url = `${this.url}/${id}`;
    const profileName = { profile_name: type };
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers,
      params: profileName
    });
    return this.http.get(url, options).pipe(
      map(response => {
        const data = response.json();
        this.createLinkForCanonicalURL();
        if (data && data.result) {
          this.setMetaTags(data.result, metaImageUrl);
        } else {
          this.setMetaTags(DefaultConstant.metaData, metaImageUrl);
        }
      }),
      catchError(this.handleError)
    );
  }

  setMetaTags(data: any, metaImageUrl: string) {
    if (data && data.page_title) {
      this.titleService.setTitle(data.page_title);
      this.metaService.updateTag({
        name: 'og:title',
        content: data.page_title
      });
      this.metaService.updateTag({
        name: 'twitter:title',
        content: data.page_title
      });
    }
    if (data && data.meta_keyword) {
      this.metaService.updateTag({
        name: 'keywords',
        content: data.meta_keyword
      });
    }
    if (data && data.meta_description) {
      this.metaService.updateTag({
        name: 'description',
        content: data.meta_description
      });
      this.metaService.updateTag({
        name: 'og:description',
        content: data.meta_description
      });
      this.metaService.updateTag({
        name: 'twitter:description',
        content: data.meta_description
      });
    }
    this.metaService.updateTag({
      name: 'twitter:image',
      content: metaImageUrl ? metaImageUrl : DefaultConstant.metaData.meta_image
    });
    this.metaService.updateTag({
      name: 'og:image',
      content: metaImageUrl ? metaImageUrl : DefaultConstant.metaData.meta_image
    });
  }

  createLinkForCanonicalURL() {
    this.removeTag('rel=canonical');
    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');
    this.doc.head.appendChild(link);
    link.setAttribute('href', `${DefaultConstant.Url}${this.doc.location.pathname}`);
  }

  removeTag(attrSelector: string) {
    if (attrSelector) {
      try {
        const renderer = this.rendererFactory.createRenderer(this.doc, {
          id: '-1',
          encapsulation: ViewEncapsulation.None,
          styles: [],
          data: {}
        });
        const head = this.doc.head;
        if (head === null) {
          throw new Error('<head> not found within DOCUMENT.');
        }
        const linkTags = this.doc.querySelectorAll(
          'link[' + attrSelector + ']'
        );
        for (const link of linkTags) {
          renderer.removeChild(head, link);
        }
      } catch (e) {
        console.log('Error while removing tag ' + e.message);
      }
    }
  }

  public handleError(error: Response) {
    return observableThrowError(error.json().error || 'Server error');
  }
}
