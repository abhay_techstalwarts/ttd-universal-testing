export class DefaultConstant {
  static cloudinaryImageBaseUrl = 'https://res.cloudinary.com/dcm/image/upload/';
  static securedUrl = [
    '/thankyou',
    '/paymentfail',
    '/pay',
    '/shoppingcart',
    '/home',
    '/home/bookings'
  ];
  static metaData = {
    'page_title': 'TicketsToDo - Activities, Tours, Attractions and Things To Do',
    'meta_keyword': 'Ticket, Activities, Tours, Attractions, Things To Do',
    // tslint:disable-next-line:max-line-length
    'meta_description': 'At TicketsToDo we offer a simple way to discover activities, attractions and things to do with just a few clicks. We aim at offering a one stop shop solution and the best online booking experience for all sorts of entertainment with a superior customer service.',
    'meta_image': 'http://res.cloudinary.com/dcm/image/upload/ar_1:1,c_fill,g_auto,e_art:hokusai/v1535448451/prod/t/ttd-meta-icon.png'
  };
  static Url = 'https://www.ticketstodo.com';
  static blogUrl = 'https://www.ticketstodo.com/blog/';
  static refundUrl = 'https://support.ticketstodo.com/hc/en-us/categories/115001538087-Refunds';
  static faqUrl = 'https://support.ticketstodo.com/hc/en-us/';
  static paymentMethodUrl = 'https://support.ticketstodo.com/hc/en-us/categories/115001538047-Payments-';
  static askTTDUrl = 'https://support.ticketstodo.com/hc/en-us/requests/new';
  static dcmTrackingUrl = 'https://go.urtrackinglink.com/SL1Bf?';
  static kidzappDomain = 'kidzapp';
  static defaultCurrency = 'AED';
  static whatsapp = {
    number:'971503369944',
    text:'I am looking for'
  }
  static telegram = {
    number:'971503369944',
    name:'YourName'
  }
}
