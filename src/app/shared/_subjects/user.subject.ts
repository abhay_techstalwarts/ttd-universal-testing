import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class UserService {
  private subject = new Subject<any>();
  private subject2 = new Subject<any>();

  clearUser(): Observable<any> {
    this.subject.next({ loggedIn: false });
    return this.subject.asObservable();
  }

  modifyUser(): Observable<any> {
    this.subject2.next();
    return this.subject2.asObservable();
  }
}
