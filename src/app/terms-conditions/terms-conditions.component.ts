import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { StaticPagesService, SEOProfileService } from '../shared/_services/index';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent implements OnInit {
  termsData: any;
  text: any;
  device: string;
  activityContent: string;

  constructor(
    @Inject(WINDOW) private window: Window, 
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platformID: any,
    private staticPagesService: StaticPagesService,
    private browserStorageService: BrowserStorageService,
    private seoProfileService: SEOProfileService
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformID)) {
      this.window.scrollTo(0, 0);
    }
    this.staticPagesService.getTermsAndConditions().subscribe(
      success => {
        this.termsData = success.result[0].page_content;
        this.activityContent = this.termsData;
        this.seoProfileService
          .setSEOProfileDetails(success.result[0].page_id, 'pages', '')
          .subscribe(_success => { }, _fail => { });
      },
      _fail => {

      }
    );
  }
}
