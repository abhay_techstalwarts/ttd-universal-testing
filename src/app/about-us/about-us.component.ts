import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { StaticPagesService,SEOProfileService } from '../shared/_services/index';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  aboutUsData: any;
  text: any;
  device: string;
  pageContent: string;
  currency: string;

  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platformId: any,
    private staticPagesService: StaticPagesService,
    private seoProfileService: SEOProfileService,
    private browserStorageService: BrowserStorageService
  ) {
    this.device = this.browserStorageService.getLocalStorage('device');
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.window.scrollTo(0, 0);
    }
    this.staticPagesService.getAboutUs().subscribe(
      success => {
        this.aboutUsData = success.result[0].page_content;
        this.pageContent = this.aboutUsData;
        this.seoProfileService
          .setSEOProfileDetails(success.result[0].page_id, 'pages', '')
          .subscribe(_success => { }, _fail => { });
      }
    );
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.currency = $event;
  }
}
