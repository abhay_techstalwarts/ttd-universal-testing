import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AboutUsComponent } from './about-us.component';
import { AboutUsRouting } from './about-us-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AboutUsComponent
  ],
  imports: [
    AboutUsRouting,
    CommonModule,
    SharedModule,
  ],
  exports: [
    AboutUsComponent
  ],
  providers: []
})
export class AboutUsModule {

}
