import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NouisliderModule } from 'ng2-nouislider';
import { SearchPageMobileComponent } from './search-page-mobile.component';
import { SearchMobileComponent } from '../shared/_components/search/search-mobile/search-mobile.component';
import { SharedModule } from '../shared/shared.module';
import { SearchPageRouting } from './search-page-routing.module';
@NgModule({
  declarations: [
    SearchPageMobileComponent,
    SearchMobileComponent
  ],
  imports: [
    SearchPageRouting,
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AngularMultiSelectModule,
    NouisliderModule
  ],
  exports: [
    SearchPageMobileComponent,
    SearchMobileComponent
  ],
  providers: [
  ]
})
export class SearchPageModule {
}
