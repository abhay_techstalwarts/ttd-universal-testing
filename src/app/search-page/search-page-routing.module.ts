import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchMobileComponent } from '../shared/_components/search/search-mobile/search-mobile.component';

const routes: Routes = [
  {path: '', component: SearchMobileComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})

export class SearchPageRouting {
}
