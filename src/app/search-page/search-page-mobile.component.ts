import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { SearchPageService, CityService } from '../shared/_services/index';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpParam } from '../shared/_models/searchPageHttpParam';
declare let $: any;

@Component({
  selector: 'app-search-page-mobile',
  templateUrl: 'search-page-mobile.component.html',
  styleUrls: ['./search-page-mobile.component.css']
})

export class SearchPageMobileComponent implements OnInit {
  text: object;
  user: object;
  searchKeyword: string;
  searchData: any;
  loading: boolean;
  cities: any;
  categories: any;
  cityIds = new Array();
  categoriesIds = new Array();
  httpParams = new HttpParam;
  priceRange: any = [0, 5000];

  itemList = new Array();
  selectedItems = [];
  settings = {
    singleSelection: false,
    text: 'Select Cities',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    badgeShowLimit: 3
  };

  categoryList = new Array();
  selectedCategories = [];
  categorySettings = {
    singleSelection: false,
    text: 'Select Categories',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    badgeShowLimit: 3
  };

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any, private activatedRoute: ActivatedRoute,
              private searchPageService: SearchPageService,
              private cityService: CityService,
              private browserStorageService:BrowserStorageService,
              location: PlatformLocation) {
    location.onPopState(() => {
      $('#filterModal').modal('hide');
    });
  }

  ngOnInit() {
    this.getSearchKeyword();
    this.getCities();
    this.getCategories();
  }

  getSearchKeyword() {
    this.searchKeyword = this.activatedRoute.snapshot.queryParamMap.get('search');
    this.httpParams.search = this.searchKeyword;
    this.search();
  }

  search() {
    this.loading = true;
    this.searchPageService.search(this.httpParams)
      .subscribe(
        data => {
          this.searchData = <object> <any> data.result;
          this.loading = false;
        },
        (_err: HttpErrorResponse) => {
          this.loading = false;
        }
      );
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.getSearchKeyword();
  }

// Categories starts ------------------------------------------------------------------------------

  getCategories() {
    this.searchPageService.getCategories()
      .subscribe(
        data => {
          this.categories = data.result;
          this.initializeCategories(data.result);
        }
      );
  }

  onCategorySelect(item: any) {
    this.categoriesIds.push(item);
  }

  OnCategoryDeSelect(item: any) {
    const index: number = this.categoriesIds.indexOf(item);
    if (index !== -1) {
      this.categoriesIds.splice(index, 1);
    }
  }

  onSelectAllCategory(items: any) {
    this.categoriesIds = [];
    for (let i = 0; i < items.length; i++) {
      this.categoriesIds.push(items[i].id);
    }
  }

  onDeSelectAllCategory(_items: any) {
    this.categoriesIds = [];
  }

  initializeCategories(categories) {
    for (let i = 0; i < categories.length; i++) {
      const element = {'id': categories[i].id, 'itemName': categories[i].name};
      this.categoryList.push(element);
    }
  }

  filterByCategories() {
    for (let i = 1; i <= this.categoriesIds.length; i++) {
      if (i === 1) {
        this.httpParams.category_id = this.categoriesIds[i - 1];
      } else {
        this.httpParams.category_id = this.httpParams.category_id + ',' + this.categoriesIds[i - 1];
      }
    }

    if (this.categoriesIds.length === 0) {
      delete this.httpParams['category_id'];
    }
  }

  clearCategoryFilter() {
    this.httpParams.category_id = '';
    this.categoriesIds = [];
    this.search();
    this.selectedCategories = [];
  }

  // Categories ends ------------------------------------------------------------------------------

  // cities starts ------------------------------------------------------------------------------

  getCities() {
    this.cityService.getCities()
      .subscribe(
        data => {
          this.cities = data.result;
          this.initializeCities(data.result);
        }
      );
  }

  onItemSelect(item: any) {
    this.cityIds.push(item);
  }

  OnItemDeSelect(item: any) {
    const index: number = this.cityIds.indexOf(item);
    if (index !== -1) {
      this.cityIds.splice(index, 1);
    }
  }

  onSelectAll(items: any) {
    this.cityIds = [];
    for (let i = 0; i < items.length; i++) {
      this.cityIds.push(items[i].id);
    }
  }

  onDeSelectAll(_items: any) {
    this.cityIds = [];
  }

  initializeCities(cities) {
    for (let i = 0; i < cities.length; i++) {
      const element = {'id': cities[i].city_id, 'itemName': cities[i].displayname};
      this.itemList.push(element);
    }
  }

  filterByCities() {
    for (let i = 1; i <= this.cityIds.length; i++) {
      if (i === 1) {
        this.httpParams.city_id = this.cityIds[i - 1];
      } else {
        this.httpParams.city_id = this.httpParams.city_id + ',' + this.cityIds[i - 1];
      }
    }

    if (this.cityIds.length === 0) {
      delete this.httpParams['city_id'];
    }
  }

  clearCityFilter() {
    this.httpParams.city_id = '';
    this.cityIds = [];
    this.search();
    this.selectedItems = [];
  }

  // Cities ends ------------------------------------------------------------------------------

  // Sort By starts ------------------------------------------------------------------------------
  sortBy(param: string) {
    this.httpParams.sort_by = param;
    this.search();
  }

  clearPriceRange() {
    this.priceRange = [0, 5000];
    this.httpParams.price_from = this.priceRange[0];
    this.httpParams.price_to = this.priceRange[1];
    this.search();
  }

  filterByPrice() {
    this.httpParams.price_from = this.priceRange[0];
    this.httpParams.price_to = this.priceRange[1];
  }

  applyFilters() {
    this.filterByCategories();
    this.filterByCities();
    this.filterByPrice();
    this.search();
    $('#filterModal').modal('hide');
  }
}

