import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import {
  ActivityComponent,
} from './index';
import { SharedModule } from '../shared/shared.module';
import { ActivityRouting } from './activity-routing.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    ActivityComponent,
  ],
  imports: [
    ActivityRouting,
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA8tN4tdy2crRpSA79Xf8exRCjo2UWqoIc'
    }),
  ],
  exports: [
    ActivityComponent,
  ],
  providers: []
})
export class ActivityModule {

}
