import { isPlatformBrowser, isPlatformServer, formatDate } from '@angular/common';
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  KeyValueDiffers,
  KeyValueDiffer,
  Inject,
  PLATFORM_ID,
  HostListener,
  OnDestroy
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from './../../environments/environment';
import {
  AlertService,
  ActivityService,
  AuthenticationService,
  CartService
} from '../shared/_services/index';
import { SEOProfileService, ShoppingCartService } from '../shared/_services';
import { DefaultConstant } from '../shared/_constants/default_constants';
import { JsonLdService } from '../shared/_services/json-ld.service';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { FullCalendarService } from '../shared/_services/fullCalendar.service';
import { Cart } from '../shared/_models/cart';
import { LocalCartHelper } from '../shared/_helper/local-cart.helper';
import { FormHelper } from '../shared/_helper/form-helper';
import { ValidationManager } from 'ng2-validation-manager';
import swal from 'sweetalert2';
import { CommonHelper } from '../shared/_helper/common.helper';
import * as _filter from 'lodash/filter';
import * as _find from "lodash/find";
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { FormArray } from '@angular/forms';
declare let jQuery: any;
declare var StickySidebar: any;
@Component({
  moduleId: module.id.toString(),
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit, OnDestroy {
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  text: any;
  activityData: any;
  play = false;
  device: string;
  addToCartForm;
  cartGroupPriceDetails: any;
  cart = new Cart();
  noOfItem: any;
  menuItems: any;
  scrollItems: any;
  lastId: any;
  avail_conf: { availability: any, confirmation: any };
  currency: any;
  canAddToCart: boolean;
  hideBookFixedDiv = false;
  date = new Date();
  packageDate = new Date();
  checkingAvailability: boolean;
  selectedCalenderPkgId:any;
  @ViewChild('addToCartButton') addToCartButton: ElementRef;
  @ViewChild('mymap') mymap: ElementRef;
  @ViewChild('calender') calender: ElementRef;

  differ: KeyValueDiffer<string, any>;

  canSelectTime: boolean;
  canSelectGroup: boolean;
  selectedPackageId;
  timeSlots;
  packageGroups;
  current;
  cityName: string;
  cityId: number;
  sideBar;
  appiBaseUrl = environment.serverBaseUrl;
  isServer = false;
  shouldShow = false;
  shouldShowCalender = false;
  afterinit = false;
  imgWidth='';
  totalPrice = 0;


  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(WINDOW) private window: Window,
    @Inject(PLATFORM_ID) private platformId: any,
    private route: ActivatedRoute,
    private seoProfileService: SEOProfileService,
    private shoppingCartService: ShoppingCartService,
    private cartService: CartService,
    private jsonLdService: JsonLdService,
    private activityService: ActivityService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private differs: KeyValueDiffers,
    private alertService: AlertService,
    private browserStorageService: BrowserStorageService,
    private fullCalendarService: FullCalendarService
  ) {
    this.getActivityData();
    this.differ = this.differs.find({}).create();
    if (isPlatformServer(this.platformId)) {
      this.isServer = true;
    }
  }

  getActivityData() {
    this.route.data.subscribe(data => {
      const activityData = data.activityData.result;
      let metaImageUrl = '';
      if (activityData) {
        // tslint:disable-next-line:max-line-length
        metaImageUrl = activityData[0].thumbnail_image_path_name? this.cloudinaryImageBaseUrl + 'c_fill,w_1200,h_630,f_auto/v1/' + environment.cloudinaryServer + '/' + activityData[0].thumbnail_image_path_name : '';

        // Open below part whenever API is ready
        if (activityData[0].profile_details) {
          this.seoProfileService.setMetaTags(activityData[0].profile_details , metaImageUrl);
        }
      }
      if (activityData && activityData[0]) {
        this.jsonLdService.setSchemaObjects(activityData[0]);
      }
    });
  }

  ngOnInit() {
    this.shoppingCartService.updateCartCount().subscribe(_success => {}, _error => {});
    if (isPlatformBrowser(this.platformId)) {
      this.window.scrollTo(0, 0);
      if (this.window.screen.width < 769) {
        this.imgWidth =`q_70,w_${this.window.screen.width}`;
      }else{
        this.imgWidth = 'q_70';
      }

    }
    if (this.authenticationService.getUser()) {
      this.addToCartForReturnUrl();
    }
    this.initiateForm();

    this.getDataOnLoad();
    if (isPlatformBrowser(this.platformId)) {
      setTimeout(() => {
        this.sideBar = new StickySidebar('#stickyBox', {
          containerSelector: '#stickyContainer',
          topSpacing: 115,
          bottomSpacing: 0
        });
      }, 1000);

      if (this.activityData[0].package_details) {

        this.activityData[0].package_details.forEach(package_detail => {
          if (package_detail.schedule_details[0].availability && (new Date() < new Date(package_detail.schedule_details[0].availability))) {
            if (this.avail_conf && this.avail_conf.availability ) {
              this.avail_conf = new Date(this.avail_conf.availability) > new Date(package_detail.schedule_details[0].availability) ? { availability: package_detail.schedule_details[0].availability, confirmation: package_detail.schedule_details[0].confirmation } : this.avail_conf;
            } else {
              this.avail_conf = { availability: package_detail.schedule_details[0].availability, confirmation: package_detail.schedule_details[0].confirmation };
            }

          }
        });
        if (this.avail_conf && this.avail_conf.availability  !== undefined) {
          let tomorrow = new Date();
          tomorrow.setDate(tomorrow.getDate() + 1);
          if (formatDate(tomorrow, 'yyyy-MM-dd', 'en') == formatDate(this.avail_conf.availability, 'yyyy-MM-dd', 'en')) {
            this.avail_conf.availability ='Available Tomorrow';
          }else{
            this.avail_conf.availability = `Earliest available date: ${formatDate(this.avail_conf.availability, 'dd-MM-yyyy', 'en')}`;
          }
        }


      }

    }
    this.scrollspy();
  }
  ngAfterViewInit() {
    // scroll to package options
    if (this.router.url.split('edit=')[1] === 'true') {
      this.scroll('packageOptions', 50);
      this.hideBookFixedDiv = true;
    }

    setTimeout(() =>
      this.afterinit = true, 3000);
    this.fullCalenderInit();
    this.subscribedData();
    const that = this;
    jQuery(document).ready(function () {
      const mymapObserver = new IntersectionObserver((entries, mapObserver) => {
        entries.forEach(entry => {
          const lazyMap = entry.target;
          if (entry.isIntersecting) {
            that.shouldShow = true;
            mapObserver.unobserve(lazyMap);
          }
        });
      });
      mymapObserver.observe(that.mymap.nativeElement);
    });
    jQuery(document).ready(function () {
      const calenderObserver = new IntersectionObserver((entries, calender) => {
        entries.forEach(entry => {
          const lazyMap = entry.target;
          if (entry.isIntersecting) {
            that.shouldShowCalender = true;
            calender.unobserve(lazyMap);
          }
        });
      });
      calenderObserver.observe(that.calender.nativeElement);
    });
  }

  selectedCalenderPackage(event){
    this.selectedCalenderPkgId = event;    
  }

  fullCalenderInit() {
    this.fullCalendarService.sendCalendarData('activityDate', this.date);
    this.fullCalendarService.sendCalendarData('packageDate', this.packageDate);
  }

  @HostListener('window:scroll', ['$event'])
  onScrollEvent(_$event) {
    if (isPlatformBrowser(this.platformId)) {
      const y = jQuery(document).scrollTop();
      const t = jQuery('#stickyContainer').offset().top - 100;
      
      if (y > t) {
        jQuery('.activity_nav_sections').fadeIn();
        jQuery('.white-nav').css('box-shadow', 'unset')
        
      } else {
        jQuery('.activity_nav_sections').fadeOut();
        jQuery('.white-nav').css('box-shadow', '0 2px 6px rgba(0,0,0,0.12)');
      }
      const fromTop = jQuery(document).scrollTop() + 200;
      let id = '';
      for (const item of this.scrollItems) {
        if (jQuery(item).offset().top < fromTop) {
          id = item[0].id;
        }
      }

      // Set/remove active class
      this.menuItems.removeClass('active');
      jQuery('[href=\'#' + id + '\']').addClass('active');
    }
  }

  subscribedData() {
    this.fullCalendarService.getCalendarData().subscribe(event => {
      const action = event.action;
      switch (action) {
        case 'activityDate': {
          this.date = event.data;
          this.packageDate = event.data;
          break;
        }
        case 'packageDate': {
          this.packageDate = event.data;
          break;
        }
      }
    });
  }
  scrollspy() {
    // Cache selectors
    const topMenu = jQuery('#activity_nav_container'),
      topMenuHeight = topMenu.outerHeight() + 15;
    // All list items
    this.menuItems = topMenu.find('a');
    // Bind click handler to menu items
    this.scrollItems = this.menuItems.map(function () {
      const item = jQuery(jQuery(this).attr('href'));
      if (item.length) {
        return item;
      }
    });
    // so we can get a fancy scroll animation
    this.menuItems.click(function (e) {
      const href = jQuery(this).attr('href'),
        offsetTop =
          href === '#' ? 0 : jQuery(href).offset().top - topMenuHeight - 100;
      jQuery('html, body')
        .stop()
        .animate(
          {
            scrollTop: offsetTop
          },
          300
        );
      e.preventDefault();
    });
  }

  clickBookFixedDiv() {
    this.hideBookFixedDiv = true;
    this.scroll('packageOptions', 50);
  }
  // @HostListener('scroll', ['$event']) onScroll(event) {
  //   this.hideBookFixedDiv = false;
  //   console.log(event);
  //   console.log('scrolling');
  //   // tslint:disable-next-line: no-debugger
  //   debugger;
  // }

  scroll(el, offset = 0) {
    jQuery('html, body').animate(
      {
        scrollTop: jQuery('#' + el).offset().top - offset
      },
      400
    );
  }

  stickSidebar() {
    if (isPlatformBrowser(this.platformId)) {
      setTimeout(() => {
        if (jQuery('#stickyBox').length) {
          this.sideBar = new StickySidebar('#stickyBox', {
            containerSelector: '#stickyContainer',
            topSpacing: 115,
            bottomSpacing: 0
          });
        }
        if (isPlatformBrowser(this.platformId)) this.window.scrollBy(0, 1);
      }, 0);
    }
  }

  getDataOnLoad() {
    this.route.data.subscribe(data => {
      this.activityData = data.activityData.result;
      if (this.activityData && this.activityData[0].city_name) {
        this.cityName = this.activityData[0].city_name;
        this.cityId = this.activityData[0].city_id;
      }
      if (
        this.activityData &&
        this.activityData[0] &&
        this.activityData[0].global_min_booking_date
      ) {
        const min_booking_date = new Date(
          this.activityData[0].global_min_booking_date
        ).getTime();
        const min_date = new Date(min_booking_date - 24 * 60 * 60 * 1000);

        this.date = new Date(this.activityData[0].global_min_booking_date);
        this.packageDate = new Date(
          this.activityData[0].global_min_booking_date
        );
      }
    });
    this.setToRecentActivity();
  }

  optimizeImage(img) {
    const image = img.substr(0, img.lastIndexOf('.')) || img;
    return `${image}.webp`;
  }

  loadActivity() {
    let activityId = '';
    if (this.activityData === undefined) {
      const urlString: any = this.router.url.split('/');
      activityId = urlString[urlString.length - 1].split('-')[0];
    } else {
      activityId = this.activityData[0].activity_id;
    }
    this.activityService.getActivityData(activityId).subscribe(success => {
      this.activityData = success.result;
      if (this.activityData && this.activityData[0].city_name) {
        this.cityName = this.activityData[0].city_name;
        this.cityId = this.activityData[0].city_id;
      }
      if (
        this.activityData &&
        this.activityData[0] &&
        this.activityData[0].global_min_booking_date
      ) {
        const min_booking_date = new Date(
          this.activityData[0].global_min_booking_date
        ).getTime();
        const min_date = new Date(min_booking_date - 24 * 60 * 60 * 1000);

        this.date = new Date(this.activityData[0].global_min_booking_date);
        this.packageDate = new Date(
          this.activityData[0].global_min_booking_date
        );
      }
    });
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event); // this.localStorage.setItem('currencyType', $event);
    this.currency = this.browserStorageService.getLocalStorage('currencyType'); // this.localStorage.getItem('currencyType');
    this.loadActivity();
    if (
      this.selectedPackageId &&
      this.selectedPackageId > 0 &&
      this.packageDate
    ) {
      this.cancelAddToCart();
    }
  }

  playVideo() {
    this.play = true;
    let vid;
    if (isPlatformBrowser(this.platformId)) {
      vid = <HTMLVideoElement>document.getElementById('myVideo');
      vid.play();
    }
  }

  // Affiliate book
  book(packageDetails) {
    if (!packageDetails.affiliate_url) {
    } else {
      if (isPlatformBrowser(this.platformId))
        this.window.open(packageDetails.affiliate_url);
    }
  }

  // cart functions ---------------------------------------------
  setDate(pkg: any, target, i) {
    this.totalPrice = 0;
    this.scroll(target, 140);
    this.hideBookFixedDiv = true;
    this.initiateForm();
    this.initializeProperties();
    setTimeout(() => {
      this.current = i;
    }, 500);
    this.selectedPackageId = pkg.package_id;

    const todaysDate = new Date();
    if (pkg.schedule_details && pkg.schedule_details[0]) {
      const pkgSchedule = pkg.schedule_details[0];
      const daysDisabled: number[] = [];
      if (!pkgSchedule.activity_on_SU || pkgSchedule.activity_on_SU === 0) {
        daysDisabled.push(0);
      }
      if (!pkgSchedule.activity_on_M || pkgSchedule.activity_on_M === 0) {
        daysDisabled.push(1);
      }
      if (!pkgSchedule.activity_on_T || pkgSchedule.activity_on_T === 0) {
        daysDisabled.push(2);
      }
      if (!pkgSchedule.activity_on_W || pkgSchedule.activity_on_W === 0) {
        daysDisabled.push(3);
      }
      if (!pkgSchedule.activity_on_TH || pkgSchedule.activity_on_TH === 0) {
        daysDisabled.push(4);
      }
      if (!pkgSchedule.activity_on_F || pkgSchedule.activity_on_F === 0) {
        daysDisabled.push(5);
      }
      if (!pkgSchedule.activity_on_S || pkgSchedule.activity_on_S === 0) {
        daysDisabled.push(6);
      }
      this.fullCalendarService.sendCalendarData('disabledDays', daysDisabled);
    }
    if (this.date.getDate() !== todaysDate.getDate() &&  this.selectedCalenderPkgId == this.selectedPackageId) {
      this.addToCartForm.setValue('booking_activity_date', this.date);
      this.packageDate = this.date;
      const formattedPackageDate = CommonHelper.dateFormaterNew(this.date);
      this.loadPackageTimeSlots(this.selectedPackageId, formattedPackageDate);
    } else {
    }
  }

  goToLogin() {
    const returnUrl = this.router.url;
    this.router.navigate(['login'], { queryParams: { returnUrl: returnUrl } });
  }

  goToProfile() {
    const returnUrl = this.router.url;
    this.router.navigate(['home'], { queryParams: { returnUrl: returnUrl } });
  }

  jsonParse(data) {
    if (data) {
      return JSON.parse(data);
    }
    return [];
  }

  addToCart(bookNow?: boolean) {
    const cartData = this.addToCartForm.getData();
    // tslint:disable-next-line:max-line-length
    cartData.group_price_details = _filter(
      cartData.group_price_details,
      function (obj) {
        return obj.quantity !== 0 && obj.quantity !== '0';
      }
    );
    if (this.authenticationService.getUser()) {
      if (
        !this.authenticationService.getUser().firstname ||
        this.authenticationService.getUser().firstname === ''
      ) {
        LocalCartHelper.addItem(cartData);
        this.goToProfile();
      } else {
        if (bookNow) {
          this.bookNow();
        } else {
          this.addToServerCart();
        }
      }
    } else {
      LocalCartHelper.addItem(cartData);
      this.goToLogin();
    }
  }

  addToServerCart() {
    this.addToCartForm.setValue(
      'email',
      JSON.parse(this.browserStorageService.getLocalStorage('user')).email
    );
    FormHelper.disableButton(this.addToCartButton, 'ADDING...');
    const cartData = this.addToCartForm.getData();
    // tslint:disable-next-line:max-line-length
    cartData.group_price_details = _filter(
      cartData.group_price_details,
      function (obj) {
        return obj.quantity !== 0 && obj.quantity !== '0';
      }
    );
    this.cartService.addToCart(cartData).subscribe(
      success => {
        this.takePermission();
        this.noOfItem = success.result.item_counts;

        this.browserStorageService.setLocalStorage(
          'cartCount',
          success.result.item_counts
        );
        FormHelper.enableButton(this.addToCartButton, 'ADD TO CART');
      },
      fail => {
        FormHelper.enableButton(this.addToCartButton, 'ADD TO CART');
        if (fail.error.message) {
          swal({
            type: 'error',
            text: fail.error.message,
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Keep looking',
            confirmButtonText: 'Checkout'
          }).then(result => {
            if (result.value) {
              this.router.navigate(['/shoppingcart']);
            }
          });
        } else {
          swal({
            type: 'error',
            text: 'Adding to cart failed',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Keep looking',
            confirmButtonText: 'Checkout'
          }).then(result => {
            if (result.value) {
              this.router.navigate(['/shoppingcart']);
            }
          });
        }
      }
    );
  }

  addToCartForReturnUrl() {
    if (LocalCartHelper.getCart()) {
      const cart = LocalCartHelper.getCart();
      cart.email = JSON.parse(
        this.browserStorageService.getLocalStorage('user')
      ).email; // JSON.parse(this.localStorage.getItem('user')).email;
      this.cartService.addToCart(cart).subscribe(
        success => {
          this.takePermission();
          this.noOfItem = success.result.item_counts;
          this.browserStorageService.setLocalStorage(
            'shopperId',
            success.result.data[0].shopper_id
          ); // this.localStorage.setItem('shopperId', success.result.data[0].shopper_id);
          this.browserStorageService.setLocalStorage(
            'cartCount',
            success.result.item_counts
          ); // this.localStorage.setItem('cartCount', success.result.item_counts);
          LocalCartHelper.deleteCart();
        },
        fail => {
          LocalCartHelper.deleteCart();
          swal({
            type: 'error',
            text: fail.error.message,
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Keep looking',
            confirmButtonText: 'Checkout'
          }).then(result => {
            if (result.value) {
              this.router.navigate(['/shoppingcart']);
            }
          });
        }
      );
    }
  }

  takePermission() {
    this.router.navigate(['/addtocart'], {
      queryParams: {
        city_id: this.cityId,
        shoppingcart_id: this.localStorage.getItem('shopperId')
      }
    });
  }

  goToAddCart(shopperId: any) {
    this.router.navigate(['/addToCart', shopperId]);
  }

  // Cart input data---------------------------------------
  addGroup(pkgGroups) {
    this.addToCartForm.setValue('group_price_details', []);
    for (let i = 0; i < pkgGroups.length; i++) {
      if (
        !_find(this.addToCartForm.getValue('group_price_details'), [
          'group_price_id',
          pkgGroups[i].group_price_id
        ])
      ) {
        this.addToCartForm.addChildGroup(
          'group_price_details',
          new ValidationManager({
            group_price_id: {
              rules: 'required',
              value: pkgGroups[i].group_price_id
            },
            quantity: {
              rules: 'required',
              value: pkgGroups[i].min_quantity.toString()
            },
            partner_id: {
              rules: '',
              value: pkgGroups[i].partner_id
            },
            discounted_price: {
              rules: '',
              value: pkgGroups[i].discounted_price
            }
          })
        );
      }
    }
    this.totalAmount();
  }

  initiateForm() {
    this.addToCartForm = new ValidationManager({
      email: {
        rules: ''
      },
      package_id: {
        rules: 'required'
      },
      currency: {
        rules: 'required',
        value: this.browserStorageService.getLocalStorage('currencyType')
      },
      discount: {
        rules: 'required',
        value: '0'
      },
      booking_activity_date: {
        rules: 'required'
      },
      booking_activity_time: {
        rules: ''
      },
      tracking_url: {
        rules: 'required',
        value: 'https://api.ticketstodo.com/addtocart'
      },
      total_participants: {
        rules: 'required'
      },
      total_amount: {
        rules: 'required'
      },
      group_price_details: []
    });
  }

  onFormSubmit(pkg: any) {
    this.addToCartForm.setValue('package_id', pkg.package_id);
    const groupDetails = this.addToCartForm.getValue('group_price_details');
    const groupPriceDetails = _filter(groupDetails, function (obj) {
      return obj.quantity !== 0 && obj.quantity !== '0';
    });
    // count total participants & total amount
    this.addToCartForm.setValue('total_participants', 0);
    this.addToCartForm.setValue('total_amount', 0);
    for (let i = 0; i < groupPriceDetails.length; i++) {
      this.addToCartForm.setValue(
        'total_participants',
        Number(this.addToCartForm.getValue('total_participants')) +
        Number(groupPriceDetails[i].quantity)
      );
      this.addToCartForm.setValue(
        'total_amount',
        Number(this.addToCartForm.getValue('total_amount')) +
        Number(
          groupPriceDetails[i].discounted_price *
          groupPriceDetails[i].quantity
        )
      );
    }
    if (this.addToCartForm.isValid() && groupPriceDetails.length !== 0) {
      this.canAddToCart = true;
      this.stickSidebar();
    } else {
      this.canAddToCart = false;
      this.stickSidebar();
    }
  }

  cancelAddToCart() {
    this.initiateForm();
    this.initializeProperties();
    this.current = 'notNumber';
    this.stickSidebar();
  }

  // Check availabilty
  ngDoCheck() {
    const change = this.differ.diff(this);
    if (change) {
      change.forEachChangedItem(item => {
        if (item.key === 'date') {
          this.checkAvailability();
        } else if (
          item.key === 'packageDate' &&
          this.selectedPackageId &&
          this.selectedPackageId > 0
        ) {
          const formattedPackageDate = CommonHelper.dateFormaterNew(
            this.packageDate
          );
          this.addToCartForm.setValue(
            'booking_activity_date',
            formattedPackageDate
          );
          this.loadPackageTimeSlots(
            this.selectedPackageId,
            formattedPackageDate
          );
        }
      });
    }
  }

  checkAvailability() {
    this.initializeProperties();
    jQuery('#packages').addClass('disabledbutton');
    this.checkingAvailability = true;
    this.activityService
      .checkAvailability(
        this.activityData[0].activity_id,
        CommonHelper.dateFormaterNew(this.date)
      )
      .subscribe(
        success => {
          this.checkingAvailability = false;
          jQuery('#packages').removeClass('disabledbutton');
          this.activityData[0].package_details = success.result.package_details;
          this.current = 'notNumber';
        },
        _fail => {
          this.checkingAvailability = false;
          jQuery('#packages').removeClass('disabledbutton');
          this.alertService.error('Failed to filter packages');
        }
      );
  }

  loadPackageTimeSlots(packageId, formattedPackageDate) {
    if (
      this.activityData[0] &&
      this.activityData[0].type &&
      this.activityData[0].type === 'open'
    ) {
      this.loadPackageGroups(null);
    } else {
      this.activityService
        .getPackageTimeSlots(packageId, formattedPackageDate)
        .subscribe(
          success => {
            this.canSelectTime = true;
            this.timeSlots = success.result;
          },
          fail => {
            // TTD-1509
            // this.alertService.error(fail.message);
          }
        );
    }
  }

  // Book Now starts here
  bookNow() {
    this.addToCartForm.setValue(
      'email',
      JSON.parse(this.browserStorageService.getLocalStorage('user')).email
    );
    const cartData = this.addToCartForm.getData();
    // tslint:disable-next-line:max-line-length
    cartData.group_price_details = _filter(
      cartData.group_price_details,
      function (obj) {
        return obj.quantity !== 0 && obj.quantity !== '0';
      }
    );
    this.cartService.addToCart(cartData).subscribe(
      success => {
        this.noOfItem = success.result.item_counts;
        this.browserStorageService.setLocalStorage(
          'cartCount',
          success.result.item_counts
        );

        const selectedItems = new Array();
        const obj = {
          package_id: cartData.package_id,
          booking_date: CommonHelper.stringToDateFormater(
            cartData.booking_activity_date
          )
        };
        selectedItems.push(obj);
        this.browserStorageService.setLocalStorage(
          'package_details',
          JSON.stringify(selectedItems)
        );
        this.router.navigate(['/pay']);
      },
      fail => {
        if (fail.error.message) {
          this.alertService.error(fail.error.message);
        } else {
          this.alertService.error('Failed to book. Please try again');
        }
      }
    );
  }

  loadPackageGroups(time) {
    time = CommonHelper.extractStartTime(time);
    this.activityService
      .getPackageGroups(this.selectedPackageId, this.packageDate, time)
      .subscribe(
        success => {
          this.packageGroups = success.result;
          this.addGroup(success.result);
          this.canSelectGroup = true;
        },
        _fail => {
          this.alertService.error('Failed to fetch package group details');
        }
      );
  }

  initializeProperties() {
    this.canSelectTime = false;
    this.canSelectGroup = false;
    this.selectedPackageId = 0;
    this.timeSlots = null;
    this.packageGroups = null;
    this.canAddToCart = false;
  }

  validateMaxQty(event, group) {
    if (event.target.value < group.min_quantity) {
      event.target.value = group.min_quantity;
    } else if (
      group.max_quantity > 0 &&
      event.target.value > group.max_quantity
    ) {
      event.target.value = group.max_quantity;
    }
  }

  preventBackSpace(event) {
    const elid = jQuery(document.activeElement).hasClass('textInput');
    if (event.keyCode === 8 && !elid) {
      return false;
    }
  }

  getQuantityValue(groupIndex: number) {
    const groupDetails = this.addToCartForm.getValue('group_price_details');
    const item = groupDetails[groupIndex];
    return item ? Number(item.quantity) : 0;
  }

  addQuantity(groupIndex: number, maxQuantity: number) {
    const groupControl = (<FormArray>(
      this.addToCartForm.controls.group_price_details.control
    )).at(groupIndex) as any;
    const currentQty = Number(groupControl['controls'].quantity.value);
    if (currentQty < maxQuantity) {
      groupControl.controls['quantity'].setValue(String(currentQty + 1));
      this.totalAmount();

    }
  }

  removeQuantity(groupIndex: number, minQuantity: number) {
    const groupControl = (<FormArray>(
      this.addToCartForm.controls.group_price_details.control
    )).at(groupIndex) as any;
    const currentQty = Number(groupControl['controls'].quantity.value);
    if (currentQty > minQuantity) {
      groupControl.controls['quantity'].setValue(String(currentQty - 1));
      this.totalAmount();
    }
  }

  totalAmount() {
    const packageGroupsValue = <FormArray>(
      this.addToCartForm.controls.group_price_details.control
    )['value'];
    this.totalPrice = 0;
    for (let index = 0; index < packageGroupsValue.length; index++) {
      this.totalPrice += packageGroupsValue[index]['discounted_price'] * Number(packageGroupsValue[index]['quantity']);
    }
  }

  // set this Activity in session storage for recent activity
  setToRecentActivity() {
    if (this.activityData && this.activityData[0]) {
      const {
        activity_id,
        name,
        min_default_discounted_price,
        min_default_original_price,
        request_currency,
        thumbnail_image_path_name
      } = this.activityData[0];
      const activity = {
        activity_id,
        url: this.router.url,
        name,
        min_default_discounted_price,
        min_default_original_price,
        request_currency,
        thumbnail_image_path_name
      };
      this.activityService.setRecentActivity(activity);
    }
  }

  ngOnDestroy() {
    this.jsonLdService.removeTag('type="application/ld+json"');
  }
}
