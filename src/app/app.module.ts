﻿﻿import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HomeModule } from './home/home.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { SharedModule } from './shared/shared.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { HomeComponent } from './home/index';

import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';

// Configs
const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(
      '1095380391155-pvick9hgca55105m63sims9d0amq605k.apps.googleusercontent.com'
    )
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('693753777466558')
  }
]);

export function provideConfig() {
  return config;
}


// Shared
import {
  AffiliateService,
  AlertService,
  AuthenticationService,
  HomeService,
  CityService,
  LanguageService,
  ActivityService,
  ArticleService,
  StaticPagesService,
  SubscribeService,
  SearchService,
  CurrencyService,
  ForgetpwdService,
  SearchPageService,
  ApiHelperService,
  CartService,
  UserdataService,
  ShoppingCartService,
  SEOProfileService,
  BrowserStorageService,
  CountryService
} from './shared/_services/index';
import { FullCalendarService } from './shared/_services/fullCalendar.service';
import { AuthGuard } from './shared/_guards/index';
import { ActivityResolve } from './shared/_resolver/activity-resolve.service';
import { CityResolve } from './shared/_resolver/city-resolve';
import { ArticleResolve } from "./shared/_resolver/article-resolve.service";
import { AddToCartResolve } from './shared/_resolver/add-to-cart-resolve.service';
import { CheckoutResolve } from './shared/_resolver/checkout-resolve.service';
import { UserService } from './shared/_subjects/user.subject';
import { DefaultConstant } from './shared/_constants/default_constants';
import { TableFilterableDirective } from './shared/_directives/filter.directive';
import { AlertComponent } from './_directives/index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { ForgotPasswordComponent } from './forget-password/forget-password.component';
import { ApiService } from './shared/_services/api.service';
import { CheckoutComponent } from './checkout-page/checkout.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { PaymentfailComponent } from './paymentfail/paymentfail.component';
import { RecommendComponent } from './recommend/recommend.compnent';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GoTopButtonModule } from 'ng2-go-top-button';
import { DeviceDetectorModule } from 'ngx-device-detector';

@NgModule({
  imports: [
    CommonModule,
    NgtUniversalModule,

    TransferHttpCacheModule,
    HttpClientModule,

    FormsModule,
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    LazyLoadImageModule,
    BrowserAnimationsModule,
    PaginationModule.forRoot(),
    InfiniteScrollModule,
    SocialLoginModule,
    GoTopButtonModule,
    DeviceDetectorModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    PrivacyPolicyComponent,
    TermsConditionsComponent,
    TableFilterableDirective,
    ForgotPasswordComponent,
    CheckoutComponent,
    ThankyouComponent,
    PaymentfailComponent,
    HomeComponent,

    // Desktop component
    RecommendComponent,
  ],
  providers: [
    AffiliateService,
    AuthGuard,
    AlertService,
    AuthenticationService,
    HomeService,
    CityService,
    LanguageService,
    ActivityService,
    ArticleService,
    StaticPagesService,
    SubscribeService,
    ActivityResolve,
    CityResolve,
    ArticleResolve,
    AddToCartResolve,
    CheckoutResolve,
    UserService,
    DefaultConstant,
    SearchService,
    CurrencyService,
    ForgetpwdService,
    SearchPageService,
    ApiService,
    CartService,
    ApiHelperService,
    UserdataService,
    ShoppingCartService,
    SEOProfileService,
    BrowserStorageService,
    CountryService,
    FullCalendarService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ]
})
export class AppModule {}
