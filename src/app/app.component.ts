﻿﻿import { Component, Inject, PLATFORM_ID, Optional } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { DefaultConstant } from './shared/_constants/default_constants';
import { BrowserStorageService } from './shared/_services/browserStorage.service';
import { isPlatformBrowser } from '@angular/common';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { AffiliateService } from "./shared/_services";
@Component({
  moduleId: module.id.toString(),
  selector: 'app-ticketstodo',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private affiliateService: AffiliateService,
    private router: Router,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private browserStorageService: BrowserStorageService,
    @Inject(PLATFORM_ID) private platFormId: any,
    @Optional() @Inject(REQUEST) private request: any,
    private activatedRoute: ActivatedRoute
  ) {
    if (isPlatformBrowser(this.platFormId)) {
      this.ifRefererExist(_document.referrer);
      if (_document.referrer) {
        let a = _document.referrer.split("://");
        if (a.length > 1) {
          let b = a[1].split("/");
          this.affiliateService.getAffiliateId(b[0])
            .subscribe(
              success => {
                this.browserStorageService.setLocalStorage(
                  'Affiliate',
                  JSON.stringify(success.result),
                  false
                );
              },
              _fail => { }
            );
        }

      }

    } else {
      const referer = this.request.get('referer');
      console.log('referrer url : ', referer);
      this.ifRefererExist(referer);
      this.browserStorageService.setLocalStorage('currencyType', 'AED', true);
    }
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/pay') {
          this._document
            .getElementById('ttdFavicon')
            .setAttribute('href', 'assets/TTDfavicon-inv.ico');
        } else {
          this._document
            .getElementById('ttdFavicon')
            .setAttribute('href', 'assets/TTDfavicon.ico');
        }
      }
    });
    // debugger;
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['txn_id']) {
        this.browserStorageService.setLocalStorage('txn_id', params['txn_id'], false);
      }
    });
  }

  ifRefererExist(referrer) {
    if (referrer) {
      if (this.isValidUrl(referrer)) {
        const hostName = new URL(referrer).hostname;
        if (hostName.includes(DefaultConstant.kidzappDomain)) {
          this.browserStorageService.setLocalStorage(
            'from_kidzapp',
            'true',
            false
          );
        } else {
          this.browserStorageService.setLocalStorage(
            'from_kidzapp',
            'false',
            false
          );
        }
      } else {
        if (referrer.includes(DefaultConstant.kidzappDomain)) {
          this.browserStorageService.setLocalStorage(
            'from_kidzapp',
            'true',
            false
          );
        } else {
          this.browserStorageService.setLocalStorage(
            'from_kidzapp',
            'false',
            false
          );
        }
      }
    } else {
      this.browserStorageService.setLocalStorage(
        'from_kidzapp',
        'false',
        false
      );
    }
  }

  isValidUrl(url: string) {
    try {
      return Boolean(new URL(url));
    } catch (e) {
      return false;
    }
  }
  ngAfterViewInit() {
    if (isPlatformBrowser(this.platFormId)) {
      // Google Tag Manager
      (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j:any = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PHJQN5');
    // Google Tag Manager

      setTimeout(() => {
         // Start of dcm-inc Zendesk Widget script 
      var zen = window.zE || (function (e, t, s) {
        var n: any = window.zE = window.zEmbed = function () {
          n._.push(arguments)
        },
          a = n.s = e.createElement(t),
          r = e.getElementsByTagName(t)[0];
        n.set = function (e) {
          n.set._.push(e)
        }, n._ = [], n.set._ = [], a.async = true, a.setAttribute("charset", "utf-8"), a.src =
          "https://static.zdassets.com/ekr/asset_composer.js?key=" + s, n.t = +new Date, a.type =
          "text/javascript", r.parentNode.insertBefore(a, r)
      });
        zen(document, "script", "2d6613e3-cbc7-477c-85da-2d1fe61b921a");
      }, 5000)
      // Start of dcm-inc Zendesk Widget script 
      
    }
  }
}
