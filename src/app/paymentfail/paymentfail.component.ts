import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingCartService } from '../shared/_services';
@Component({
  selector: 'app-paymentfail',
  templateUrl: './paymentfail.component.html',
  styleUrls: ['./paymentfail.component.css']
})
export class PaymentfailComponent implements OnInit {
  text: any;
  errorMessage: string;
  orderId: any;

  constructor(@Inject(WINDOW) private window: Window, private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformID: any,
    private shoppingCartService: ShoppingCartService) {
  }

  ngOnInit() {
    this.getOrderId();
  }

  getOrderId() {
    this.orderId = this.route.snapshot.queryParamMap.get('order_id');
    this.loadErrorMessage();
  }

  loadErrorMessage() {
    this.shoppingCartService.getErrorMessage(this.orderId)
      .subscribe(
        success => {
          this.errorMessage = success.result[0].gresponseerrormsg;
        },
        fail => {
          if (fail.error && fail.error.error && fail.error.error.message) {
            this.errorMessage = fail.error.error.message;
          }
        }
      );
  }

}
