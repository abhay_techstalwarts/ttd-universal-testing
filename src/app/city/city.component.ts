import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  SEOProfileService,
  ShoppingCartService,
  CityService,
  ArticleService,
  BrowserStorageService
} from '../shared/_services';
import { DefaultConstant } from '../shared/_constants/default_constants';
import { environment } from './../../environments/environment';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
declare let $:any;
@Component({
  moduleId: module.id.toString(),
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit, AfterViewInit {
  cloudinaryImageBaseUrl = DefaultConstant.cloudinaryImageBaseUrl;
  environment = environment;
  cityData: any;
  text: any;
  device: string;
  currency: string;
  categories: any[];
  cityActivities: any[];
  cityActivitiesPaged: any[];
  cityName: string;
  cityId: number;
  startItem;
  endItem;
  itemsPerPage = 10;
  maxSize = 5;
  rotate = true;
  showBoundaryLinks = true;
  showDirectionLinks = true;
  sideBar;
  articleItemWidth :number;
  articleItemCount: number=0;
  totalItemToShow =5;
  state = 'in';
  cityArticles: any[];
  cityArticlesPage: any[];
  articleData: any = [];
  cityArray: any = [];
  hide_select:boolean;
  hide_select_text:boolean = true;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private router: Router,
    private browserStorageService: BrowserStorageService,
    private cityService: CityService,
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private seoProfileService: SEOProfileService,
    private cartService: ShoppingCartService
  ) {}

  ngOnInit() {
    window.scroll(0, 0);
    this.currency = this.browserStorageService.getLocalStorage('currencyType');
    this.cartService.updateCartCount().subscribe(_success => {});
    this.cityArray = this.route.snapshot.paramMap.get('cityUrl').split('-');
    this.loadArticleData(this.cityArray[0]);
    this.getCityData();
    
  }

  loadCityData() {
    this.cityService.getCityData(this.cityId).subscribe(success => {
      this.cityData = success.result[0];
      if (this.cityData && this.cityData.Cityactivities) {
        this.cityActivities = this.cityData.Cityactivities;
        this.cityActivitiesPaged = this.cityActivities.slice(
          this.startItem,
          this.endItem
        );
      }
    });
  }
  ngAfterViewInit(){
    setTimeout(() => { 
      this.articleItemWidth= Number($("app-article-card").outerWidth()); 
      }, 1000)
  }

  prev(){
    if (this.articleItemCount > 0) {
      this.articleItemCount = (this.articleItemCount -1)
      let value = Number(this.articleItemWidth) *  this.articleItemCount;
      $("app-article-card").animate({left: `-${value}px`});
    }
  
  }
  next(){
    if(this.articleItemCount < this.articleData.length - this.totalItemToShow){
      this.articleItemCount = (this.articleItemCount +1)
    let value = Number(this.articleItemWidth) * this.articleItemCount
      $("app-article-card").animate({left: `-${value}px`});
    }
   
  }

  currencyChanged($event) {
    this.browserStorageService.setLocalStorage('currencyType', $event);
    this.currency = $event;
    this.loadCityData();
  }

  getCityData() {
    this.route.data.forEach(data => {
      const cityData = data.cityData.result[0];
      let metaImageUrl = '';
      if (cityData && cityData.largeimage_path_name) {
        metaImageUrl =
          this.cloudinaryImageBaseUrl +
          'v1/' +
          environment.cloudinaryServer +
          '/' +
          cityData.largeimage_path_name;
      }
      this.seoProfileService
        .setSEOProfileDetails(cityData.city_id, 'city', metaImageUrl)
        .subscribe(_success => {}, _error => {});
    });

    this.route.data.forEach(data => {
      this.cityData = data.cityData.result[0];
      this.cityName = this.cityData.name;
      this.cityId = this.cityData.city_id;
      if (this.cityData && this.cityData.Cityactivities) {
        this.cityActivities = this.cityData.Cityactivities;
        this.cityActivitiesPaged = this.cityActivities.slice(
          0,
          this.itemsPerPage
        );
      }
      this.getCategories();
    });
  }

  getCategories() {
    this.cityService.getCategories(this.cityData.city_id).subscribe(
      success => {
        this.categories = success.result;
      },
      _fail => {}
    );
  }

  loadCategoryActivities(category ,$event) {
    let findactivitychose = $event.target.innerHTML;
    if(findactivitychose !== '')
    {
      this.hide_select == true;
      this.hide_select_text = false;
    }
  $("#selectcityheader").text(findactivitychose);
    this.cityService
      .getCategoryWiseCityActivities(this.cityData.city_id, category.id)
      .subscribe(
        success => {
          this.cityActivities = success.result[0].Cityactivities;
         
          this.cityActivitiesPaged = this.cityActivities.slice(
            0,
            this.itemsPerPage
          );
        },
        _fail => {}
      );
}

  loadArticleData(city_id) {
    this.cityService.getArticles(city_id).subscribe(success => {
      this.articleData = success.result;
      this.articleService.setArticleData(this.articleData);
    });
  }

  pageChanged(event: PageChangedEvent): void {
    this.startItem = (event.page - 1) * event.itemsPerPage;
    this.endItem = event.page * event.itemsPerPage;
    this.cityActivitiesPaged = this.cityActivities.slice(
      this.startItem,
      this.endItem
    );
    window.scroll({
      top: 280,
      behavior: 'smooth'
    });
  }
}
