import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CityComponent } from './index';
import { SharedModule } from '../shared/shared.module';
import { CityRouting } from './city-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  declarations: [
    CityComponent,
  ],
  imports: [
    CityRouting,
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
  ],
  exports: [
    CityComponent,
  ],
  providers: [
  ]
})
export class CityModule {
}
