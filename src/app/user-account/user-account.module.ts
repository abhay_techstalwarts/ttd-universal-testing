import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UserAccountRouting } from './user-account-routing.module';
import { UserAccountComponent } from './user-account.component';
import { ProfileComponent } from './profile/profile.component';
import { BookingsComponent } from './bookings/bookings.component';


@NgModule({
  imports: [
    UserAccountRouting,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    UserAccountComponent,
    ProfileComponent,
    BookingsComponent,
    BookingsComponent,
  ]
})
export class UserAccountModule {
}
