
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationManager } from 'ng2-validation-manager';
import { BrowserStorageService } from '../../shared/_services/browserStorage.service';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import swal from 'sweetalert2';
import { AlertService,UserdataService,CityService } from '../../shared/_services/index';
import { environment } from '../../../environments/environment';
import { UserService } from '../../shared/_subjects/user.subject';
declare let $: any;

@Component({
  selector: 'app-user-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  text: object;
  user = {
    name: undefined,
    email: undefined,
    image: undefined,
    token: undefined,
    uid: undefined,
    provider: undefined
  };

  uploadingImage: boolean;

  userData: object;
  shopperId = "";
  userProfileForm: any;
  cities = [];
  returnUrl: string;
  @ViewChild('uploadButton') uploadButton: ElementRef;

  constructor(
    @Inject(WINDOW) private window: Window, @Inject(LOCAL_STORAGE) private localStorage: any,
    private browserStorageService: BrowserStorageService,
    private router: Router,
    private alertService: AlertService,
    private userdataService: UserdataService,
    private cityService: CityService,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.shopperId = this.browserStorageService.getLocalStorage('shopperId');

  }

  ngOnInit() {
    this.window.scrollTo(0, 0);
    this.user = JSON.parse(this.browserStorageService.getLocalStorage('user'));

    this.initaiteForm();
    this.getShopperdata();
    this.getCities();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
  }

  getShopperdata() {
    this.userdataService.shopperData(this.shopperId).subscribe(success => {
      this.userData = success.result[0];
      console.log("this.userData");
      console.log(this.userData);
      this.initializeForm(this.userData);
    });
  }

  getCities() {
    this.cityService.getCities().subscribe(success => {
      this.cities = success.result;
    });
  }

  logOut() {
    this.browserStorageService.emptyKeyLocalStorage('user');
    this.user = null;
    this.router.navigate(['']);
  }

  editShopperdata() {
    if (this.userProfileForm.isValid()) {
      this.userdataService
        .editshopperdata(this.userProfileForm.getData())
        .subscribe(
          success => {
            this.uploadingImage = false;
            this.browserStorageService.setLocalStorage('user', JSON.stringify(success.result));
            this.alertService.success(success.error.message);
            this.userService.modifyUser();
            if (this.returnUrl) {
              this.router.navigateByUrl(this.returnUrl);
            }
          },
          _fail => {
            this.alertService.error('Failed Updating User Details');
          }
        );
    }
  }

  uploadImage() {
    this.uploadingImage = true;
    if ($('#fileToUpload').val()) {
      const file_data = $('#fileToUpload').prop('files')[0];
      const form_data = new FormData();
      form_data.append('file', file_data);
      $.ajax({
        url: environment.imageUploadUrl,
        type: 'POST',
        data: form_data,
        async: true,
        success: data => {
          this.userProfileForm.setValue('image_url', data.result.secure_url);
          this.editShopperdata();
        },
        error: data => {
          swal(data.responseJSON.error.message, '', 'error');
        },
        cache: false,
        contentType: false,
        processData: false
      });
      return false;
    } else {
      this.uploadingImage = false;
    }
  }

  initaiteForm() {
    var pattern = new RegExp("^[0-9\\(\\)\\-\\+]{8,15}$");
    this.userProfileForm = new ValidationManager({
      shopper_id: { rules: 'required|number' },
      title: { rules: 'required' },
      firstname: { rules: 'required' },
      middlename: { rules: '' },
      lastname: { rules: 'required' },
      email: { rules: 'required|email' },
      phone: { rules: 'pattern:' + pattern, value: '' },
      faxno: { rules: '', value: '' },
      address: { rules: '', value: '' },
      city: { rules: '', value: '' },
      state: { rules: '', value: '' },
      country: { rules: '', value: '' },
      zipcode: { rules: '', value: '' },
      image_url: { rules: '', value: '' }
    });
    this.userProfileForm.setErrorMessage('phone', 'pattern', 'Phone number can contain: [0-9()-+] with 8-15 digits');
  }

  initializeForm(shopper: any) {
    this.userProfileForm.setValue({
      shopper_id: shopper.shopper_id == null ? '' : shopper.shopper_id,
      title: shopper.title == null ? '' : shopper.title,
      firstname: shopper.firstname == null ? '' : shopper.firstname,
      middlename: shopper.middlename == null ? '' : shopper.middlename,
      lastname: shopper.lastname == null ? '' : shopper.lastname,
      email: shopper.email == null ? '' : shopper.email,
      phone: shopper.phone == null ? '' : shopper.phone,
      faxno: shopper.faxno == null ? '' : shopper.faxno,
      address: shopper.address == null ? '' : shopper.address,
      city: shopper.shoppcityer_id == null ? '' : shopper.shoppcityer_id,
      state: shopper.state == null ? '' : shopper.state,
      country: shopper.country == null ? '' : shopper.country,
      zipcode: shopper.zipcode == null ? '' : shopper.zipcode,
      image_url: shopper.image_url
    });
  }
}
