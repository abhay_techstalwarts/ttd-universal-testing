import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { AlertService,UserdataService } from '../../shared/_services/index';
import { BrowserStorageService } from './../../shared/_services/browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { ValidationManager } from 'ng2-validation-manager';
import { FormHelper } from '../../shared/_helper/form-helper';
import swal from 'sweetalert2';
import { environment } from './../../../environments/environment';
import * as _filter from 'lodash/filter';

declare let $: any;

@Component({
  selector: 'app-user-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  bookings;
  shopperId = '';
  cancelOrderForm;
  orderId;
  orderTransactions;
  loadingTransactions: boolean;
  apiBaseUrl = environment.apiBaseUrl;
  @ViewChild('cancelButton')
  cancelButton: ElementRef;
  bookingId;
  cancelBookingForm;
  @ViewChild('cancelBookingButton')
  cancelBookingButton: ElementRef;
  cancelPackages: any[];

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private alertService: AlertService,
    private userdataService: UserdataService,
    private browserStorageService: BrowserStorageService
  ) {
    // this.shopperId = localStorage.getItem('shopperId');
    this.shopperId = this.browserStorageService.getLocalStorage('shopperId');

  }

  ngOnInit() {
    this.getBookings();
    this.initaiteForm();
  }

  getBookings() {
    this.userdataService.getUserBookings(this.shopperId).subscribe(
      success => {
        this.bookings = success.result;
      },
      _fail => {
        this.bookings = [];
      }
    );
  }

  cancelOrder() {
    this.cancelOrderForm.setValue('order_id', this.orderId);
    if (this.cancelOrderForm.isValid()) {
      swal({
        title: 'Are you sure?',
        text: 'Are you sure want to cancel this order?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then(result => {
        if (result.value) {
          $('#myModal').modal('hide');
          FormHelper.disableButton(this.cancelButton, 'Cancelling...');
          this.userdataService
            .cancelOrder(this.cancelOrderForm.getData())
            .subscribe(
              _success => {
                FormHelper.enableButton(this.cancelButton, 'Ok');
                this.getBookings();
                this.initaiteForm();
                swal('Order cancelled successfully', '', 'success');
              },
              _fail => {
                FormHelper.enableButton(this.cancelButton, 'Ok');
                $('#myModal').modal('hide');
                swal('Order cancellation failed', '', 'error');
              }
            );
        }
      });
    }
  }

  dropItem(orderId, bookingId, transactions: any[]) {
    this.orderId = orderId;
    this.bookingId = bookingId;
    this.cancelPackages = _filter(transactions, ['booking_id', bookingId]);
  }

  cancelBooking() {
    this.cancelBookingForm.setValue('order_id', this.orderId);
    this.cancelBookingForm.setValue('booking_id', this.bookingId);
    if (this.cancelBookingForm.isValid()) {
      swal({
        title: 'Are you sure?',
        text: 'Are you sure want to cancel this booking?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then(result => {
        if (result.value) {
          $('#cancelBooking').modal('hide');
          FormHelper.disableButton(this.cancelBookingButton, 'Cancelling...');
          this.userdataService
            .cancelPackage(this.cancelBookingForm.getData())
            .subscribe(
              _success => {
                FormHelper.enableButton(this.cancelBookingButton, 'Ok');
                this.getBookings();
                this.initaiteForm();
                swal('Booking cancelled successfully', '', 'success');
              },
              _fail => {
                FormHelper.enableButton(this.cancelBookingButton, 'Ok');
                $('#cancelBooking').modal('hide');
                swal('Booking cancellation failed', '', 'error');
              }
            );
        }
      });
    }
  }

  loadTransactions(transactions) {
    this.orderTransactions = transactions;
  }

  initaiteForm() {
    this.cancelOrderForm = new ValidationManager({
      order_id: { rules: 'required' },
      reason: { rules: 'required' }
    });
    this.cancelBookingForm = new ValidationManager({
      order_id: { rules: 'required' },
      booking_id: { rules: 'required' },
      reason: { rules: 'required' }
    });
  }
}
