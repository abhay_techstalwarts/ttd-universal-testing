import { Component, OnInit , Inject} from '@angular/core';
import { BrowserStorageService } from './../shared/_services/browserStorage.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {
  device: string;

  constructor(
    @Inject(LOCAL_STORAGE)
     private localStorage: any,
     private browserStorageService:BrowserStorageService ) {

    this.device = this.browserStorageService.getLocalStorage('device');

  }

  ngOnInit() {
  }

}
