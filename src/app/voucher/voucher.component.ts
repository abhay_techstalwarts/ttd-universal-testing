
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingCartService } from '../shared/_services';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {
  text: any;
  orderId: string | number;
  transactionId: string | number;
  voucherDetails: any;
  qrCode;
  isServer = false;

  showMapLink = true;

  constructor(
    @Inject(WINDOW) private window: Window, 
    @Inject(LOCAL_STORAGE) private localStorage: any, private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: any,
    private cartService: ShoppingCartService,
    public sanitizer: DomSanitizer) {
    if (isPlatformServer(this.platformId)) {
      this.isServer = true;
    }
  }

  ngOnInit() {
    this.getOrderId();
    this.getVoucherDeatails();
    this.cartService.updateCartCount().subscribe(_success => { });

  }

  getOrderId() {
    this.orderId = this.route.snapshot.paramMap.get('order_id');
    this.transactionId = this.route.snapshot.queryParamMap.get('transaction_id');
  }

  getVoucherDeatails() {
    this.cartService
      .getVoucherDetails(this.orderId, this.transactionId)
      .subscribe((success: any) => {
        this.voucherDetails = success.result.order_details[0];
        if (!this.voucherDetails.booking_activity_time) {
          this.voucherDetails.booking_activity_time = this.voucherDetails.operating_hours;
        }
        if (this.voucherDetails.supplierOrders_id) {
          this.getQRCode(this.voucherDetails);
        }
      });
  }

  getQRCode(voucherDetails) {
    this.cartService
      .getQrcode(voucherDetails.QR_code_url)
      .subscribe(success => {
        this.qrCode = success.image_content;
      });
  }

  getCapacityManaged(hasManagedCapacity) {
    if (hasManagedCapacity == null || hasManagedCapacity === false) {
      return 'No';
    } else {
      return 'Yes';
    }
  }

  print() {
    this.showMapLink = false;
    setTimeout(() => {
      if (isPlatformBrowser(this.platformId)) {
        this.window.print();
      }
    }, 100);
    setTimeout(() => {
      this.showMapLink = true;
    }, 10000);
  }
}
