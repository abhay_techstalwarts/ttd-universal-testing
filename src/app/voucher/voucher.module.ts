import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VoucherRouting} from './voucher.routing';
import {VoucherComponent} from './voucher.component';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        VoucherRouting,
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA8tN4tdy2crRpSA79Xf8exRCjo2UWqoIc'
          }),
    ],
    declarations: [
        VoucherComponent,
    ]
})
export class VoucherModule {
}
