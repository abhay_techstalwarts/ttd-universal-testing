import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VoucherComponent} from './voucher.component';

const routes: Routes = [
  { path: ':order_id', component: VoucherComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})

export class VoucherRouting {
}
